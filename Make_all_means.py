#!/usr/bin/env python
'''
    This file is part of the scripts accompanying the 
    2018 Hunger, Kumar, Schmidt Paper titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations"   
    This script should be run after the Make_the_data_ROC script to calculate averages 
    and use the classifier to classify different sequences. This needs to be done before running the 
    Plot_F2* scripts.
    
    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import pickle
import os
import numpy as np
from Make_avg_classify_grid import Make_avg
from Make_avg_classify_grid import Make_avg_classify_grid
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n','--nr_samples', nargs='?',type=int,default=10, const=10, help='How many realizations are to be calculated. The paper uses 500, default is 10.')
args = parser.parse_args()

nr_steps=5000
nr_samples=args.nr_samples

my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Fig_2_ROC") 

os.chdir(res_path)  #go to the folder

for p_rew in np.arange(0.0,1.01,0.1):
    for DR in ["D1","D2"]:
        print "Averages: p_rew, DR:", p_rew, DR
        Make_avg(D_switch=DR,p_burst=p_rew,p_buwp=1.0-p_rew,nr_samples=nr_samples)


for DR in ["D1","D2"]:
    print "Classify:",DR
    Make_avg_classify_grid(D_switch=DR,nr_samples=nr_samples)



