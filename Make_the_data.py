#!/usr/bin/env python
''' This is a script that runs all the subscripts to make the data for the figures (except the ROC figures) in the Hunger, Kumar, Schmidt 2018 Publication 
    titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations"
    The figures themselves will be made from this data in another script. 
    This script is more or less an afterthought to be used instead of the originally used BASH scripts (to be more OS independent). 
    That also means the script isn't very pretty, if you wanna make it prettier feel free to do so.
       
    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import sys
import os
import glob
import shutil
import argparse
from R_occup_DA_free_single import R_occup_DA

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-slo','--skip_long_ones', action='store_true' , default=False, help='If this option is used  time-intensive data items will not be created, shortening the runtime of the script.')
    parser.add_argument('-sos','--skip_one_state', action='store_true' , default=False, help='If this option is used  one state receptor data items will not be created, shortening the runtime of the script.')
    parser.add_argument('-sts','--skip_two_state', action='store_true' , default=False, help='If this option is used  two state receptor data items will not be created, shortening the runtime of the script.')


    args = parser.parse_args()
    





    long_ones=not args.skip_long_ones            #flag to decide if the time-intensive data items should be recreated too ... if this is true running this will prob. take 30 minutes to an hour
    one_state_receptors=not args.skip_one_state    #Make data for one state receptors
    two_state_receptors=not args.skip_two_state    #Make data for two state receptors (e.g. like in Supp.Fig.7 D1^high and D1^low )
    print "Starting..."
    print "Time intensive data items:", long_ones
    print "One State receptor data items:", one_state_receptors
    print "Two State receptor data items:", two_state_receptors
    
    my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
    

#--------------------------- First part that generates data ----------------------------------------------------------------------------#
    res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_1_kinetics") #make a Data folder with a subfolder for the kinetic data
    if not os.path.exists(res_path):    #if the path doesn't exist, create it
        os.makedirs(res_path)

    if(one_state_receptors):
        #These make data for Supp.Fig.1 a,b,c,d
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0) #D1 receptors, burst w_pause for V_max=1.5, rise_time=0.1s, DA_rise=100.0nM and pause length=1.0s
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=2.0)   #kinetics faster by x2
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=10.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=50.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=100.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,ode_mode='Kd')
        
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0) #D2 receptors, burst w_pause for V_max=1.5, rise_time=0.1s, DA_rise=100.0nM and pause length=1.0s
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=2.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=10.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=50.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=100.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,ode_mode='Kd')
        
        #We do the bursts too so that we can also have D1-D2 compare for the long burst, and kinetic comparisons for lb too
        R_occup_DA('D1','burst',1.5,0.2,200.0)  #D1 receptors, burst for V_max=1.5, rise_time=0.2s, DA_rise=200.0nM
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=2.0)
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=10.0)
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=50.0)
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=100.0)
        
        R_occup_DA('D2','burst',1.5,0.2,200.0)  #D2 receptors, burst for V_max=1.5, rise_time=0.2s, DA_rise=200.0nM
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=2.0)
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=10.0)
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=50.0)
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=100.0)
        
        R_occup_DA('D1','burst',1.5,0.2,200.0,ode_mode='Kd')
        R_occup_DA('D2','burst',1.5,0.2,200.0,ode_mode='Kd')
        
        if(long_ones):
            #These make data for Supp.Fig.1 e f
            R_occup_DA('D1','50_bu',1.5,0.2,200.0)  #pattern with 50 bursts , burst parameters as above t_repetition is 15.0s (a urst every 15.0s) 
            R_occup_DA('D1','50_bu',1.5,0.2,200.0,fast_r=2.0)
            R_occup_DA('D1','50_bu',1.5,0.2,200.0,fast_r=10.0)
            
            R_occup_DA('D2','50_bu',1.5,0.2,200.0)  #pattern with 50 bursts , burst parameters as above t_repetition is 15.0s (a urst every 15.0s) 
            R_occup_DA('D2','50_bu',1.5,0.2,200.0,fast_r=2.0)
            R_occup_DA('D2','50_bu',1.5,0.2,200.0,fast_r=10.0)
    
    if(two_state_receptors):    #do when the receptors are treated with their high/low states both
        #These make data for Supp.Fig.1 a,b,c,d
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,frac_s2=0.1) #D1 receptors, burst w_pause for V_max=1.5, rise_time=0.1s, DA_rise=100.0nM and pause length=1.0s
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=2.0,frac_s2=0.1)   #kinetics faster by x2
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=10.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=50.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=100.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.0,ode_mode='Kd',frac_s2=0.1)
        
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,frac_s2=0.1) #D2 receptors, burst w_pause for V_max=1.5, rise_time=0.1s, DA_rise=100.0nM and pause length=1.0s
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=2.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=10.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=50.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,fast_r=100.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.0,ode_mode='Kd',frac_s2=0.1)
        
        #We do the bursts too so that we can also have D1-D2 compare for the long burst, and kinetic comparisons for lb too
        R_occup_DA('D1','burst',1.5,0.2,200.0,frac_s2=0.1)  #D1 receptors, burst for V_max=1.5, rise_time=0.2s, DA_rise=200.0nM
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=2.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=10.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=50.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,fast_r=100.0,frac_s2=0.1)
        
        R_occup_DA('D2','burst',1.5,0.2,200.0,frac_s2=0.1)  #D2 receptors, burst for V_max=1.5, rise_time=0.2s, DA_rise=200.0nM
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=2.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=10.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=50.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,fast_r=100.0,frac_s2=0.1)
        
        R_occup_DA('D1','burst',1.5,0.2,200.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,ode_mode='Kd',frac_s2=0.1)
        
        if(long_ones):
            #These make data for Supp.Fig.1 e f
            R_occup_DA('D1','50_bu',1.5,0.2,200.0,frac_s2=0.1)  #pattern with 50 bursts , burst parameters as above t_repetition is 15.0s (a urst every 15.0s,frac_s2=0.1) 
            R_occup_DA('D1','50_bu',1.5,0.2,200.0,fast_r=2.0,frac_s2=0.1)
            R_occup_DA('D1','50_bu',1.5,0.2,200.0,fast_r=10.0,frac_s2=0.1)
            
            R_occup_DA('D2','50_bu',1.5,0.2,200.0,frac_s2=0.1)  #pattern with 50 bursts , burst parameters as above t_repetition is 15.0s (a urst every 15.0s,frac_s2=0.1) 
            R_occup_DA('D2','50_bu',1.5,0.2,200.0,fast_r=2.0,frac_s2=0.1)
            R_occup_DA('D2','50_bu',1.5,0.2,200.0,fast_r=10.0,frac_s2=0.1)
    
    
    

    #move the generated Data to Data/Diff_kinetics, the folder we made above
    for fn in glob.glob(os.path.join(my_path,'*'+'.dat')):
        shutil.move(fn,os.path.join(res_path,os.path.basename(fn)))

#------------------------------ end of first part of generated data --------------------------------------------------------------#

#------------------------------2nd part that makes data... this is Fig.1 e -------------------------------------------------------#
    res_path=os.path.join(my_path, "Data/Diff_Fig_1_efg") #make a Data folder with a subfolder for the kinetic data
    if not os.path.exists(res_path):    #if the path doesn't exist, create it
        os.makedirs(res_path)

    if(one_state_receptors):
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.3)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.3)
        R_occup_DA('D1','burst',1.5,0.2,200.0)
        R_occup_DA('D2','burst',1.5,0.2,200.0)

        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.3,ode_mode='Kd')
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.3,ode_mode='Kd')
        R_occup_DA('D1','burst',1.5,0.2,200.0,ode_mode='Kd')
        R_occup_DA('D2','burst',1.5,0.2,200.0,ode_mode='Kd')
    
    if(two_state_receptors):
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.3,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.3,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,frac_s2=0.1)

        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.3,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.3,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,ode_mode='Kd',frac_s2=0.1)
        
    
    #move the generated Data to Data/Diff_Fig_1_efg, the folder we made above
    for fn in glob.glob(os.path.join(my_path,'*'+'.dat')):
        shutil.move(fn,os.path.join(res_path,os.path.basename(fn)))
        
#------------------------------2nd part that makes data... end -------------------------------------------------------#


#------------------------------3rd part that makes data... this is Fig.1 h the ramps -------------------------------------------------------#
    
    res_path=os.path.join(my_path, "Data/Diff_Fig_1_h_ramps") #make a Data folder with a subfolder for the kinetic data
    if not os.path.exists(res_path):    #if the path doesn't exist, create it
        os.makedirs(res_path)
    
    if(one_state_receptors):
        R_occup_DA('D1','burst',1.5,2.0,50.0)       #ramps
        R_occup_DA('D1','burst',1.5,3.0,50.0)
        R_occup_DA('D1','burst',1.5,4.0,50.0)
        R_occup_DA('D1','burst',1.5,5.0,50.0)
        R_occup_DA('D1','burst',1.5,6.0,50.0)
        
        R_occup_DA('D2','burst',1.5,2.0,50.0)
        R_occup_DA('D2','burst',1.5,3.0,50.0)
        R_occup_DA('D2','burst',1.5,4.0,50.0)
        R_occup_DA('D2','burst',1.5,5.0,50.0)
        R_occup_DA('D2','burst',1.5,6.0,50.0)
        
        R_occup_DA('D1','burst',1.5,2.0,50.0,ode_mode='Kd')       #ramps, instant kinetics
        R_occup_DA('D1','burst',1.5,3.0,50.0,ode_mode='Kd')
        R_occup_DA('D1','burst',1.5,4.0,50.0,ode_mode='Kd')
        R_occup_DA('D1','burst',1.5,5.0,50.0,ode_mode='Kd')
        R_occup_DA('D1','burst',1.5,6.0,50.0,ode_mode='Kd')
        
        R_occup_DA('D2','burst',1.5,2.0,50.0,ode_mode='Kd')
        R_occup_DA('D2','burst',1.5,3.0,50.0,ode_mode='Kd')
        R_occup_DA('D2','burst',1.5,4.0,50.0,ode_mode='Kd')
        R_occup_DA('D2','burst',1.5,5.0,50.0,ode_mode='Kd')
        R_occup_DA('D2','burst',1.5,6.0,50.0,ode_mode='Kd')
    
    if(two_state_receptors):
        R_occup_DA('D1','burst',1.5,2.0,50.0,frac_s2=0.1)       #ramps
        R_occup_DA('D1','burst',1.5,3.0,50.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,4.0,50.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,5.0,50.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,6.0,50.0,frac_s2=0.1)
        
        R_occup_DA('D2','burst',1.5,2.0,50.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,3.0,50.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,4.0,50.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,5.0,50.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,6.0,50.0,frac_s2=0.1)
        
        R_occup_DA('D1','burst',1.5,2.0,50.0,ode_mode='Kd',frac_s2=0.1)       #ramps, instant kinetics
        R_occup_DA('D1','burst',1.5,3.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,4.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,5.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,6.0,50.0,ode_mode='Kd',frac_s2=0.1)
        
        R_occup_DA('D2','burst',1.5,2.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,3.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,4.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,5.0,50.0,ode_mode='Kd',frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,6.0,50.0,ode_mode='Kd',frac_s2=0.1)
    
    #move the generated Data to Data/Diff_Fig_1_h_ramps, the folder we made above
    for fn in glob.glob(os.path.join(my_path,'*'+'.dat')):
        shutil.move(fn,os.path.join(res_path,os.path.basename(fn)))
        
#---------------------------- end of data for Ramps figure ---------------------------------------------------------------#

#--------------------------------Data for Supp.Fig.2 Pause cancels -------------------------------------------------------#
    res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_2_t_pause") #make a Data folder with a subfolder for the kinetic data
    if not os.path.exists(res_path):    #if the path doesn't exist, create it
        os.makedirs(res_path)

    if(one_state_receptors):
        R_occup_DA('D1','burst_w_pause',1.5,0.1,150.0,2.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.3)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,60.0,0.8)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,150.0,2.5,BL_fraction_min_DA=0.25)
        
        R_occup_DA('D2','burst_w_pause',1.5,0.1,150.0,2.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.3)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,60.0,0.8)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,150.0,2.5,BL_fraction_min_DA=0.25)    
    
    if(two_state_receptors):
        R_occup_DA('D1','burst_w_pause',1.5,0.1,150.0,2.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,100.0,1.3,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.1,60.0,0.8,frac_s2=0.1)
        
        R_occup_DA('D2','burst_w_pause',1.5,0.1,150.0,2.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,100.0,1.3,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.1,60.0,0.8,frac_s2=0.1)

    #move the generated Data to Data/Diff_Supp_Fig_2_t_pause, the folder we made above
    for fn in glob.glob(os.path.join(my_path,'*'+'.dat')):
        shutil.move(fn,os.path.join(res_path,os.path.basename(fn)))



# ------------------------------end of data for Pause cancels ------------------------------------------------------------#

#-------------------------------Data for Supp.Fig. 3 and 4  Parameter study ----------------------------------------------#
    res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_3_4_param_study") #make a Data folder with a subfolder for the kinetic data
    if not os.path.exists(res_path):    #if the path doesn't exist, make it
        os.makedirs(res_path)
    if(one_state_receptors):
        #D2 different V_max
        R_occup_DA('D2','burst',1.0,0.2,200.0)
        R_occup_DA('D2','burst',1.5,0.2,200.0)
        R_occup_DA('D2','burst',2.0,0.2,200.0)
        R_occup_DA('D2','burst',2.5,0.2,200.0)
        R_occup_DA('D2','burst',3.0,0.2,200.0)
        R_occup_DA('D2','burst',3.5,0.2,200.0)
        R_occup_DA('D2','burst',4.0,0.2,200.0)

        #D1 different V_max 
        R_occup_DA('D1','burst',1.0,0.2,200.0)
        R_occup_DA('D1','burst',1.5,0.2,200.0)
        R_occup_DA('D1','burst',2.0,0.2,200.0)
        R_occup_DA('D1','burst',2.5,0.2,200.0)
        R_occup_DA('D1','burst',3.0,0.2,200.0)
        R_occup_DA('D1','burst',3.5,0.2,200.0)
        R_occup_DA('D1','burst',4.0,0.2,200.0)

        #D2 different peak heights
        R_occup_DA('D2','burst',1.5,0.2,50.0)
        R_occup_DA('D2','burst',1.5,0.2,100.0)
        R_occup_DA('D2','burst',1.5,0.2,150.0)
        R_occup_DA('D2','burst',1.5,0.2,200.0)
        R_occup_DA('D2','burst',1.5,0.2,250.0)
        R_occup_DA('D2','burst',1.5,0.2,300.0)
        R_occup_DA('D2','burst',1.5,0.2,350.0)
        R_occup_DA('D2','burst',1.5,0.2,400.0)
        R_occup_DA('D2','burst',1.5,0.2,500.0)
        R_occup_DA('D2','burst',1.5,0.2,600.0)
        R_occup_DA('D2','burst',1.5,0.2,700.0)
        R_occup_DA('D2','burst',1.5,0.2,800.0)
        R_occup_DA('D2','burst',1.5,0.2,900.0)
        R_occup_DA('D2','burst',1.5,0.2,1000.0)

        #D1 different peak heights
        R_occup_DA('D1','burst',1.5,0.2,50.0)
        R_occup_DA('D1','burst',1.5,0.2,100.0)
        R_occup_DA('D1','burst',1.5,0.2,150.0)
        R_occup_DA('D1','burst',1.5,0.2,200.0)
        R_occup_DA('D1','burst',1.5,0.2,250.0)
        R_occup_DA('D1','burst',1.5,0.2,300.0)
        R_occup_DA('D1','burst',1.5,0.2,350.0)
        R_occup_DA('D1','burst',1.5,0.2,400.0)
        R_occup_DA('D1','burst',1.5,0.2,500.0)
        R_occup_DA('D1','burst',1.5,0.2,600.0)
        R_occup_DA('D1','burst',1.5,0.2,700.0)
        R_occup_DA('D1','burst',1.5,0.2,800.0)
        R_occup_DA('D1','burst',1.5,0.2,900.0)
        R_occup_DA('D1','burst',1.5,0.2,1000.0)

        #D2 different rise times- same peak height
        R_occup_DA('D2','burst',1.5,0.2,200.0)
        R_occup_DA('D2','burst',1.5,0.5,200.0)
        R_occup_DA('D2','burst',1.5,1.0,200.0)
        R_occup_DA('D2','burst',1.5,1.5,200.0)
        R_occup_DA('D2','burst',1.5,2.0,200.0)
        R_occup_DA('D2','burst',1.5,3.0,200.0)
        R_occup_DA('D2','burst',1.5,4.0,200.0)
        R_occup_DA('D2','burst',1.5,5.0,200.0)
        R_occup_DA('D2','burst',1.5,6.0,200.0)
        R_occup_DA('D2','burst',1.5,7.0,200.0)

        #D1 different rise times- same peak height
        R_occup_DA('D1','burst',1.5,0.2,200.0)
        R_occup_DA('D1','burst',1.5,0.5,200.0)
        R_occup_DA('D1','burst',1.5,1.0,200.0)
        R_occup_DA('D1','burst',1.5,1.5,200.0)
        R_occup_DA('D1','burst',1.5,2.0,200.0)
        R_occup_DA('D1','burst',1.5,3.0,200.0)
        R_occup_DA('D1','burst',1.5,4.0,200.0)
        R_occup_DA('D1','burst',1.5,5.0,200.0)
        R_occup_DA('D1','burst',1.5,6.0,200.0)
        R_occup_DA('D1','burst',1.5,7.0,200.0)

        #D2 with pause ... different pause times 
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,0.5)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,1.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,1.5)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,2.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,2.5)

        #D1 with pause ... different pause times 
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,0.5)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,1.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,1.5)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,2.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,2.5)

        #D2 just pause ... different pause times 
        R_occup_DA('D2','just_pause',1.5,pause_length=1.0)
        R_occup_DA('D2','just_pause',1.5,pause_length=2.0)
        R_occup_DA('D2','just_pause',1.5,pause_length=3.0)
        R_occup_DA('D2','just_pause',1.5,pause_length=4.0)
        R_occup_DA('D2','just_pause',1.5,pause_length=5.0)
        R_occup_DA('D2','just_pause',1.5,pause_length=5.0,BL_fraction_min_DA=0.25)  #makes the data for the curve where the DA Baseline does not drop  to 0

        #D1 with pause ... different pause times 
        R_occup_DA('D1','just_pause',1.5,pause_length=1.0)
        R_occup_DA('D1','just_pause',1.5,pause_length=2.0)
        R_occup_DA('D1','just_pause',1.5,pause_length=3.0)
        R_occup_DA('D1','just_pause',1.5,pause_length=4.0)
        R_occup_DA('D1','just_pause',1.5,pause_length=5.0)
        R_occup_DA('D1','just_pause',1.5,pause_length=5.0,BL_fraction_min_DA=0.25)   #makes the data for the curve where the DA Baseline does not drop  to 0
        
        
        #D2 with pause ... different pause times stronger pulse 
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,0.5)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,1.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,1.5)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,2.0)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,2.5)

        #D1 with pause ... different pause times stronger pulse
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,0.5)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,1.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,1.5)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,2.0)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,2.5)
    
    if(two_state_receptors):
        #D2 different V_max
        R_occup_DA('D2','burst',1.0,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',2.0,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',2.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',3.0,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',3.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',4.0,0.2,200.0,frac_s2=0.1)

        #D1 different V_max 
        R_occup_DA('D1','burst',1.0,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',2.0,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',2.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',3.0,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',3.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',4.0,0.2,200.0,frac_s2=0.1)

        #D2 different peak heights
        R_occup_DA('D2','burst',1.5,0.2,50.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,100.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,150.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,250.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,300.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,350.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,400.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,500.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,600.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,700.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,800.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,900.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.2,1000.0,frac_s2=0.1)

        #D1 different peak heights
        R_occup_DA('D1','burst',1.5,0.2,50.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,100.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,150.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,250.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,300.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,350.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,400.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,500.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,600.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,700.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,800.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,900.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.2,1000.0,frac_s2=0.1)

        #D2 different rise times- same peak height
        R_occup_DA('D2','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,0.5,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,1.0,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,1.5,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,2.0,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,3.0,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,4.0,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,5.0,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,6.0,200.0,frac_s2=0.1)
        R_occup_DA('D2','burst',1.5,7.0,200.0,frac_s2=0.1)

        #D1 different rise times- same peak height
        R_occup_DA('D1','burst',1.5,0.2,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,0.5,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,1.0,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,1.5,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,2.0,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,3.0,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,4.0,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,5.0,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,6.0,200.0,frac_s2=0.1)
        R_occup_DA('D1','burst',1.5,7.0,200.0,frac_s2=0.1)

        #D2 with pause ... different pause times 
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,0.5,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,1.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,1.5,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,2.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,200.0,2.5,frac_s2=0.1)

        #D1 with pause ... different pause times 
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,0.5,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,1.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,1.5,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,2.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,200.0,2.5,frac_s2=0.1)

        #D2 just pause ... different pause times 
        R_occup_DA('D2','just_pause',1.5,pause_length=1.0,frac_s2=0.1)
        R_occup_DA('D2','just_pause',1.5,pause_length=2.0,frac_s2=0.1)
        R_occup_DA('D2','just_pause',1.5,pause_length=3.0,frac_s2=0.1)
        R_occup_DA('D2','just_pause',1.5,pause_length=4.0,frac_s2=0.1)
        R_occup_DA('D2','just_pause',1.5,pause_length=5.0,frac_s2=0.1)

        #D1 with pause ... different pause times 
        R_occup_DA('D1','just_pause',1.5,pause_length=1.0,frac_s2=0.1)
        R_occup_DA('D1','just_pause',1.5,pause_length=2.0,frac_s2=0.1)
        R_occup_DA('D1','just_pause',1.5,pause_length=3.0,frac_s2=0.1)
        R_occup_DA('D1','just_pause',1.5,pause_length=4.0,frac_s2=0.1)
        R_occup_DA('D1','just_pause',1.5,pause_length=5.0,frac_s2=0.1)
        
        
        #D2 with pause ... different pause times stronger pulse 
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,0.5,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,1.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,1.5,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,2.0,frac_s2=0.1)
        R_occup_DA('D2','burst_w_pause',1.5,0.2,500.0,2.5,frac_s2=0.1)

        #D1 with pause ... different pause times stronger pulse
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,0.5,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,1.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,1.5,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,2.0,frac_s2=0.1)
        R_occup_DA('D1','burst_w_pause',1.5,0.2,500.0,2.5,frac_s2=0.1)


    #move the generated Data to Data/Diff_Supp_Fig_3_4_param_study, the folder we made above
    for fn in glob.glob(os.path.join(my_path,'*'+'.dat')):
        shutil.move(fn,os.path.join(res_path,os.path.basename(fn)))





#------------------------------End of data for Supp.Fig. 5 Sequences ------------------------------------------# 
    if(long_ones):
        res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_5_sequences") #make a Data folder with a subfolder for the kinetic data
        if not os.path.exists(res_path):    #if the path doesn't exist, make it
            os.makedirs(res_path)

        if(one_state_receptors):
            R_occup_DA('D1','40_00_bu_buwp',1.5,0.2,200.0,1.3)
            R_occup_DA('D1','40_10_bu_buwp',1.5,0.2,200.0,1.3)
            R_occup_DA('D1','50_00_bu_buwp',1.5,0.2,200.0,1.3)
            
            R_occup_DA('D2','40_00_bu_buwp',1.5,0.2,200.0,1.3)
            R_occup_DA('D2','40_10_bu_buwp',1.5,0.2,200.0,1.3)
            R_occup_DA('D2','50_00_bu_buwp',1.5,0.2,200.0,1.3)
        if(two_state_receptors):
            R_occup_DA('D1','40_00_bu_buwp',1.5,0.2,200.0,1.3,frac_s2=0.1)
            R_occup_DA('D1','40_10_bu_buwp',1.5,0.2,200.0,1.3,frac_s2=0.1)
            R_occup_DA('D1','50_00_bu_buwp',1.5,0.2,200.0,1.3,frac_s2=0.1)
            
            R_occup_DA('D2','40_00_bu_buwp',1.5,0.2,200.0,1.3,frac_s2=0.1)
            R_occup_DA('D2','40_10_bu_buwp',1.5,0.2,200.0,1.3,frac_s2=0.1)
            R_occup_DA('D2','50_00_bu_buwp',1.5,0.2,200.0,1.3,frac_s2=0.1)

        #move the generated Data to Data/Diff_Supp_Fig_5_sequences, the folder we made above
        for fn in glob.glob(os.path.join(my_path,'*'+'.dat')):
            shutil.move(fn,os.path.join(res_path,os.path.basename(fn)))


#------------------------------End of data for Supp.Fig. 5 Sequences ------------------------------------------#












