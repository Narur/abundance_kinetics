#!/usr/bin/env python
''' This script generates all Panels for Figure 4 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_data script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
import os
nr_steps=5000


#Where am I and where are the data files ? (assuming the data files have been made with the Make_the Data script)
my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_5_sequences") #make a Data folder with a subfolder for the kinetic data


D1_40_00=pickle.load(open(os.path.join(res_path,"free_D1_40_00_bu_buwp.dat"),"rb"))
D1_40_10=pickle.load(open(os.path.join(res_path,"free_D1_40_10_bu_buwp.dat"),"rb"))
D1_50_00=pickle.load(open(os.path.join(res_path,"free_D1_50_00_bu_buwp.dat"),"rb"))

D2_40_00=pickle.load(open(os.path.join(res_path,"free_D2_40_00_bu_buwp.dat"),"rb"))
D2_40_10=pickle.load(open(os.path.join(res_path,"free_D2_40_10_bu_buwp.dat"),"rb"))
D2_50_00=pickle.load(open(os.path.join(res_path,"free_D2_50_00_bu_buwp.dat"),"rb"))










legend_fontsize=15 #set the fontsize in all legends in this script

plt.close('all')
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})
#Plot receptor activation for D1  receptors for the timecourse of NAcc 
fig=plt.figure()
fig.clf()
time=np.arange(len(D1_40_10[:,0]))
ax1=fig.add_subplot(111)


ax1.set_ylim(19.0,24.0)
ax1.set_xlim(-50.0,1000.0)
#ax2.set_ylim(ax2_min,ax2_max)

ax1.plot(time/1000.0,D1_50_00[:,2],lw=3,label=r'\textbf{Pattern 50-00}')
ax1.plot(time/1000.0,D1_40_10[:,2],lw=3,label=r'\textbf{Pattern 40-10}')
ax1.plot(time/1000.0,D1_40_00[:,2],lw=3,label=r'\textbf{Pattern 40-00}')


#ax1.set_ylabel('[D1-DA] in nM',fontsize=15)

#ax1.set_xlabel('Time in s',fontsize=15)

#fig.suptitle('D1 receptor activation - realistic kinetics')

ax1.legend(fontsize=legend_fontsize)

filename="Fig_4_a_D1_40_10.pdf"

fig.savefig(filename)

fig.show()
fig.clf()

#Plot receptor activation for D2  receptors for the timecourse of NAcc 
fig=plt.figure()
fig.clf()
time=np.arange(len(D2_40_10[:,0]))
ax1=fig.add_subplot(111)


ax1.set_ylim(35.0,40.0)
ax1.set_xlim(-50.0,1000.0)
#ax2.set_ylim(ax2_min,ax2_max)

ax1.plot(time/1000.0,D2_50_00[:,2],lw=3,label=r'\textbf{Pattern 50-00}')
ax1.plot(time/1000.0,D2_40_10[:,2],lw=3,label=r'\textbf{Pattern 40-10}')
ax1.plot(time/1000.0,D2_40_00[:,2],lw=3,label=r'\textbf{Pattern 40-00}')


#ax1.set_ylabel('[D2-DA] in nM',fontsize=15)

#ax1.set_xlabel('Time in s',fontsize=15)

#fig.suptitle('D2 receptor activation - realistic kinetics')

ax1.legend(fontsize=legend_fontsize)

filename="Fig_4_b_D2_40_10.pdf"

fig.savefig(filename)

fig.show()
fig.clf()

#Plot receptor activation for D2  receptors for the timecourse of NAcc 
fig=plt.figure()
fig.clf()
time=np.arange(len(D2_40_10[:,0]))
ax1=fig.add_subplot(111)


ax1.set_ylim(-10.0,370.0)
ax1.set_xlim(-50.0,1000.0)
#ax2.set_ylim(ax2_min,ax2_max)

ax1.plot(time/1000.0,D2_50_00[:,0],lw=3,label=r'\textbf{Pattern 50-00}')
ax1.plot(time/1000.0,D2_40_10[:,0],lw=3,label=r'\textbf{Pattern 40-10}')
ax1.plot(time/1000.0,D2_40_00[:,0],lw=3,label=r'\textbf{Pattern 40-00}')


ax1.set_ylabel('[DA] in nM',fontsize=15)

ax1.set_xlabel('Time in s',fontsize=15)

#fig.suptitle('D2 receptor activation - realistic kinetics')

ax1.legend(fontsize=legend_fontsize)
fig.tight_layout()

filename="Fig_4_c_DA_40_10.pdf"

fig.savefig(filename)

fig.show()
fig.clf()


#Plot receptor activation for D2  receptors for the timecourse of NAcc 
fig=plt.figure()
fig.clf()
time=np.arange(len(D2_40_10[:,0]))
ax1=fig.add_subplot(111)


ax1.set_ylim(0.0,300.0)
ax1.set_xlim(0.0,30.0)
#ax2.set_ylim(ax2_min,ax2_max)

ax1.plot(time/1000.0,D2_50_00[:,0],lw=3,label=r'\textbf{Pattern 50-00}')
#ax1.plot(time/1000.0,D2_40_10[:,0],lw=3,label=r'\textbf{Pattern 40-10}')
#ax1.plot(time/1000.0,D2_40_00[:,0],lw=3,label=r'\textbf{Pattern 40-00}')


ax1.set_ylabel('[DA] in nM',fontsize=15)

ax1.set_xlabel('Time in s',fontsize=15)

#fig.suptitle('D2 receptor activation - realistic kinetics')

ax1.legend(fontsize=legend_fontsize)
fig.tight_layout()

filename="Fig_4_d_DA_2_40_10.pdf"

fig.savefig(filename)

fig.show()
fig.clf()







