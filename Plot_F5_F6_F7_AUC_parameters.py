#!/usr/bin/env python
''' This script generates all Panels for Figure 4,5 and 6 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_data script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
from scipy.integrate import simps
from matplotlib.ticker import FormatStrFormatter
import os
nr_steps=5000

my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_3_4_param_study") #make a Data folder with a subfolder for the kinetic data



#This is for the parameters tudy... mostly here we change V_max
D1_sburst_V_samples=[1.0,1.5,2.0,2.5,3.0,3.5,4.0]
D1_sburst_V=[]
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.0_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified ...value is 1
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_2.0_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_2.5_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_3.0_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_3.5_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_4.0_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 

D2_sburst_V_samples=[1.0,1.5,2.0,2.5,3.0,3.5,4.0]
D2_sburst_V=[]
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.0_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified ...value is 1
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_2.0_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_2.5_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_3.0_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_3.5_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_sburst_V.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_4.0_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 

D1_sburst_DAh_samples=[50.0,100.0,150.0,200.0,250.0,300.0,350.0,400.0,500.0,600.0,700.0,800.0,900.0,1000.0]
D1_sburst_DAh=[]
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_50.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified value is 50 nM
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_100.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_150.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_250.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_300.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_350.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_400.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_500.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_600.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_700.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_800.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_900.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified
D1_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_1000.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... DA burst height modified

D2_sburst_DAh_samples=[50.0,100.0,150.0,200.0,250.0,300.0,350.0,400.0,500.0,600.0,700.0,800.0,900.0,1000.0]
D2_sburst_DAh=[]
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_50.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified value is 50 nM
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_100.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_150.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_250.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_300.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_350.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_400.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_500.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_600.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_700.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_800.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_900.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified
D2_sburst_DAh.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_1000.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... DA burst height modified


D1_sburst_rt_samples=[0.2,0.5,1.0,1.5,2.0,3.0,4.0,5.0,6.0,7.0]
D1_sburst_rt=[]
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified value is 0.2s
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.5_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_1.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_1.5_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_2.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_3.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_4.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_5.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_6.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 
D1_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_7.0_200.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... rise time modified 

D2_sburst_rt_samples=[0.2,0.5,1.0,1.5,2.0,3.0,4.0,5.0,6.0,7.0]
D2_sburst_rt=[]
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified value is 0.2s
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.5_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_1.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_1.5_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_2.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_3.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_4.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_5.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_6.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 
D2_sburst_rt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_7.0_200.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... rise time modified 

D1_sburstwp_pt_samples=[0.5,1.0,1.5,2.0,2.5]
D1_sburstwp_pt=[]
D1_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_200.0_0.5.dat"),"rb")))	#name is D1 recep. ... standard-burst with pause... pause time modified value is 0.5s
D1_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_200.0_1.0.dat"),"rb")))	#name is D1 recep. ... standard-burst with pause... pause time modified
D1_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_200.0_1.5.dat"),"rb")))	#name is D1 recep. ... standard-burst with pause... pause time modified
D1_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_200.0_2.0.dat"),"rb")))	#name is D1 recep. ... standard-burst with pause... pause time modified
D1_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_200.0_2.5.dat"),"rb")))	#name is D1 recep. ... standard-burst with pause... pause time modified

D2_sburstwp_pt_samples=[0.5,1.0,1.5,2.0,2.5]
D2_sburstwp_pt=[]
D2_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_200.0_0.5.dat"),"rb")))	#name is D2 recep. ... standard-burst with pause... pause time modified value is 0.5s
D2_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_200.0_1.0.dat"),"rb")))	#name is D2 recep. ... standard-burst with pause... pause time modified
D2_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_200.0_1.5.dat"),"rb")))	#name is D2 recep. ... standard-burst with pause... pause time modified
D2_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_200.0_2.0.dat"),"rb")))	#name is D2 recep. ... standard-burst with pause... pause time modified
D2_sburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_200.0_2.5.dat"),"rb")))	#name is D2 recep. ... standard-burst with pause... pause time modified



#This is kinda stupid ... 
D1_stroburstwp_pt_samples=[0.5,1.0,1.5,2.0,2.5]
D1_stroburstwp_pt=[]
D1_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_500.0_0.5.dat"),"rb")))	#name is D1 recep. ... strong-burst with pause... pause time modified value is 0.5s
D1_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_500.0_1.0.dat"),"rb")))	#name is D1 recep. ... strong-burst with pause... pause time modified
D1_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_500.0_1.5.dat"),"rb")))	#name is D1 recep. ... strong-burst with pause... pause time modified
D1_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_500.0_2.0.dat"),"rb")))	#name is D1 recep. ... strong-burst with pause... pause time modified
D1_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.2_500.0_2.5.dat"),"rb")))	#name is D1 recep. ... strong-burst with pause... pause time modified

D2_stroburstwp_pt_samples=[0.5,1.0,1.5,2.0,2.5]
D2_stroburstwp_pt=[]
D2_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_500.0_0.5.dat"),"rb")))	#name is D2 recep. ... strong-burst with pause... pause time modified value is 0.5s
D2_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_500.0_1.0.dat"),"rb")))	#name is D2 recep. ... strong-burst with pause... pause time modified
D2_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_500.0_1.5.dat"),"rb")))	#name is D2 recep. ... strong-burst with pause... pause time modified
D2_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_500.0_2.0.dat"),"rb")))	#name is D2 recep. ... strong-burst with pause... pause time modified
D2_stroburstwp_pt.append(pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.2_500.0_2.5.dat"),"rb")))	#name is D2 recep. ... strong-burst with pause... pause time modified


#just_pause data
D1_jp_samples=[1.0,2.0,3.0,4.0,5.0]
D1_jp=[]
D1_jp.append(pickle.load(open(os.path.join(res_path,"free_D1_just_pause_1.5_1.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified ...value is 1
D1_jp.append(pickle.load(open(os.path.join(res_path,"free_D1_just_pause_1.5_2.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_jp.append(pickle.load(open(os.path.join(res_path,"free_D1_just_pause_1.5_3.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_jp.append(pickle.load(open(os.path.join(res_path,"free_D1_just_pause_1.5_4.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 
D1_jp.append(pickle.load(open(os.path.join(res_path,"free_D1_just_pause_1.5_5.0.dat"),"rb")))	#name is D1 recep. ... standard-burst ... V is modified 

D1_jp_TBL_samples=[5.0]
D1_jp_TBL=[]
D1_jp_TBL.append(pickle.load(open(os.path.join(res_path,"free_D1_just_pause_1.5_5.0_BL_0.25.dat"),"rb")))

#just_pause data
D2_jp_samples=[1.0,2.0,3.0,4.0,5.0]
D2_jp=[]
D2_jp.append(pickle.load(open(os.path.join(res_path,"free_D2_just_pause_1.5_1.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified ...value is 1
D2_jp.append(pickle.load(open(os.path.join(res_path,"free_D2_just_pause_1.5_2.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_jp.append(pickle.load(open(os.path.join(res_path,"free_D2_just_pause_1.5_3.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_jp.append(pickle.load(open(os.path.join(res_path,"free_D2_just_pause_1.5_4.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 
D2_jp.append(pickle.load(open(os.path.join(res_path,"free_D2_just_pause_1.5_5.0.dat"),"rb")))	#name is D2 recep. ... standard-burst ... V is modified 

D2_jp_TBL_samples=[5.0]
D2_jp_TBL=[]
D2_jp_TBL.append(pickle.load(open(os.path.join(res_path,"free_D2_just_pause_1.5_5.0_BL_0.25.dat"),"rb")))




#Plot maximum of signal against Integral of DA signal
#find maximum of the signals and put it in a list of rise times
time=np.arange(len(D1_sburst_V[0][:,0]))


# maxima and maxima times for rt
nr_samples=len(D1_sburst_rt_samples)
Max_values_D1_rt=np.zeros((nr_samples))
AUC_D1_rt=np.zeros((nr_samples))
Max_values_ab_base_D1_rt=np.zeros((nr_samples))
Max_value_times_D1_rt=np.zeros((nr_samples))
for i,data in enumerate(D1_sburst_rt):
	Max_values_D1_rt[i]=data[:,2].max()
	Max_values_ab_base_D1_rt[i]=data[:,2].max()-data[0,2]
	Max_value_times_D1_rt[i]=data[:,2].argmax()
	#here we try to calculate the AUC for the DA signal
	start=np.argmax(data[:,0]>data[0,0])	#find beginning of pulse
	end=start+np.argmax(data[start:,0]<(1.001*data[0,0]))
	start=start-1
	AUC_D1_rt[i]=simps(data[start:end,0]-data[0,0],time[start:end])
	print start,end,AUC_D1_rt[i]

nr_samples=len(D2_sburst_rt_samples)
Max_values_D2_rt=np.zeros((nr_samples))
AUC_D2_rt=np.zeros((nr_samples))
Max_values_ab_base_D2_rt=np.zeros((nr_samples))
Max_value_times_D2_rt=np.zeros((nr_samples))
for i,data in enumerate(D2_sburst_rt):
	Max_values_D2_rt[i]=data[:,2].max()
	Max_values_ab_base_D2_rt[i]=data[:,2].max()-data[0,2]
	Max_value_times_D2_rt[i]=data[:,2].argmax()
	#here we try to calculate the AUC for the DA signal 
	start=np.argmax(data[:,0]>data[0,0])	#find beginning of pulse
	end=start+np.argmax(data[start:,0]<(1.001*data[0,0]))
	start=start-1
	AUC_D2_rt[i]=simps(data[start:end,0]-data[0,0],time[start:end])
	print start,end,AUC_D2_rt[i]

#maxima and maxima times for DAh
nr_samples=len(D1_sburst_DAh_samples)
Max_values_D1_DAh=np.zeros((nr_samples))
Max_values_ab_base_D1_DAh=np.zeros((nr_samples))
AUC_D1_DAh=np.zeros((nr_samples))
Max_value_times_D1_DAh=np.zeros((nr_samples))
for i,data in enumerate(D1_sburst_DAh):
	Max_values_D1_DAh[i]=data[:,2].max()
	Max_values_ab_base_D1_DAh[i]=data[:,2].max()-data[0,2]
	Max_value_times_D1_DAh[i]=data[:,2].argmax()
	#here we try to calculate the AUC for the DA signal 
	start=np.argmax(data[:,0]>data[0,0])	#find beginning of pulse
	end=start+np.argmax(data[start:,0]<(1.001*data[0,0]))
	start=start-1
	AUC_D1_DAh[i]=simps(data[start:end,0]-data[0,0],time[start:end]) #also subtract baseline activation value 
	print start,end,AUC_D1_DAh[i]
	
	

#maxima and maxima times for DAh
nr_samples=len(D2_sburst_DAh_samples)
Max_values_D2_DAh=np.zeros((nr_samples))
Max_values_ab_base_D2_DAh=np.zeros((nr_samples))
AUC_D2_DAh=np.zeros((nr_samples))
Max_value_times_D2_DAh=np.zeros((nr_samples))
for i,data in enumerate(D2_sburst_DAh):
	Max_values_D2_DAh[i]=data[:,2].max()
	Max_values_ab_base_D2_DAh[i]=data[:,2].max()-data[0,2]
	Max_value_times_D2_DAh[i]=data[:,2].argmax()
	#here we try to calculate the AUC for the DA signal 
	start=np.argmax(data[:,0]>data[0,0])	#find beginning of pulse
	end=start+np.argmax(data[start:,0]<(1.001*data[0,0]))
	start=start-1
	AUC_D2_DAh[i]=simps(data[start:end,0]-data[0,0],time[start:end])	#also subtract baseline activation value 
	print start,end,AUC_D2_DAh[i]

#maxima and maxima times for V
nr_samples=len(D2_sburst_V_samples)
Max_values_D2_V=np.zeros((nr_samples))
Max_values_ab_base_D2_V=np.zeros((nr_samples))
AUC_D2_V=np.zeros((nr_samples))
Max_value_times_D2_V=np.zeros((nr_samples))
for i,data in enumerate(D2_sburst_V):
	Max_values_D2_V[i]=data[:,2].max()
	Max_values_ab_base_D2_V[i]=data[:,2].max()-data[0,2]
	Max_value_times_D2_V[i]=data[:,2].argmax()
	#here we try to calculate the AUC for the DA signal 
	start=np.argmax(data[:,0]>data[0,0])	#find beginning of pulse
	end=start+np.argmax(data[start:,0]<(1.001*data[0,0]))
	start=start-1
	AUC_D2_V[i]=simps(data[start:end,0]-data[0,0],time[start:end])
	print start,end,AUC_D2_V[i]
	
#maxima and maxima times for V
nr_samples=len(D1_sburst_V_samples)
Max_values_D1_V=np.zeros((nr_samples))
Max_values_ab_base_D1_V=np.zeros((nr_samples))
AUC_D1_V=np.zeros((nr_samples))
Max_value_times_D1_V=np.zeros((nr_samples))
for i,data in enumerate(D1_sburst_V):
	Max_values_D1_V[i]=data[:,2].max()
	Max_values_ab_base_D1_V[i]=data[:,2].max()-data[0,2]
	Max_value_times_D1_V[i]=data[:,2].argmax()
	#here we try to calculate the AUC for the DA signal 
	start=np.argmax(data[:,0]>data[0,0])	#find beginning of pulse
	end=start+np.argmax(data[start:,0]<(1.001*data[0,0]))
	start=start-1
	AUC_D1_V[i]=simps(data[start:end,0]-data[0,0],time[start:end])
	print start,end,AUC_D1_V[i]


#maxima and maxima times for pt
nr_samples=len(D1_sburstwp_pt_samples)
Max_values_D1_pt=np.zeros((nr_samples))
Max_values_ab_base_D1_pt=np.zeros((nr_samples))
Max_value_times_D1_pt=np.zeros((nr_samples))
for i,data in enumerate(D1_sburstwp_pt):
	Max_values_D1_pt[i]=data[:,2].max()
	Max_values_ab_base_D1_pt[i]=data[:,2].max()-data[0,2]
	Max_value_times_D1_pt[i]=data[:,2].argmax()

#maxima and maxima times for pt
nr_samples=len(D2_sburstwp_pt_samples)
Max_values_D2_pt=np.zeros((nr_samples))
Max_values_ab_base_D2_pt=np.zeros((nr_samples))
Max_value_times_D2_pt=np.zeros((nr_samples))
for i,data in enumerate(D2_sburstwp_pt):
	Max_values_D2_pt[i]=data[:,2].max()
	Max_values_ab_base_D2_pt[i]=data[:,2].max()-data[0,2]
	Max_value_times_D2_pt[i]=data[:,2].argmax()



#minima and minima times for jp
nr_samples=len(D1_jp_samples)
Min_values_D1_jp=np.zeros((nr_samples))
Min_values_ab_base_D1_jp=np.zeros((nr_samples))
Min_value_times_D1_jp=np.zeros((nr_samples))
for i,data in enumerate(D1_jp):
	Min_values_D1_jp[i]=data[:,2].min()
	Min_values_ab_base_D1_jp[i]=data[:,2].min()-data[0,2]
	Min_value_times_D1_jp[i]=data[:,2].argmin()

#minima and minima times for jp
nr_samples=len(D2_jp_samples)
Min_values_D2_jp=np.zeros((nr_samples))
Min_values_ab_base_D2_jp=np.zeros((nr_samples))
Min_value_times_D2_jp=np.zeros((nr_samples))
for i,data in enumerate(D2_jp):
	Min_values_D2_jp[i]=data[:,2].min()
	Min_values_ab_base_D2_jp[i]=data[:,2].min()-data[0,2]
	Min_value_times_D2_jp[i]=data[:,2].argmin()


#minima and minima times for jp TBL
nr_samples=len(D1_jp_TBL_samples)
Min_values_D1_jp_TBL=np.zeros((nr_samples))
Min_values_ab_base_D1_jp_TBL=np.zeros((nr_samples))
Min_value_times_D1_jp_TBL=np.zeros((nr_samples))
for i,data in enumerate(D1_jp_TBL):
	Min_values_D1_jp_TBL[i]=data[:,2].min()
	Min_values_ab_base_D1_jp_TBL[i]=data[:,2].min()-data[0,2]
	Min_value_times_D1_jp_TBL[i]=data[:,2].argmin()

#minima and minima times for jp TBL
nr_samples=len(D2_jp_TBL_samples)
Min_values_D2_jp_TBL=np.zeros((nr_samples))
Min_values_ab_base_D2_jp_TBL=np.zeros((nr_samples))
Min_value_times_D2_jp_TBL=np.zeros((nr_samples))
for i,data in enumerate(D2_jp_TBL):
	Min_values_D2_jp_TBL[i]=data[:,2].min()
	Min_values_ab_base_D2_jp_TBL[i]=data[:,2].min()-data[0,2]
	Min_value_times_D2_jp_TBL[i]=data[:,2].argmin()










legend_fontsize=15 #set the fontsize in all legends in this script

plt.close('all')
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})


#1		D1 activation for different rise times of ramps
fig=plt.figure()
fig.clf()
#fig.suptitle('D1 Receptor Activation - ramping DA')
time=np.arange(len(D1_sburst_V[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,10.0])
ax1.set_ylim([19.5,25.0])
for i,data in enumerate(D1_sburst_rt):
	if (i%2==0):
		legend_text=r'${:>1.1f} s$'.format(D1_sburst_rt_samples[i])  #old r'$t_{{rise}}={:>1.1f} s$'              
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D1_rt[i])]/1000.0,data[int(Max_value_times_D1_rt[i]),2],'xk',mew=2,ms=12) 


#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D1-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_7_a_D1_param_study_ramps.pdf"
fig.savefig(filename)
fig.clf()



#2             D2 activation for different rise times of ramps
fig=plt.figure()
fig.clf()
#fig.suptitle('D2 Receptor Activation - ramping DA')
time=np.arange(len(D2_sburst_V[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,10.0])
ax1.set_ylim([35.0,44.0])
for i,data in enumerate(D2_sburst_rt):
	if (i%2==0):
		legend_text=r'${:>1.1f} s$'.format(D2_sburst_rt_samples[i]) #'$t_{rise=}$'+str(D2_sburst_rt_samples[i])+'s'
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D2_rt[i])]/1000.0,data[int(Max_value_times_D2_rt[i]),2],'ob',mfc='none',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D2-DA] in nM',fontsize=18)

ax1.yaxis.set_major_formatter(FormatStrFormatter(r'\textbf{{%.1f}}'))
ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_7_a_D2_param_study_ramps.pdf"
fig.savefig(filename)
fig.clf()

#3          DA timecourse for different rise times of ramps with markers for MAX D1 and D2 activation
fig=plt.figure()
fig.clf()

#fig.suptitle('DA concentration - ramping DA')
time=np.arange(len(D2_sburst_V[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,10.0])
ax1.set_ylim([0.0,250.0])
for i,data in enumerate(D2_sburst_rt):
	if i<11:
		if (i%2==0):
			legend_text=r'${:>1.1f} s$'.format(D2_sburst_rt_samples[i]) 
			ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)
			ax1.plot(time[int(Max_value_times_D2_rt[i])]/1000.0,data[int(Max_value_times_D2_rt[i]),0],'ob',mfc='none',mew=2,ms=12) 
			ax1.plot(time[int(Max_value_times_D1_rt[i])]/1000.0,data[int(Max_value_times_D1_rt[i]),0],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_7_a_DA_param_study_ramps.pdf"
fig.savefig(filename)
fig.clf()



#3         D1 activation for different concentrations of bursts
fig=plt.figure()
fig.clf()
#fig.suptitle('D1 Receptor Activation - burst DA')
time=np.arange(len(D1_sburst_DAh[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,5.0])
ax1.set_ylim([19.5,22.0])
for i,data in enumerate(D1_sburst_DAh):
	if i<9:
		if (i%2==0):
			legend_text=r'${:>3.0f} nM$'.format(D1_sburst_DAh_samples[i]) #'$\Delta [DA]^{max}=$'+str(D1_sburst_DAh_samples[i])+'nM'
			ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
			ax1.plot(time[int(Max_value_times_D1_DAh[i])]/1000.0,data[int(Max_value_times_D1_DAh[i]),2],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D1-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize, loc=1)

#fig.show()
filename="Fig_6_a_D1_param_study_burst.pdf"
fig.savefig(filename)
fig.clf()




#4               D2 activation for different concentrations of bursts
fig=plt.figure()
fig.clf()
#fig.suptitle('D2 Receptor Activation - burst DA')
time=np.arange(len(D2_sburst_DAh[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,5.0])
ax1.set_ylim([35.0,40.0])
for i,data in enumerate(D2_sburst_DAh):
	if i<9:
		if (i%2==0):
			legend_text=r'${:>3.0f} nM$'.format(D2_sburst_DAh_samples[i]) #'$\Delta [DA]^{max}=$'+str(D2_sburst_DAh_samples[i])+'nM'
			ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
			ax1.plot(time[int(Max_value_times_D2_DAh[i])]/1000.0,data[int(Max_value_times_D2_DAh[i]),2],'ob',mfc='none',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D2-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize, loc=1)

#fig.show()
filename="Fig_6_a_D2_param_study_burst.pdf"
fig.savefig(filename)
fig.clf()



#4         DA timecourse for different Maximal DA concentrations with markers for positions of maximal D2 and D2 activation
fig=plt.figure()
fig.clf()
#fig.suptitle('DA concentration - burst DA')
time=np.arange(len(D2_sburst_DAh[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,5.0])
for i,data in enumerate(D2_sburst_DAh):
	if i<9:
		if (i%2==0):
			legend_text=r'${:>3.0f} nM$'.format(D2_sburst_DAh_samples[i]) #'$\Delta[DA]^{max}=$'+str(D2_sburst_DAh_samples[i])+'nM'
			ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)
			ax1.plot(time[int(Max_value_times_D2_DAh[i])]/1000.0,data[int(Max_value_times_D2_DAh[i]),0],'ob',mfc='none',mew=2,ms=12) 
			ax1.plot(time[int(Max_value_times_D1_DAh[i])]/1000.0,data[int(Max_value_times_D1_DAh[i]),0],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize, loc=1)

#fig.show()
filename="Fig_6_a_DA_param_study_burst.pdf"
fig.savefig(filename)
fig.clf()













#4++1               D1 activation for different V_max
fig=plt.figure()
fig.clf()
#fig.suptitle('D1 Receptor Activation - differing $V_{max}$')
time=np.arange(len(D1_sburst_V[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,5.0])
ax1.set_ylim([19.5,20.5])
for i,data in enumerate(D1_sburst_V):
	if (D1_sburst_V_samples[i]==1.5): 
		legend_text=r'${:>1.1f} \mu M,\: vStr$'.format(D1_sburst_V_samples[i]) #'$V_{max}=$'+str(D1_sburst_V_samples[i])+'$\mu M$, Nacc'
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D1_V[i])]/1000.0,data[int(Max_value_times_D1_V[i]),2],'xk',mew=2,ms=12) 
	elif (D1_sburst_V_samples[i]==2.5): 
		legend_text=r'${:>1.1f} \mu M$'.format(D1_sburst_V_samples[i]) #'$V_{max}=$'+str(D1_sburst_V_samples[i])+'$\mu M$'
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D1_V[i])]/1000.0,data[int(Max_value_times_D1_V[i]),2],'xk',mew=2,ms=12) 
	elif (D1_sburst_V_samples[i]==4.0): 
		legend_text=r'${:>1.1f} \mu M,\: dStr$'.format(D1_sburst_V_samples[i]) #'$V_{max}=$'+str(D1_sburst_V_samples[i])+'$\mu M$, Str'
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D1_V[i])]/1000.0,data[int(Max_value_times_D1_V[i]),2],'xk',mew=2,ms=12) 



#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D1-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_6_b_D1_param_study_Vmax.pdf"
fig.savefig(filename)
fig.clf()

#4++2               D2 activation for different V_max
fig=plt.figure()
fig.clf()
#fig.suptitle('D2 Receptor Activation - differing $V_{max}$')
time=np.arange(len(D2_sburst_V[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,5.0])
ax1.set_ylim([35.0,37.0])
for i,data in enumerate(D2_sburst_V):
	if (D2_sburst_V_samples[i]==1.5): 
		legend_text=r'${:>1.1f} \mu M,\: vStr$'.format(D2_sburst_V_samples[i])
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D2_V[i])]/1000.0,data[int(Max_value_times_D2_V[i]),2],'ob',mfc='none',mew=2,ms=12)
	elif (D2_sburst_V_samples[i]==2.5): 
		legend_text=r'${:>1.1f} \mu M$'.format(D2_sburst_V_samples[i])
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D2_V[i])]/1000.0,data[int(Max_value_times_D2_V[i]),2],'ob',mfc='none',mew=2,ms=12)
	elif (D2_sburst_V_samples[i]==4.0): 
		legend_text=r'${:>1.1f} \mu M,\: dStr$'.format(D2_sburst_V_samples[i])
		ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
		ax1.plot(time[int(Max_value_times_D2_V[i])]/1000.0,data[int(Max_value_times_D2_V[i]),2],'ob',mfc='none',mew=2,ms=12)

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D2-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_6_b_D2_param_study_Vmax.pdf"
fig.savefig(filename)
fig.clf()


#7          DA timecourse for different V_max with markers for positions of maximal D2 and D2 activation
fig=plt.figure()
fig.clf()

#fig.suptitle('DA concentration - differing $V_{max}$')
time=np.arange(len(D2_sburst_V[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,5.0])
ax1.set_ylim([0.0,250.0])
for i,data in enumerate(D2_sburst_V):
	if (D2_sburst_V_samples[i]==1.5): 
		legend_text=r'${:>1.1f} \mu M,\: vStr$'.format(D2_sburst_V_samples[i]) #'$V_{max}=$'+str(D2_sburst_V_samples[i])+'$\mu M$, Nacc'
		ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)
	elif (D2_sburst_V_samples[i]==2.5): 
		legend_text=r'${:>1.1f} \mu M$'.format(D2_sburst_V_samples[i])
		ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)
	elif (D2_sburst_V_samples[i]==4.0): 
		legend_text=r'${:>1.1f} \mu M,\: dStr$'.format(D2_sburst_V_samples[i])
		ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)

for i,data in enumerate(D2_sburst_V):
	if ((D2_sburst_V_samples[i]==1.5) or (D2_sburst_V_samples[i]==2.5) or (D2_sburst_V_samples[i]==4.0)):
		ax1.plot(time[int(Max_value_times_D2_V[i])]/1000.0,data[int(Max_value_times_D2_V[i]),0],'ob',mfc='none',mew=2,ms=12) 
		ax1.plot(time[int(Max_value_times_D1_V[i])]/1000.0,data[int(Max_value_times_D1_V[i]),0],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_6_b_DA_param_study_Vmax.pdf"
fig.savefig(filename)
fig.clf()











#4++3               D1 activation for different t_pause
fig=plt.figure()
fig.clf()
#fig.suptitle('D1 Receptor Activation - differing $t_{pause}$')
time=np.arange(len(D1_sburstwp_pt[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,10.0])
ax1.set_ylim([19.5,20.5])
for i,data in enumerate(D1_sburstwp_pt):
	legend_text=r'${:>1.1f}s$'.format(D1_sburstwp_pt_samples[i]) #'$t_{pause}=$'+str(D1_sburstwp_pt_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
	ax1.plot(time[int(Max_value_times_D1_pt[i])]/1000.0,data[int(Max_value_times_D1_pt[i]),2],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D1-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize,loc=1)

#fig.show()
filename="Fig_7_b_D1_param_study_t_pause.pdf"
fig.savefig(filename)
fig.clf()



#4++4               D2 activation for different t_pause
fig=plt.figure()
fig.clf()
#fig.suptitle('D2 Receptor Activation - differing $t_{pause}$')
time=np.arange(len(D2_sburstwp_pt[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,10.0])
ax1.set_ylim([35.0,37.0])
for i,data in enumerate(D2_sburstwp_pt):
	legend_text=r'${:>1.1f}s$'.format(D2_sburstwp_pt_samples[i]) #'$t_{pause}=$'+str(D2_sburstwp_pt_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
	ax1.plot(time[int(Max_value_times_D2_pt[i])]/1000.0,data[int(Max_value_times_D2_pt[i]),2],'ob',mfc='none',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D2-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize,loc=1)

#fig.show()
filename="Fig_7_b_D2_param_study_t_pause.pdf"
fig.savefig(filename)
fig.clf()













#7++1          DA timecourse for different t_pause with markers for positions of maximal D2 and D2 activation
fig=plt.figure()
fig.clf()
#fig.suptitle('DA concentration - differing $t_{pause}$')
time=np.arange(len(D2_sburstwp_pt[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,10.0])
ax1.set_ylim([0.0,250.0])
for i,data in enumerate(D2_sburstwp_pt):
	legend_text=r'${:>1.1f}s$'.format(D2_sburstwp_pt_samples[i]) #'$t_{pause}=$'+str(D2_sburstwp_pt_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)

for i,data in enumerate(D2_sburstwp_pt):
	ax1.plot(time[int(Max_value_times_D2_pt[i])]/1000.0,data[int(Max_value_times_D2_pt[i]),0],'ob',mfc='none',mew=2,ms=12) 
	ax1.plot(time[int(Max_value_times_D1_pt[i])]/1000.0,data[int(Max_value_times_D1_pt[i]),0],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_7_b_DA_param_study_t_pause.pdf"
fig.savefig(filename)
fig.clf()



#10             D1 and D2 max_activation against AUC for the burst and ramps 
fig=plt.figure(figsize=(6.4,4.8))
fig.clf()
plt.rc('text', usetex=True)

ax1=fig.add_subplot(111)
ax1.set_xlim([0,800000])
AUC_all=np.concatenate((AUC_D2_DAh,AUC_D2_rt,AUC_D2_V))
Max_val_D2_all=np.concatenate((Max_values_ab_base_D2_DAh,Max_values_ab_base_D2_rt,Max_values_ab_base_D2_V))
coeff_D2_AUC_all=np.polynomial.polynomial.polyfit(AUC_all,Max_val_D2_all,1,full=True)
r_sq_AUC_D2=1.0-coeff_D2_AUC_all[1][0]/sum((Max_val_D2_all-Max_val_D2_all.mean())**2)
f=lambda x: coeff_D2_AUC_all[0][1]*x+coeff_D2_AUC_all[0][0]

ax1.plot(AUC_all,f(AUC_all),"-",lw=3,color='#C0C0C0',alpha=0.7,label="linear fit")
ax1.plot(AUC_D2_DAh,Max_values_ab_base_D2_DAh[:],'+r',label=r'$\Delta [DA]^{max}$')
ax1.plot(AUC_D2_rt,Max_values_ab_base_D2_rt[:],'og',label='$t_{rise}$')
ax1.plot(AUC_D2_V,Max_values_ab_base_D2_V[:],'xk',label='$V_{max}$')
#ax1.set_xlabel('AUC in nMs')
#ax1.set_ylabel('$\Delta [D2-DA]$ in nM')

ax1.yaxis.set_major_formatter(FormatStrFormatter(r'\textbf{{%.1f}}'))
ax1.legend(fontsize=legend_fontsize)
#fig.show()
filename="Fig_5_b_D2_max_act_AUC.pdf"
fig.savefig(filename)

fig.clf()



fig=plt.figure(figsize=(6.4,4.8))
fig.clf()
#plt.rc('text', usetex=True)

ax1=fig.add_subplot(111)
ax1.set_xlim([0,800000])
AUC_all=np.concatenate((AUC_D1_DAh,AUC_D1_rt,AUC_D1_V))
Max_val_D1_all=np.concatenate((Max_values_ab_base_D1_DAh,Max_values_ab_base_D1_rt,Max_values_ab_base_D1_V))
coeff_D1_AUC_all=np.polynomial.polynomial.polyfit(AUC_all,Max_val_D1_all,1,full=True)
r_sq_AUC_D1=1.0-coeff_D1_AUC_all[1][0]/sum((Max_val_D1_all-Max_val_D1_all.mean())**2)
f=lambda x: coeff_D1_AUC_all[0][1]*x+coeff_D1_AUC_all[0][0]

ax1.plot(AUC_all,f(AUC_all),"-",lw=3,color='#C0C0C0',alpha=0.7,label="linear fit")
ax1.plot(AUC_D1_DAh,Max_values_ab_base_D1_DAh[:],'+b',label="$\Delta [DA]^{max}$")
ax1.plot(AUC_D1_rt,Max_values_ab_base_D1_rt[:],'oy',label="$t_{rise}$")
ax1.plot(AUC_D1_V,Max_values_ab_base_D1_V[:],'xm',label="$V_{max}$")
#ax1.set_xlabel('AUC in nMs')
#ax1.set_ylabel('$\Delta [D1-DA]$ in nM')



ax1.legend(fontsize=legend_fontsize)
#fig.show()
filename="Fig_5_a_D1_max_act_AUC.pdf"
fig.savefig(filename)
fig.clf()



#11              D1 activation for different t_pause in just pause
fig=plt.figure(figsize=(6.4,4.8))
fig.clf()
#fig.suptitle('D1 Receptor Activation - differing $t_{pause}$')
time=np.arange(len(D1_jp[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,30.0])
ax1.set_ylim([18.5,20.5])
for i,data in enumerate(D1_jp):
	legend_text=r'${:>1.1f}s$'.format(D1_jp_samples[i]) #'$t_{pause}=$'+str(D1_jp_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)

for i,data in enumerate(D1_jp_TBL):
	legend_text=r'${:>1.1f}s$, $QBL$'.format(D1_jp_TBL_samples[i]) #'$t_{pause}=$'+str(D1_jp_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,2],'--',color='C4',lw=3,label=legend_text,alpha=0.7,zorder=0)

for i,data in enumerate(D1_jp):
	ax1.plot(time[int(Min_value_times_D1_jp[i])]/1000.0,data[int(Min_value_times_D1_jp[i]),2],'xk',mew=2,ms=12) 
    
for i,data in enumerate(D1_jp_TBL):
	ax1.plot(time[int(Min_value_times_D1_jp_TBL[i])]/1000.0,data[int(Min_value_times_D1_jp_TBL[i]),2],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D1-DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_7_c_D1_param_study_j_pause.pdf"
fig.savefig(filename)
fig.clf()


#11++1              D2 activation for different t_pause in just pause
fig=plt.figure(figsize=(6.4,4.8))
fig.clf()
#fig.suptitle('D2 Receptor Activation - differing $t_{pause}$')
time=np.arange(len(D2_jp[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,30.0])
ax1.set_ylim([33.5,36.5])
for i,data in enumerate(D2_jp):
	legend_text=r'${:>1.1f}s$'.format(D2_jp_samples[i]) #'$t_{pause}=$'+str(D2_jp_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,2],lw=3,label=legend_text)
    
for i,data in enumerate(D2_jp_TBL):
	legend_text=r'${:>1.1f}s$, $QBL$'.format(D2_jp_TBL_samples[i]) #'$t_{pause}=$'+str(D1_jp_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,2],'--',color='C4',lw=3,label=legend_text,alpha=0.7,zorder=0)
    
for i,data in enumerate(D2_jp_TBL):
    ax1.plot(time[int(Min_value_times_D2_jp_TBL[i])]/1000.0,data[int(Min_value_times_D2_jp_TBL[i]),2],'ob',mfc='none',mew=2,ms=12) 

for i,data in enumerate(D2_jp):
    ax1.plot(time[int(Min_value_times_D2_jp[i])]/1000.0,data[int(Min_value_times_D2_jp[i]),2],'ob',mfc='none',mew=2,ms=12)    
	

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[D2-DA] in nM',fontsize=18)

#ax1.yaxis.set_major_formatter(FormatStrFormatter(r'\textbf{{%.1f}}'))
ax1.legend(fontsize=legend_fontsize,loc=1)

#fig.show()
filename="Fig_7_c_D2_param_study_j_pause.pdf"
fig.savefig(filename)
fig.clf()


#11++2              DA for different t_pause in just pause
fig=plt.figure(figsize=(6.4,4.8))
fig.clf()
#fig.suptitle('[DA] - differing $t_{pause}$')
time=np.arange(len(D2_jp[0][:,0]))
ax1=fig.add_subplot(111)
ax1.set_xlim([0.0,30.0])
ax1.set_ylim([0.0,250.0])
for i,data in enumerate(D2_jp):
	legend_text=r'${:>1.1f}s$'.format(D2_jp_samples[i])
	ax1.plot(time/1000.0,data[:,0],lw=3,label=legend_text)
    
for i,data in enumerate(D2_jp_TBL):
	legend_text=r'${:>1.1f}s$, $QBL$'.format(D2_jp_TBL_samples[i]) #'$t_{pause}=$'+str(D1_jp_samples[i])+'s'
	ax1.plot(time/1000.0,data[:,0],'--',color='C4',lw=3,label=legend_text,alpha=0.7,zorder=0)

for i,data in enumerate(D2_jp):
    ax1.plot(time[int(Min_value_times_D2_jp[i])]/1000.0,data[int(Min_value_times_D2_jp[i]),0],'ob',mfc='none',mew=2,ms=12) 
    ax1.plot(time[int(Min_value_times_D1_jp[i])]/1000.0,data[int(Min_value_times_D1_jp[i]),0],'xk',mew=2,ms=12) 
    
for i,data in enumerate(D2_jp_TBL):
    ax1.plot(time[int(Min_value_times_D2_jp_TBL[i])]/1000.0,data[int(Min_value_times_D2_jp_TBL[i]),0],'ob',mfc='none',mew=2,ms=12) 
    ax1.plot(time[int(Min_value_times_D1_jp_TBL[i])]/1000.0,data[int(Min_value_times_D1_jp_TBL[i]),0],'xk',mew=2,ms=12) 

#ax1.set_xlabel('Time in s',fontsize=18)
#ax1.set_ylabel('[DA] in nM',fontsize=18)


ax1.legend(fontsize=legend_fontsize)

#fig.show()
filename="Fig_7_c_DA_param_study_j_pause.pdf"
fig.savefig(filename)
fig.clf()





