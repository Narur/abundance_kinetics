This set of scripts is accompanying the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
They can be used to reproduce all the figures that are presented in the afforementioned publication.
Most of the figures do not come with axis labels, since those where added when collecting the figures into the larger compound figures. 
(Usually the label commands are just commented out). All these scripts are scripts for python2.7 and use some common modules (numpy, os, pickle).
All these scripts have been tested under Debian8, Ubuntu 18.04 and Windows 10 (with Anaconda), but should work under most OS (fingers crossed).

To reproduce the paper figures the following scripts should be run (in this order):
1)Make_the_data.py -> Generates the data for the figures (except the ROC figures) (Takes around 30 minutes) (typing Make_the_data.py -h will give some options to skip generating some time intensive data items)

2)Plot_F1_F10_a_b.py ->These plot most of the figures. Scripts titled F plot parts of the given figure.
Plot_F1_F10_c_d.py
Plot_F2_F11_a.py
Plot_F2_F11_b.py
Plot_F3_pauses.py
Plot_F4_long_mix.py
Plot_F5_F6_F7_AUC_parameters.py
Plot_F9_kinetics.py 

3)Make_the_data_ROC.py -> Generates the behavioral sequences for Figure8. However this version only generates 10 samples, For the figure in the paper we generated 500 samples. If more samples are supposed to be generated the script can be called with the parameter -n NRSAMPLES to set the target number of samples. However it should be noted that the runtime of this script with
10 samples is already around 60 minutes. and generates around 20GB of data. (which should scale proportionally with the number of samples) 

4)Plot_F8_a_c_realizations.py -> Realizations can be plotted before the classifier is run. This will plot Realization nr. 9. The seed for the Random number generator is the same as we used in the paper, so it will most likely look the same. However the RNG implementation might be different, which will lead to a slightly different look of the plotted realization.

5) Make_all_means.py -> Calculates the mean signal from the behavioral sequences and applies the classifier (runtime around 30 minutes for 10 samples). If nr_samples has been changed from the default in Make_the_data_ROC.py this should be changed in this script too. (same option -n)

6) Plot_F8_b_d_e_f_ROC_ACC.py -> Generates the ROC figures. If nr_samples has been changed from the default in Make_the_data_ROC.py this should be changed in this script too. (again option -n)
