#!/usr/bin/env python
''' This script generates all Panels for Figure 3 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_data script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
import os
nr_steps=5000

#Where am I and where are the data files ? (assuming the data files have been made with the Make_the Data script)
my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_2_t_pause") #make a Data folder with a subfolder for the kinetic data


#Plot the transient without pause for STR and Naxx
#D1_burst=pickle.load(open("free_D1_burst_1.5_0.2_200.0.dat","rb"))
D1_BL=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_150.0_2.5_BL_0.25.dat"),"rb"))
D1_wp_15=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.3.dat"),"rb"))
D1_wp_20=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_150.0_2.0.dat"),"rb"))
D1_DAh_80=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_60.0_0.8.dat"),"rb"))


#D2_burst=pickle.load(open("free_D2_burst_1.5_0.2_200.0.dat","rb"))
D2_BL=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_150.0_2.5_BL_0.25.dat"),"rb"))
D2_wp_15=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.3.dat"),"rb"))
D2_wp_20=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_150.0_2.0.dat"),"rb"))
D2_DAh_80=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_60.0_0.8.dat"),"rb"))




plt.close('all')

#This is the plot with 3 different pause duration/DA_change combinations ... Its a supplemental figure
plt.close('all')
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

#1   Plot receptor activation for D1  receptors for a pause of 1.6 seconds  ... canceling the short burst
fig=plt.figure(figsize=(6.4,4.8))
fig.clf()
time=np.arange(len(D1_wp_15[:,0]))
matplotlib.rcParams.update({'font.size': 12})

#fig.suptitle('Receptor activation for different pause parameters')
ax1=fig.add_subplot(6,1,(3,4))
ax1.set_xlim(0.5,4.0)
ax1.set_ylim(19.7,20.1)

#aim_D1=np.ones(len(D1_wp_20[:,2]))*D1_wp_20[0,2]
ax1.plot(time/1000.0,D1_BL[:,2],'--',lw=3,color='C3',label=r'$t_{pause}=2.5s$, $[DA]^{max}=150 nM$',alpha=0.7)
ax1.plot(time/1000.0,D1_wp_20[:,2],lw=3,color='C0',label=r'$t_{pause}=2.0s$, $[DA]^{max}=150 nM$')
ax1.plot(time/1000.0,D1_wp_15[:,2],lw=3,color='C1',label=r'$t_{pause}=1.3s$, $[DA]^{max}=100 nM$')
ax1.plot(time/1000.0,D1_DAh_80[:,2],lw=3,color='C2',label=r'$t_{pause}=0.8s$, $[DA]^{max}=60 nM$')
#ax1.plot(time/1000.0,aim_D1,lw=3,color='k',alpha=0.5)

#ax1.set_ylabel('[D1-DA] nM',fontsize=14)
ax1.set_xticklabels([])
#ax1.legend()

ax2=fig.add_subplot(6,1,(5,6))
ax2.set_xlim(0.5,4.0)
ax2.set_ylim(35.5,36.2)

#aim_D2=np.ones(len(D2_wp_20[:,2]))*D2_wp_20[0,2]
ax2.plot(time/1000.0,D2_BL[:,2],'--',lw=3,color='C3',label=r'$t_{pause}=0.8s$, $[DA]^{max}=60 nM$',alpha=0.7)
ax2.plot(time/1000.0,D2_wp_20[:,2],lw=3,color='C0',label='$t_{pause}=1.8s$, $[DA]^{max}=150 nM$')
ax2.plot(time/1000.0,D2_wp_15[:,2],lw=3,color='C1',label='$t_{pause}=1.3s$, $[DA]^{max}=100 nM$')
ax2.plot(time/1000.0,D2_DAh_80[:,2],lw=3,color='C2',label='$t_{pause}=0.8s$, $[DA]^{max}=60 nM$')
#ax2.plot(time/1000.0,aim_D2,lw=3,color='k',alpha=0.5)

#ax2.set_ylabel('[D2-DA] nM',fontsize=14)
#ax2.set_xlabel('Time in s',fontsize=15)
#ax2.set_xticklabels([])
#ax2.legend()

ax3=fig.add_subplot(6,1,(1,2))
ax3.set_xlim(0.5,4.0)
ax3.set_ylim(0.0,200.0)

ax3.plot(time/1000.0,D2_BL[:,0],'--',lw=3,color='C3',label=r'$t_{pause}=0.8s$, $[DA]^{max}=60 nM$',alpha=0.7)
ax3.plot(time/1000.0,D2_wp_20[:,0],lw=3,color='C0',label='$t_{pause}=1.8s$, $[DA]^{max}=150 nM$')
ax3.plot(time/1000.0,D2_wp_15[:,0],lw=3,color='C1',label='$t_{pause}=1.3s$, $[DA]^{max}=100 nM$')
ax3.plot(time/1000.0,D2_DAh_80[:,0],lw=3,color='C2',label='$t_{pause}=0.8s$, $[DA]^{max}=60 nM$')


#ax3.set_ylabel('[DA] nM',fontsize=14)
ax3.set_xticklabels([])
#ax3.set_xlabel('Time in s',fontsize=15)
#ax3.legend()
#fig.text(0.03, 0.5, 'Concentration in nM',fontsize=15, va='center', rotation='vertical')
ax3.text(0.349, 0.8, r'$\Delta [DA]=150 nM$, $t_{p}=2.0s$',color='C0',transform=ax3.transAxes, fontsize=14)
ax3.text(0.349, 0.59, r'$\Delta [DA]=100 nM$, $t_{p}=1.3s$',color='C1',transform=ax3.transAxes, fontsize=14)
ax3.text(0.351, 0.38, r'$\Delta [DA]=\ 60 nM$, $t_{p}=0.8s$',color='C2',transform=ax3.transAxes, fontsize=14)
ax3.text(0.351, 0.17, r'$\Delta [DA]=150 nM$, $t_{p}=2.5s$, $QBL$',color='C3',transform=ax3.transAxes, fontsize=14,alpha=0.7)


filename="Fig_3_DA_mult_pauses.pdf"

fig.savefig(filename)

fig.show()
fig.clf()






















