#!/usr/bin/env python
'''
    This file is part of the scripts accompanying the 
    2018 Hunger, Kumar, Schmidt Paper titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations"   
    This file contains the Classifier used in the paper, and the routines that
    calculate the means over multiple behaviorial sequences. 
    
    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np
import matplotlib.pyplot as plt
import random
import math as m
import cPickle as pickle
import os





def Classify(D_switch, switch, p_1 ,p_2 ,mean_1 ,mean_2 ,nr_samples , results): # p_1 Probability of first mean, high or mid, p_2 probability of second mean mid or low, mean_1 ,mean_2 corresponding arrays with the mean timelines for the given p_1 and p_2, result, timeline of true and false positives  
    
    # First run all the p_1 samples (supposedly high)
    for i in xrange(nr_samples):    
        #filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+str(p_1)+"_"+str(1.0-p_1)+"_0.0_50_"+str(i+1)+".dat"
        if (switch=="prob_jitter_DA_decrease"):
            filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_1)+"_"+"{:.2f}".format(0.0)+"_0.00_50_"+str(i)+".dat"
        else:
            filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_1)+"_"+"{:.2f}".format(1.0-p_1)+"_0.00_50_"+str(i)+".dat"
        
        D1=pickle.load(open(filename,"rb"))     
        a=abs(mean_1[:,2]-D1[:,2])  #calculate distance to the mean values
        b=abs(mean_2[:,2]-D1[:,2])
        idx=(a<=b)  #choose all the timepoints where the timeline is closer to a then to b... that means it would be classified as a
        results[idx,0]=results[idx,0]+1 #add one to all timepoints with True_high identification
        results[~idx,3]=results[~idx,3]+1   #add one to all timepoints with False_low identification (high but identified as low)
        
    # Now run all the samples with p_2 (supposed to be low prob)
    for i in xrange(nr_samples):
        if (switch=="prob_jitter_DA_decrease"):
            filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_2)+"_"+"{:.2f}".format(0.0)+"_0.00_50_"+str(i)+".dat"
        else:
            filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_2)+"_"+"{:.2f}".format(1.0-p_2)+"_0.00_50_"+str(i)+".dat"
        
        
        D1=pickle.load(open(filename,"rb"))     
        a=abs(mean_2[:,2]-D1[:,2])  #calculate distance to the mean values
        b=abs(mean_1[:,2]-D1[:,2])
        idx=(a<b)   #choose all the timepoints where the timeline is closer to a then to b... that means it would be classified as a... in this case a is mean_2 
        results[idx,2]=results[idx,2]+1 #add one to all timepoints with True_low identification
        results[~idx,1]=results[~idx,1]+1   #add one to all timepoints with False_high identification (Low but identified as high)

def Classify_triple(D_switch, switch, p_1 ,p_2 ,p_3 ,mean_1 ,mean_2 ,mean_3 ,nr_samples , results): # p_1 Probability of first mean, high or mid, p_2 probability of second mean mid or low, mean_1 ,mean_2 corresponding arrays with the mean timelines for the given p_1 and p_2, result, timeline of true and false positives  
    #results has True high, false high, 
    
    # First run all the p_1 samples (supposedly high)
    for i in xrange(nr_samples):    
        filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_1)+"_"+"{:.2f}".format(1.0-p_1)+"_0.00_50_"+str(i)+".dat"
        D1=pickle.load(open(filename,"rb"))     
        a=abs(mean_1[:,2]-D1[:,2])  #calculate distance to the mean values
        b=abs(mean_2[:,2]-D1[:,2])
        c=abs(mean_3[:,2]-D1[:,2])
        
        #True high
        idx=((a<=b) & (a<=c))   #choose all the timepoints where the timeline is closer to a and c -> these are high that are identified as high
        results[idx,0]=results[idx,0]+1 #add one to all timepoints with True_high identification
        
        #High identified as middle 
        idx=((b<a) & (b<c))
        results[idx,4]=results[idx,4]+1
        
        #high identified as low
        idx=((c<a) & (c<b))
        results[idx,7]=results[idx,7]+1 #add one to all timepoints with False_low identification (high but identified as high)
        

    # Now run all the p_2 samples (supposedly middle)
    for i in xrange(nr_samples):    
        filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_2)+"_"+"{:.2f}".format(1.0-p_2)+"_0.00_50_"+str(i)+".dat"
        D1=pickle.load(open(filename,"rb"))     
        a=abs(mean_2[:,2]-D1[:,2])  #calculate distance to the mean values
        b=abs(mean_1[:,2]-D1[:,2])
        c=abs(mean_3[:,2]-D1[:,2])
        
        #True middle
        idx=((a<=b) & (a<=c))   #choose all the timepoints where the timeline is closer to a and c -> these are high that are identified as high
        results[idx,3]=results[idx,3]+1 #add one to all timepoints with True_high identification
        
        #middle identified as hihgh 
        idx=((b<a) & (b<c))
        results[idx,1]=results[idx,1]+1
        
        #middle identified as low
        idx=((c<a) & (c<b))
        results[idx,8]=results[idx,8]+1 #add one to all timepoints with False_low identification (high but identified as high)
    
    
    # Now run all the p_3 samples (supposedly low)
    for i in xrange(nr_samples):    
        filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_3)+"_"+"{:.2f}".format(1.0-p_3)+"_0.00_50_"+str(i)+".dat"
        D1=pickle.load(open(filename,"rb"))     
        a=abs(mean_3[:,2]-D1[:,2])  #calculate distance to the mean values
        b=abs(mean_1[:,2]-D1[:,2])
        c=abs(mean_2[:,2]-D1[:,2])
        
        #True low
        idx=((a<=b) & (a<=c))   #choose all the timepoints where the timeline is closer to a and c -> these are high that are identified as high
        results[idx,6]=results[idx,6]+1 #add one to all timepoints with True low
        
        #low identified as hihgh 
        idx=((b<a) & (b<c))
        results[idx,2]=results[idx,2]+1
        
        #low identified as middle
        idx=((c<a) & (c<b))
        results[idx,5]=results[idx,5]+1 #add one to all timepoints with False_low identification (high but identified as high)
        











def Make_avg_classify_grid(D_switch="D1",switch="prob_jitter",nr_samples=10):
    p_rew=np.arange(0,1.05,0.1) #rewarded values
    p_rew2=np.arange(0,1.05,0.1)    #rewarded values
    comp_one_one={}


    for p1 in p_rew:    #compard too
        ############### Read the mean for the high dataset 
        if (switch=="prob_jitter_DA_decrease"):
            filename="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p1)+"_"+"{:.2f}".format(0.0)+"_0.00_50.dat"        
        else:
            filename="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p1)+"_"+"{:.2f}".format(1.0-p1)+"_0.00_50.dat"
        #print filename
        a=pickle.load(open(filename,"rb"))
        mean_1=np.array(a)

        for p2 in p_rew2:   #compare this one 
            if (p1>=p2):    #only if p1 >p2 to not do the same comparisons twice because 0.9 vs 0.1 should be the same as 0.1 vs 0.9
                ############### Read the mean for the 2nd dataset 
                if (switch=="prob_jitter_DA_decrease"):
                    if (p1==p2):
                        filename="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p2)+"_"+"{:.2f}".format(0.0)+"_0.00_50_r2.dat"
                    else:
                        filename="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p2)+"_"+"{:.2f}".format(0.0)+"_0.00_50.dat"
                else:
                    if (p1==p2):
                        filename="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p2)+"_"+"{:.2f}".format(1.0-p2)+"_0.00_50_r2.dat"
                    else:
                        filename="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p2)+"_"+"{:.2f}".format(1.0-p2)+"_0.00_50.dat"
                #print filename
                a=pickle.load(open(filename,"rb"))
                mean_2=np.array(a)      

                
                #now we read the means for the original and the to be compared to data ... now we have to add the dictionary element
                key="{:.2f}".format(100.0*p1)+"_"+"{:.2f}".format(100.0*p2)
                comp_one_one[key]=np.zeros((len(mean_1[:,2]),4),dtype=np.intc ) #both mean lists should be same length
                #now we do the actual classification, only if p1>p2 so we don't do the work twice
                Classify(D_switch, switch, p1 , p2 ,mean_1 ,mean_2 ,nr_samples , comp_one_one[key])
                
                
                print "I just finished",p1 , p2





    fn="Classify_"+str(D_switch)+"_"+str(switch)+"_grid_10_space_r2.dat"
    pickle.dump( comp_one_one, open( fn, "wb" ),protocol=-1 )



def Make_avg(D_switch="D1",switch="prob_jitter",p_burst=0.5,p_buwp=0.5,p_pause=0.0,nr_samples=10,nr_steps=5000):
    '''Calculates the average over the nr_samples given sequences with same p_burst/p_buwp and p_pause. '''
    half=int(nr_samples/2)

    #we write a mean out after half of the samples... because we want the half_mean in case we compare the same probabilities to each other in the classifier... so that it comes out as 0.5/0.5 instead of 100%
    D1_samples=range(1,half)

    filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_burst)+"_"+"{:.2f}".format(p_buwp)+"_"+"{:.2f}".format(p_pause)+"_50_0.dat"
    D1=pickle.load(open(filename,"rb"))	

    for i in D1_samples:	
        filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_burst)+"_"+"{:.2f}".format(p_buwp)+"_"+"{:.2f}".format(p_pause)+"_50_"+str(i)+".dat"
        D1+=pickle.load(open(filename,"rb"))	

    a=D1/float(half)

    fn="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p_burst)+"_"+"{:.2f}".format(p_buwp)+"_"+"{:.2f}".format(p_pause)+"_50_r2.dat"
    pickle.dump( a, open( fn, "wb" ),protocol=-1 )


    D1_samples=range(half,nr_samples)
    for i in D1_samples:	
        filename="free_"+str(D_switch)+"_reward_states_"+str(switch)+"_"+"{:.2f}".format(p_burst)+"_"+"{:.2f}".format(p_buwp)+"_"+"{:.2f}".format(p_pause)+"_50_"+str(i)+".dat"
        D1+=pickle.load(open(filename,"rb"))	

    a=D1/float(nr_samples)

    fn="mean_std_"+str(D_switch)+"_"+str(switch)+"_"+"{:.2f}".format(p_burst)+"_"+"{:.2f}".format(p_buwp)+"_"+"{:.2f}".format(p_pause)+"_50.dat"
    pickle.dump( a, open( fn, "wb" ), protocol=-1 )




