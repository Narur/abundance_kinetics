#!/usr/bin/env python
''' This script generates Panel a for Figure 2 and Figure 11 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_data script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''



import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
import os
nr_steps=5000



#Where am I and where are the data files ? (assuming the data files have been made with the Make_the Data script)
my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Fig_1_efg") #make a Data folder with a subfolder for the kinetic data


#Plot the transient without pause for STR and Naxx
D1=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.3.dat"),"rb"))
D1_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.3_Kd.dat"),"rb"))
D1_s2=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.3_frac_s2_0.1.dat"),"rb"))
D1_Kd_s2=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.3_frac_s2_0.1_Kd.dat"),"rb"))

D2=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.3.dat"),"rb"))
D2_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.3_Kd.dat"),"rb"))
D2_s2=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.3_frac_s2_0.1.dat"),"rb"))
D2_Kd_s2=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.3_frac_s2_0.1_Kd.dat"),"rb"))


#Make The long bursts
D1_lb=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0.dat"),"rb"))
D2_lb=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0.dat"),"rb"))
D1_lb_s2=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0_frac_s2_0.1.dat"),"rb"))
D2_lb_s2=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0_frac_s2_0.1.dat"),"rb"))


D1_Kd_lb=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0_Kd.dat"),"rb"))
D2_Kd_lb=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0_Kd.dat"),"rb"))
D1_Kd_lb_s2=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0_frac_s2_0.1_Kd.dat"),"rb"))
D2_Kd_lb_s2=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0_frac_s2_0.1_Kd.dat"),"rb"))








plt.close('all')


#Plot receptor activation for D1  receptors for the timecourse of NAcc
fig=plt.figure(figsize=(6.2,14.35))
fig.clf()
#fig.suptitle('Concentration of active receptor')
plt.rc('text', usetex=True)	#use latex for all tex, that makes the font the same since some figures require us to use Latex
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']

matplotlib.rcParams.update({'font.size': 18})
#matplotlib.rcParams.update({"font.weight" : "bold"})
#matplotlib.rcParams.update({"axes.labelweight" : "bold"})
#matplotlib.rcParams.update({"axes.titleweight" : "bold"})


time=np.arange(-750,len(D1[:,0])-750)
ax1=fig.add_subplot(8,1,(3,4))
ax2=ax1.twinx()
ax1.set_xlim(0.0,2.0)
ax2_min=0.0		#minimum of axis 2 with the K_D kinetics
ax2_max=200.0	#maximum of axis 2 with the K_D kinetics
ax1_max=20.8		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax1_min=ax1_max-((ax1_max-D1[0,2])/(ax2_max-D1[0,2]))*(ax2_max-ax2_min)


ax1.set_ylim(ax1_min,ax1_max)
ax2.set_ylim(ax2_min,ax2_max)


ax1.plot(time/1000.0,D1_lb[:,2],'-',lw=3,label='D1',color='C2')
ax1.plot(time/1000.0,D1[:,2],'-',lw=3,label='D1',color='C4')
ax1.plot(time[D1_lb[:,2].argmax()]/1000.0,D1_lb[:,2].max(),'x',ms=12,color='#000000',mew=2)

ax2.spines['left'].set_color('k')
ax1.yaxis.label.set_color('k')
ax1.tick_params(axis='y',colors='k')

ax2.plot(time/1000.0,D1_Kd_lb[:,2],"--",lw=3,label='D1',color='#C0C0C0')
ax2.plot(time/1000.0,D1_Kd[:,2],"--",lw=3,label='D1',color='#A9A9A9')
ax2.plot(time[D1_Kd_lb[:,2].argmax()]/1000.0,D1_Kd_lb[:,2].max(),'x',ms=12,color='#ff2ace',mew=2)

ax2.spines['right'].set_color('#A9A9A9')
ax2.yaxis.label.set_color('#A9A9A9')
ax2.tick_params(axis='y',colors='#A9A9A9')
#ax1.text(0.75, 0.75, r'D1',transform=ax1.transAxes, fontsize=15)

#ax1.set_xlabel('Time in s',fontsize=18)
ax1.set_xticklabels([])
ax2.set_xticklabels([])

h1, l1=ax1.get_legend_handles_labels()
h2, l2=ax2.get_legend_handles_labels()

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas'

#ax1.legend(h1+h2,l1+l2)


ax3=fig.add_subplot(8,1,(5,6))
ax4=ax3.twinx()
ax3.set_xlim(0.0,2.0)
ax4_min=0.0		#minimum of axis 2 with the K_D kinetics
ax4_max=100.0	#maximum of axis 2 with the K_D kinetics
ax3_max=37.0		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax3_min=ax3_max-((ax3_max-D2[0,2])/(ax4_max-D2[0,2]))*(ax4_max-ax4_min)


ax3.set_ylim(ax3_min,ax3_max)
ax4.set_ylim(ax4_min,ax4_max)

ax3.plot(time/1000.0,D2_lb[:,2],'-',lw=3,label='D2',color='C2')
ax3.plot(time/1000.0,D2[:,2],'-',lw=3,label='D2',color='C4')

ax4.plot(time/1000.0,D2_Kd_lb[:,2],'--',lw=3,label='D2',color='#C0C0C0')
ax4.plot(time/1000.0,D2_Kd[:,2],'--',lw=3,label='D2',color='#A9A9A9')

ax3.plot(time[D2_lb[:,2].argmax()]/1000.0,D2_lb[:,2].max(),'o',mfc='none',ms=12,color='#000000',mew=2)
ax4.plot(time[D2_Kd_lb[:,2].argmax()]/1000.0,D2_Kd_lb[:,2].max(),'o',mfc='none',ms=12,color='#ff2ace',mew=2)

#ax3.set_ylabel('Real kinetics - Concentration in nM',fontsize=15)
ax3.spines['left'].set_color('k')
ax3.yaxis.label.set_color('k')
ax3.tick_params(axis='y',colors='k')
#ax3.text(0.75, 0.75, r'D2',transform=ax3.transAxes, fontsize=15)

#ax4.set_ylabel('Instant kinetics - Concentration in nM',fontsize=15)
ax4.spines['right'].set_color('#A9A9A9')
ax4.yaxis.label.set_color('#A9A9A9')
ax4.tick_params(axis='y',colors='#A9A9A9')

#ax3.set_xticklabels([])
#ax4.set_xticklabels([])

#ax3.set_xlabel('Time in s',fontsize=18)

ax3.set_zorder(ax4.get_zorder()+1) # put ax in front of ax2
ax3.patch.set_visible(False) # hide the 'canvas'

ax3.set_xticklabels([])
ax4.set_xticklabels([])


#DA_conc
ax5=fig.add_subplot(8,1,(1,2))
ax5.set_xlim(0.0,2.0)
ax5.plot(time/1000.0,D1_lb[:,0],lw=3,color='C2')
ax5.plot(time/1000.0,D1[:,0],lw=3,color='C4')


#Plot the maximum dots into the Dopamine curve
ax5.plot(time[D2_lb[:,2].argmax()]/1000.0,D2_lb[D2_lb[:,2].argmax(),0],'o',ms=12,mfc='none',color='#000000',mew=2)
ax5.plot(time[D1_lb[:,2].argmax()]/1000.0,D2_lb[D1_lb[:,2].argmax(),0],'x',ms=12,color='#000000',mew=2)

ax5.plot(time[D2_Kd_lb[:,2].argmax()]/1000.0,D2_lb[D2_Kd_lb[:,2].argmax(),0],'o',ms=12,mfc='none',color='#ff2ace',mew=2)
ax5.plot(time[D1_Kd_lb[:,2].argmax()]/1000.0,D2_lb[D1_Kd_lb[:,2].argmax(),0],'x',ms=12,color='#ff2ace',mew=2)

#ax5.plot(time[D1[:,2].argmax()]/1000.0,D1[D1[:,2].argmax(),0],'+',ms=10,color='#ff2ace',lw=3)



#ax5.set_ylabel('[DA] in nM',fontsize=13)
ax5.set_xticklabels([])
#ax5.text(0.75, 0.75, r'DA',transform=ax5.transAxes, fontsize=15)
#ax5.legend()


h1, l1=ax3.get_legend_handles_labels()
h2, l2=ax4.get_legend_handles_labels()
#ax3.legend(h1+h2,l1+l2)

ax5.text(0.479, 0.55, r'Long burst',color='C2',transform=ax5.transAxes, fontsize=18)
ax5.text(0.479, 0.35, r'Burst and pause',color='C4',transform=ax5.transAxes, fontsize=18)




#relative activation to baseline
ax6=fig.add_subplot(8,1,(7,8))
ax6.set_xlim(0.0,2.0)
ax6.set_ylim(0.98,1.03)

#ax6.yticks([0.98,1.0,1.02],['-2%','0%','+2%'])
ax6.set_yticks([0.98,1.0,1.02])
#ax6.set_yticklabels([r'-2\%',r'0\%',r'+2\%'])


ax6.plot(time/1000.0,D1_lb[:,2]/D1_lb[0,2],'-',lw=3,label='D1',color='C1',alpha=1.0)
ax6.plot(time/1000.0,D1[:,2]/D1[0,2],'-',lw=3,label='D1',color='sandybrown',alpha=1.0)

              
ax6.plot(time/1000.0,D2_lb[:,2]/D2_lb[0,2],'--',lw=3,label='D2',color='C0',alpha=1.0)
ax6.plot(time/1000.0,D2[:,2]/D2[0,2],'--',lw=3,label='D2',color='cornflowerblue',alpha=1.0)

ax6.plot(time[D1_lb[:,2].argmax()]/1000.0,D1_lb[:,2].max()/D1_lb[0,2],'x',ms=12,color='#000000',mew=2)
ax6.plot(time[D1_lb[:,2].argmax()]/1000.0,D2_lb[:,2].max()/D2_lb[0,2],'o',ms=12,mfc='none',color='#000000',mew=2)

ax6.text(0.58, 0.74, r'Long burst D1',color='C1',transform=ax6.transAxes, fontsize=18)
ax6.text(0.58, 0.64, r'Long burst D2',color='C0',transform=ax6.transAxes, fontsize=18)
ax6.text(0.58, 0.29, r'Burst and pause D1',color='sandybrown',transform=ax6.transAxes, fontsize=18)
ax6.text(0.58, 0.19, r'Burst and pause D2',color='cornflowerblue',transform=ax6.transAxes, fontsize=18)


#ax6.plot(time/1000.0,D1_Kd_lb[:,2],"--",lw=3,label='D1',color='#C0C0C0')
#ax6.plot(time/1000.0,D1_Kd[:,2],"--",lw=3,label='D1',color='#A9A9A9')
#ax2.plot(time[D1_Kd_lb[:,2].argmax()]/1000.0,D1_Kd_lb[:,2].max(),'x',ms=12,color='#ff2ace',mew=2)





filename="Fig_2_a.pdf"

fig.savefig(filename,transparent = True,bbox_inches='tight',)

fig.show()
fig.clf()





#two receptor state mode
plt.close('all')

#Plot receptor activation for D1  receptors for the timecourse of NAcc
fig=plt.figure(figsize=(6.2,10.76))
fig.clf()
#fig.suptitle('Concentration of active receptor')
plt.rc('text', usetex=True)	#use latex for all tex, that makes the font the same since some figures require us to use Latex
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']

matplotlib.rcParams.update({'font.size': 18})

sum_D1=np.add(D1_s2[:,1],D1_s2[:,2])
sum_D1_lb=np.add(D1_lb_s2[:,1],D1_lb_s2[:,2])
sum_D1_Kd=np.add(D1_Kd_s2[:,1],D1_Kd_s2[:,2])
sum_D1_lb_Kd=np.add(D1_Kd_lb_s2[:,1],D1_Kd_lb_s2[:,2])


time=np.arange(-750,len(D1[:,0])-750)
ax1=fig.add_subplot(6,1,(3,4))
ax2=ax1.twinx()
ax1.set_xlim(0.0,2.0)
ax2_min=0.0		#minimum of axis 2 with the K_D kinetics
ax2_max=350.0	#maximum of axis 2 with the K_D kinetics
ax1_max=92.0	# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax1_min=ax1_max-((ax1_max-sum_D1[0])/(ax2_max-sum_D1[0]))*(ax2_max-ax2_min)


ax1.set_ylim(ax1_min,ax1_max)
ax2.set_ylim(ax2_min,ax2_max)


ax1.plot(time/1000.0,sum_D1_lb[:],'-',lw=3,label='D1',color='C2')
ax1.plot(time/1000.0,sum_D1[:],'-',lw=3,label='D1',color='C4')
ax1.plot(time[sum_D1_lb[:].argmax()]/1000.0,sum_D1_lb[:].max(),'x',ms=12,color='#000000',mew=2)

ax2.spines['left'].set_color('k')
ax1.yaxis.label.set_color('k')
ax1.tick_params(axis='y',colors='k')

ax2.plot(time/1000.0,sum_D1_lb_Kd[:],"--",lw=3,label='D1',color='#C0C0C0')
ax2.plot(time/1000.0,sum_D1_Kd[:],"--",lw=3,label='D1',color='#A9A9A9')
ax2.plot(time[sum_D1_lb_Kd[:].argmax()]/1000.0,sum_D1_lb_Kd[:].max(),'x',ms=12,color='#ff2ace',mew=2)

ax2.spines['right'].set_color('#A9A9A9')
ax2.yaxis.label.set_color('#A9A9A9')
ax2.tick_params(axis='y',colors='#A9A9A9')
#ax1.text(0.75, 0.75, r'D1',transform=ax1.transAxes, fontsize=15)

#ax1.set_xlabel('Time in s',fontsize=18)
ax1.set_xticklabels([])
ax2.set_xticklabels([])

h1, l1=ax1.get_legend_handles_labels()
h2, l2=ax2.get_legend_handles_labels()

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas'

#ax1.legend(h1+h2,l1+l2)


sum_D2=np.add(D2_s2[:,1],D2_s2[:,2])
sum_D2_lb=np.add(D2_lb_s2[:,1],D2_lb_s2[:,2])
sum_D2_Kd=np.add(D2_Kd_s2[:,1],D2_Kd_s2[:,2])
sum_D2_lb_Kd=np.add(D2_Kd_lb_s2[:,1],D2_Kd_lb_s2[:,2])


ax3=fig.add_subplot(6,1,(5,6))
ax4=ax3.twinx()
ax3.set_xlim(0.0,2.0)
ax4_min=0.0		#minimum of axis 2 with the K_D kinetics
ax4_max=100.0	#maximum of axis 2 with the K_D kinetics
ax3_max=33.5		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax3_min=ax3_max-((ax3_max-sum_D2[0])/(ax4_max-sum_D2[0]))*(ax4_max-ax4_min)


ax3.set_ylim(ax3_min,ax3_max)
ax4.set_ylim(ax4_min,ax4_max)

ax3.plot(time/1000.0,sum_D2_lb[:],'-',lw=3,label='D2',color='C2')
ax3.plot(time/1000.0,sum_D2[:],'-',lw=3,label='D2',color='C4')

ax4.plot(time/1000.0,sum_D2_lb_Kd[:],'--',lw=3,label='D2',color='#C0C0C0')
ax4.plot(time/1000.0,sum_D2_Kd[:],'--',lw=3,label='D2',color='#A9A9A9')

ax3.plot(time[sum_D2_lb[:].argmax()]/1000.0,sum_D2_lb[:].max(),'o',mfc='none',ms=12,color='#000000',mew=2)
ax4.plot(time[sum_D2_lb_Kd[:].argmax()]/1000.0,sum_D2_lb_Kd[:].max(),'o',mfc='none',ms=12,color='#ff2ace',mew=2)

#ax3.set_ylabel('Real kinetics - Concentration in nM',fontsize=15)
ax3.spines['left'].set_color('k')
ax3.yaxis.label.set_color('k')
ax3.tick_params(axis='y',colors='k')
#ax3.text(0.75, 0.75, r'D2',transform=ax3.transAxes, fontsize=15)

#ax4.set_ylabel('Instant kinetics - Concentration in nM',fontsize=15)
ax4.spines['right'].set_color('#A9A9A9')
ax4.yaxis.label.set_color('#A9A9A9')
ax4.tick_params(axis='y',colors='#A9A9A9')

#ax3.set_xticklabels([])
#ax4.set_xticklabels([])

#ax3.set_xlabel('Time in s',fontsize=18)

ax3.set_zorder(ax4.get_zorder()+1) # put ax in front of ax2
ax3.patch.set_visible(False) # hide the 'canvas'


#DA_conc
ax5=fig.add_subplot(6,1,(1,2))
ax5.set_xlim(0.0,2.0)
ax5.plot(time/1000.0,D1_lb[:,0],lw=3,color='C2')
ax5.plot(time/1000.0,D1[:,0],lw=3,color='C4')


#Plot the maximum dots into the Dopamine curve
ax5.plot(time[sum_D2_lb[:].argmax()]/1000.0,D2_lb[sum_D2_lb[:].argmax(),0],'o',ms=12,mfc='none',color='#000000',mew=2)
ax5.plot(time[sum_D1_lb[:].argmax()]/1000.0,D2_lb[sum_D1_lb[:].argmax(),0],'x',ms=12,color='#000000',mew=2)

ax5.plot(time[sum_D2_lb_Kd[:].argmax()]/1000.0,D2_lb[sum_D2_lb_Kd[:].argmax(),0],'o',ms=12,mfc='none',color='#ff2ace',mew=2)
ax5.plot(time[sum_D1_lb_Kd[:].argmax()]/1000.0,D2_lb[sum_D1_lb_Kd[:].argmax(),0],'x',ms=12,color='#ff2ace',mew=2)


#ax5.plot(time[D1[:,2].argmax()]/1000.0,D1[D1[:,2].argmax(),0],'+',ms=10,color='#ff2ace',lw=3)



#ax5.set_ylabel('[DA] in nM',fontsize=13)
ax5.set_xticklabels([])
#ax5.text(0.75, 0.75, r'DA',transform=ax5.transAxes, fontsize=15)
#ax5.legend()


h1, l1=ax3.get_legend_handles_labels()
h2, l2=ax4.get_legend_handles_labels()
#ax3.legend(h1+h2,l1+l2)

ax5.text(0.479, 0.55, r'Long burst',color='C2',transform=ax5.transAxes, fontsize=18)
ax5.text(0.479, 0.35, r'Burst and pause',color='C4',transform=ax5.transAxes, fontsize=18)


filename="Fig_11_a.pdf"

fig.savefig(filename,transparent = True,bbox_inches='tight',)

fig.show()
fig.clf()




