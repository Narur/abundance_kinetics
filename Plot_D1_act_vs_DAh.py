# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 12:03:01 2019

@author: Narur
"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

a=np.asarray([50.0, 150.0, 250.0, 350.0, 500.0])
a_full=np.asarray([50.0,100.0,150.0,200.0,250.0,300.0,350.0,400.0,500.0,600.0,700.0,800.0,900.0,1000.0])
#b=[19.9, 20.1, 20.4, 20.7, 21.3]

b=np.asarray([19.82941182, 20.05992199, 20.35446621, 20.70659729, 21.33922831]) #maximum D1 activation for the shown curves in the parameter study of Fig. 6 
b_full=np.asarray([19.82941182, 19.93568894, 20.05992199, 20.19980446, 20.35446621,20.52347944, 20.70659729, 20.90366309, 21.33922831, 21.8295923 ,22.37437771, 22.9733066 , 23.62615248, 24.33271503])


AUC_DAh=np.asarray([  9290.54855055,  37396.54962498,  73362.49583298, 116411.57638743,193860.72998165]) #actual AUC for the curves numerically calculated
AUC_full=np.asarray([  9290.54855055,  22242.84854163,  37396.54962498,  54471.45783209,73362.49583298,  94018.8827771 , 116411.57638743, 140523.70188517,193860.72998165, 253972.27440023, 320825.45260001, 394400.87382388,474685.48511974, 561671.73892859])

#m=(b[1]-b[0])/(AUC_DAh[1]-AUC_DAh[0])
#c=b[0]-m*AUC_DAh[0]

m=(b_full[1]-b_full[0])/(AUC_full[1]-AUC_full[0])
c=b_full[0]-m*AUC_full[0]

coeff_p1=np.polynomial.polynomial.polyfit(a_full,b_full,1,full=True)
coeff_p2=np.polynomial.polynomial.polyfit(a_full,b_full,2,full=True)
r_sq_1p=1-coeff_p1[1][0]/sum((b_full-b_full.mean())**2)
r_sq_2p=1-coeff_p2[1][0]/sum((b_full-b_full.mean())**2)
coeff_AUC=np.polynomial.polynomial.polyfit(AUC_full,b_full,1,full=True)


def f_1p(x):
    return coeff_p1[0][1]*x+coeff_p1[0][0]

def f_2p(x):
    return coeff_p2[0][2]*x*x+coeff_p2[0][1]*x+coeff_p2[0][0]


def f_AUC_DA(x):
    return coeff_AUC[0][1]*x+coeff_AUC[0][0]




#plt.plot(a_full,f(a_full))
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})
plt.figure(figsize=(6.4,4.8))
plt.plot(a_full,f_1p(a_full),lw=4,color='C0',label='linear fit')
plt.plot(a_full,f_2p(a_full),lw=4,color='C1',label='quadratic fit')
plt.plot(a_full,f_AUC_DA(AUC_full),'--',lw=2,color='C2',label='AUC')
plt.plot(a_full,b_full,"x",ms=10,color='k')
#plt.xlabel('$\Delta [DA]^{max} (nM)$')
#plt.ylabel('[D1-DA] (nM)')
plt.tight_layout()
plt.legend()

plt.savefig("D1_act_vs_DAh.pdf",dpi=400)

