#!/usr/bin/env python
''' This is a script that runs all the subscripts to make the data for the ROC figures (F2 and S6) in the Hunger, Kumar, Schmidt 2018 Publication 
    titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations"
    The figures themselves will be made from this data in another script. 
    RUNNING THIS SCRIPT WILL TAKE A LONG TIME AND REQUIRE A LOT OF HARDDRIVE SPACE.
       
    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''




import os
import numpy as np
import argparse
from R_occup_DA_free_pattern_reward_states_seed import R_occup_sequences



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n','--nr_samples', nargs='?',type=int,default=10, const=10, help='How many realizations are to be calculated. The paper uses 500, default is 10.')
    args = parser.parse_args()
    

    nr_samples=args.nr_samples  #how many realizations are going to be created
    print "Starting... nr of realizations to be calculated is:",nr_samples
    
    #Here we create a Data folder (if it doesn't exist)
    my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
    
    res_path=os.path.join(my_path, "Data/Diff_Fig_2_ROC") #make a Data folder with a subfolder for the kinetic data
    if not os.path.exists(res_path):    #if the path doesn't exist, make it
        os.makedirs(res_path)
    
    os.chdir(res_path)  #change the directory to the data path and write the data in there

    #Reward Prob 0.0 to 1.0
    for p_rew in np.arange(0.0,1.01,0.1): #reward probability from 0.0 to 1.0 in 10% steps
        p_no=1.0-p_rew

        for i in range(0,nr_samples,1):
            R_occup_sequences(switch="D1",p_burst=p_rew,p_buwp=p_no,running_nr=i)    #D1
            R_occup_sequences(switch="D2",p_burst=p_rew,p_buwp=p_no,running_nr=i)    #D2
      
        
      
      
        

