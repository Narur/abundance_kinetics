''' Generate_DA_timeline is a helper class that generates Dopamine timelines and is part of the routines 
    accompanying the 2018 Hunger, Kumar, Schmidt Paper titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations"   
    
    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import pickle
import numpy as np
class DA_timelines():	
	def __init__(self):
		self.DA_avg_conc=20	#avg DA concentration the trace has before and after the pulse in nM
		self.nr_steps=5000
		self.V_max=1.5	#V_max for the NAcc in micromole/s
		self.K_m=0.21	#K_m for DAT in micromole
		


	def rise_linear(self,ramp_length,max_ramp):
		ramp_length_nr_steps=int(ramp_length*1000.0)	#ramp length in s 
		a=np.zeros(ramp_length_nr_steps)
		
		a[0:ramp_length_nr_steps]=np.linspace(self.DA_avg_conc,max_ramp,ramp_length_nr_steps,endpoint=True)	#make a ramp in ramp_length s to the maximum 
		t_interp=np.linspace(0.0, ramp_length_nr_steps, ramp_length_nr_steps, endpoint=False)	#time in 1 ms steps according to the rising ramp
		return t_interp, a

	def rtb(self,time,trace):	#takes the last value of the passed trace and let's it decay down to baseline extending the passed time array by the appropriate time
		V_max=1000.0*self.V_max
		K_m=1000.0*self.K_m
		max_length=10000
		D=trace[-1]
		b=np.zeros(max_length)	#Assumption is that the train decays in a maximum of 10s 
		
		
		for i in range(0,max_length):
				D-=(V_max*D/(K_m+D))*0.001	#stepsize 1 ms
				if (D<self.DA_avg_conc):
					break
				b[i]=D
				
		b[i]=self.DA_avg_conc

		return np.concatenate((time,np.linspace(time[-1]+1.0,time[-1]+1.0+i,i+1,endpoint=True))),np.concatenate((trace,b[0:i+1]))
		
		
	def pause(self,time,trace,length_of_pause):	#adds a DA neuron firing pause after the linear rise
		V_max=1000.0*self.V_max
		K_m=1000.0*self.K_m
		
		nr_length_of_pause=int(length_of_pause*1000.0) #turns the pause in s into a pause in ms ... also the amount of ms steps to look at 
		nr_rise_length=20	#time in ms it takes for the DA concentration to rise back to baseline
		
		D=trace[-1]
		b=np.zeros(nr_length_of_pause)
		
		for i in range(0,nr_length_of_pause):
			D-=(V_max*D/(K_m+D))*0.001	#stepsize 1 ms
			b[i]=D
		
		b=np.concatenate((b,np.linspace(b[i],self.DA_avg_conc,nr_rise_length,endpoint=True)))
		
		return 	np.concatenate((time,np.linspace(time[-1]+1.0,time[-1]+1.0+nr_length_of_pause+nr_rise_length,nr_length_of_pause+nr_rise_length,endpoint=False))),np.concatenate((trace,b)) 
		
		

	def combined_event(self,length_of_train,pattern):
		curr_idx=0
		t_fin=np.linspace(0.0, length_of_train, int(length_of_train), endpoint=False)	# length of the train in 1 ms segments
		b_fin=np.zeros((int(length_of_train)))	# length of the train in 1 ms segments
		
		for signal in pattern:	#go through the list of touples that make up the pattern, there are options for baseline (b), pause (p), reward pulse (r),reward pulse with pause (rp)
			if signal[0]=="b":		#adds a section where the DA concentration is at baseline
				b_fin[curr_idx:(curr_idx+int(signal[1]*1000.0))]=(self.DA_avg_conc)
				curr_idx+=int(signal[1]*1000.0)
			elif ((signal[0]=="ra") or (signal[0]=="bu")):	#adds a ramp or burst with rtb (usually burst and ramp are distinguished by different parameters)
				t,b=self.rise_linear(signal[1],signal[2])	#make a rice with signal[1] ramp length and signal[2] max DA concentration
				t,b=self.rtb(t,b)
				b_fin[curr_idx:(curr_idx+len(b))]=b
				curr_idx+=len(b)
			elif signal[0]=="buwp":	#adds burst with a pause with parameters ramp_length,maxDA,pause length all times in s				
				t,b=self.rise_linear(signal[1],signal[2])	#make a rice with signal[1] ramp length and signal[2] max DA concentration
				t,b=self.pause(t,b,signal[3])
				b_fin[curr_idx:(curr_idx+len(b))]=b
				curr_idx+=len(b)
			elif signal[0]=="pa":	#just adds a pause of signal[1] seconds
				#print signal[1]
				t,b=self.rise_linear(1,0.0)
				t,b=self.pause(t,b,signal[1])	#just put a pause
				b_fin[curr_idx:(curr_idx+len(b))]=b
				curr_idx+=len(b)

				
				
				
		b_fin[curr_idx:int(length_of_train)]=self.DA_avg_conc
		return t_fin,b_fin
		

