#!/usr/bin/env python
''' This script generates Panel b for Figure 2 and Supplemental Figure 11 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_data script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.ticker as mtick
import random
import math as m
import pickle
from scipy.integrate import simps
import os
nr_steps=5000

#Where am I and where are the data files ? (assuming the data files have been made with the Make_the Data script)
my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Fig_1_h_ramps") #make a Data folder with a subfolder for the kinetic data

D1_Nacc_ramp_2=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_2.0_50.0.dat"),"rb"))
D1_Nacc_ramp_3=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_3.0_50.0.dat"),"rb"))
D1_Nacc_ramp_4=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_4.0_50.0.dat"),"rb"))
D1_Nacc_ramp_5=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_5.0_50.0.dat"),"rb"))
D1_Nacc_ramp_6=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_6.0_50.0.dat"),"rb"))

D2_Nacc_ramp_2=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_2.0_50.0.dat"),"rb"))
D2_Nacc_ramp_3=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_3.0_50.0.dat"),"rb"))
D2_Nacc_ramp_4=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_4.0_50.0.dat"),"rb"))
D2_Nacc_ramp_5=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_5.0_50.0.dat"),"rb"))
D2_Nacc_ramp_6=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_6.0_50.0.dat"),"rb"))

D1_Nacc_ramp_2_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_2.0_50.0_Kd.dat"),"rb"))
D1_Nacc_ramp_3_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_3.0_50.0_Kd.dat"),"rb"))
D1_Nacc_ramp_4_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_4.0_50.0_Kd.dat"),"rb"))
D1_Nacc_ramp_5_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_5.0_50.0_Kd.dat"),"rb"))
D1_Nacc_ramp_6_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_6.0_50.0_Kd.dat"),"rb"))

D2_Nacc_ramp_2_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_2.0_50.0_Kd.dat"),"rb"))
D2_Nacc_ramp_3_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_3.0_50.0_Kd.dat"),"rb"))
D2_Nacc_ramp_4_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_4.0_50.0_Kd.dat"),"rb"))
D2_Nacc_ramp_5_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_5.0_50.0_Kd.dat"),"rb"))
D2_Nacc_ramp_6_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_6.0_50.0_Kd.dat"),"rb"))




D1_Nacc_ramp_s2_2=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_2.0_50.0_frac_s2_0.1.dat"),"rb"))
D1_Nacc_ramp_s2_3=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_3.0_50.0_frac_s2_0.1.dat"),"rb"))
D1_Nacc_ramp_s2_4=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_4.0_50.0_frac_s2_0.1.dat"),"rb"))
D1_Nacc_ramp_s2_5=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_5.0_50.0_frac_s2_0.1.dat"),"rb"))
D1_Nacc_ramp_s2_6=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_6.0_50.0_frac_s2_0.1.dat"),"rb"))

D2_Nacc_ramp_s2_2=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_2.0_50.0_frac_s2_0.1.dat"),"rb"))
D2_Nacc_ramp_s2_3=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_3.0_50.0_frac_s2_0.1.dat"),"rb"))
D2_Nacc_ramp_s2_4=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_4.0_50.0_frac_s2_0.1.dat"),"rb"))
D2_Nacc_ramp_s2_5=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_5.0_50.0_frac_s2_0.1.dat"),"rb"))
D2_Nacc_ramp_s2_6=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_6.0_50.0_frac_s2_0.1.dat"),"rb"))

D1_Nacc_ramp_s2_2_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_2.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D1_Nacc_ramp_s2_3_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_3.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D1_Nacc_ramp_s2_4_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_4.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D1_Nacc_ramp_s2_5_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_5.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D1_Nacc_ramp_s2_6_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_6.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))

D2_Nacc_ramp_s2_2_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_2.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D2_Nacc_ramp_s2_3_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_3.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D2_Nacc_ramp_s2_4_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_4.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D2_Nacc_ramp_s2_5_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_5.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))
D2_Nacc_ramp_s2_6_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_6.0_50.0_frac_s2_0.1_Kd.dat"),"rb"))








plt.close('all')
#Plot receptor activation for D1  receptors for the timecourse of NAcc
fig=plt.figure(figsize=(6.2,14.35))
fig.clf()
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})


#fig.suptitle('Physiological signals - Nacc')
time=np.arange(len(D1_Nacc_ramp_2[:,0]))
#ax1=fig.add_subplot(312)
ax1=fig.add_subplot(8,1,(3,4))
ax2=ax1.twinx()
ax1.set_xlim(0.0,8.0)

ax2_min=0.0		#minimum of axis 2 with the K_D kinetics
ax2_max=80.0	#maximum of axis 2 with the K_D kinetics
ax1_max=21.2		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax1_min=ax1_max-((ax1_max-D1_Nacc_ramp_2[0,2])/(ax2_max-D1_Nacc_ramp_2[0,2]))*(ax2_max-ax2_min)


ax1.set_ylim(ax1_min,ax1_max)
ax2.set_ylim(ax2_min,ax2_max)


#ax2.plot(time/1000.0,D1_Nacc_ramp_6_Kd[:,2],'--',lw=3,color="#C0C0C0",label='Ramp Kd 6s')
ax2.plot(time/1000.0,D1_Nacc_ramp_6_Kd[:,2],'--',lw=3,color="#A9A9A9",label='Ramp Kd 6s')
ax2.plot(time/1000.0,D1_Nacc_ramp_4_Kd[:,2],'--',lw=3,color="#C0C0C0",label='Ramp Kd 4s')
ax2.plot(time/1000.0,D1_Nacc_ramp_2_Kd[:,2],'--',lw=3,color="#CBCBCB",label='Ramp Kd 2s')
ax1.plot(time/1000.0,D1_Nacc_ramp_2[:,2],lw=3,label='Ramp 2s',color='C2')
#ax1.plot(time/1000.0,D1_Nacc_ramp_3[:,2],lw=3,label='Ramp 3s')
ax1.plot(time/1000.0,D1_Nacc_ramp_4[:,2],lw=3,label='Ramp 4s',color='C4')
#ax1.plot(time/1000.0,D1_Nacc_ramp_5[:,2],lw=3,label='Ramp 5s')
ax1.plot(time/1000.0,D1_Nacc_ramp_6[:,2],lw=3,label='Ramp 6s',color='C6')


#ax1.set_ylabel('[D1-DA] nM',fontsize=14)
ax1.set_xticklabels([])

ax1.spines['left'].set_color('k')
ax1.yaxis.label.set_color('k')
ax1.tick_params(axis='y',colors='k')
#ax1.text(0.7, 0.8, r'D1',transform=ax1.transAxes, fontsize=15)



#ax4.set_ylabel('Instant kinetics - Concentration in nM',fontsize=15)
ax2.spines['right'].set_color('#A9A9A9')
ax2.yaxis.label.set_color('#A9A9A9')
ax2.tick_params(axis='y',colors='#A9A9A9')

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas'


ax3=fig.add_subplot(8,1,(5,6))
ax4=ax3.twinx()
ax3.set_xlim(0.0,8.0)

ax4_min=20.0		#minimum of axis 2 with the K_D kinetics
ax4_max=80.0	#maximum of axis 2 with the K_D kinetics
ax3_max=38.0		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax3_min=ax3_max-((ax3_max-D2_Nacc_ramp_2[0,2])/(ax4_max-D2_Nacc_ramp_2[0,2]))*(ax4_max-ax4_min)


ax3.set_ylim(ax3_min,ax3_max)
ax4.set_ylim(ax4_min,ax4_max)
#ax4.set_yticks([10.0,25.0,40.0])
ax3.set_yticks([35.0,36.0,37.0,38.0])

#ax2.set_ylim(ax2_min,ax2_max)
#ax2.set_ylim(ax2_min,ax2_max)

ax4.plot(time/1000.0,D2_Nacc_ramp_6_Kd[:,2],'--',lw=3,color="#A9A9A9",label='Ramp Kd 6s')
ax4.plot(time/1000.0,D2_Nacc_ramp_4_Kd[:,2],'--',lw=3,color="#C0C0C0",label='Ramp Kd 4s')
ax4.plot(time/1000.0,D2_Nacc_ramp_2_Kd[:,2],'--',lw=3,color="#CBCBCB",label='Ramp Kd 2s')
ax3.plot(time/1000.0,D2_Nacc_ramp_2[:,2],lw=3,label='Ramp 2s',color='C2')
#ax2.plot(time/1000.0,D2_Nacc_ramp_3[:,2],lw=3,label='Ramp 3s')
ax3.plot(time/1000.0,D2_Nacc_ramp_4[:,2],lw=3,label='Ramp 4s',color='C4')
#ax2.plot(time/1000.0,D2_Nacc_ramp_5[:,2],lw=3,label='Ramp 5s')
ax3.plot(time/1000.0,D2_Nacc_ramp_6[:,2],lw=3,label='Ramp 6s',color='C6')

#ax3.set_ylabel('[D2-DA] nM',fontsize=14)
#ax3.set_xlabel('Time in s',fontsize=15)

#ax3.get_yaxis().set_major_formatter(mtick.FormatStrFormatter('%.1f'))
#ax2.legend(loc=7)
ax3.spines['left'].set_color('k')
ax3.yaxis.label.set_color('k')
ax3.tick_params(axis='y',colors='k')
#ax3.text(0.7, 0.8, r'D2',transform=ax3.transAxes, fontsize=15)



#ax4.set_ylabel('Instant kinetics - Concentration in nM',fontsize=15)
ax4.spines['right'].set_color('#A9A9A9')
ax4.yaxis.label.set_color('#A9A9A9')
ax4.tick_params(axis='y',colors='#A9A9A9')





#ax2.set_xticklabels([])
ax3.set_zorder(ax4.get_zorder()+1) # put ax in front of ax2
ax3.patch.set_visible(False) # hide the 'canvas'

ax3.set_xticklabels([])
ax4.set_xticklabels([])


#ax5=fig.add_subplot(311)
ax5=fig.add_subplot(8,1,(1,2))
ax5.set_xlim(0.0,8.0)
ax5.set_ylim(0.0,100.0)

#ax3.set_ylim(ax3_min,ax3_max)
#ax2.set_ylim(ax2_min,ax2_max)

ax5.plot(time/1000.0,D2_Nacc_ramp_2[:,0],lw=3,label='Ramp 2s',color='C2')
#ax3.plot(time/1000.0,D2_Nacc_ramp_3[:,0],lw=3,label='Ramp 3s')
ax5.plot(time/1000.0,D2_Nacc_ramp_4[:,0],lw=3,label='Ramp 4s', color='C4')
#ax3.plot(time/1000.0,D2_Nacc_ramp_5[:,0],lw=3,label='Ramp 5s')
ax5.plot(time/1000.0,D2_Nacc_ramp_6[:,0],lw=3,label='Ramp 6s',color='C6')

#ax5.set_ylabel('[DA] nM',fontsize=14)
#ax3.set_xlabel('Time in s',fontsize=15)
ax5.set_xticklabels([])

#ax3.legend()

#ax5.text(0.7, 0.8, r'DA',color='k',transform=ax5.transAxes, fontsize=15)
ax5.text(0.25, 0.6, r'2s',color='C2',transform=ax5.transAxes, fontsize=18)
ax5.text(0.45, 0.6, r'4s',color='C4',transform=ax5.transAxes, fontsize=18)
ax5.text(0.65, 0.6, r'6s',color='C6',transform=ax5.transAxes, fontsize=18)

#fig.text(0.018, 0.78, '[DA] (nM)',fontsize=18, color='k', va='center', rotation='vertical')
#fig.text(0.015, 0.22, '[D2-DA] (nM)',fontsize=18, color='k', va='center', rotation='vertical')
#fig.text(0.015, 0.5, '[D1-DA] (nM)',fontsize=18, color='k', va='center', rotation='vertical')
#fig.text(0.96, 0.5, '[D1-DA] (nM)',fontsize=18, color='#A9A9A9', va='center', rotation='vertical')
#fig.text(0.96, 0.22, '[D2-DA] (nM)',fontsize=18, color='#A9A9A9', va='center', rotation='vertical')

#fig.text(0.03, 0.5, 'Concentration in nM',fontsize=15, va='center', rotation='vertical')

#relative activation to baseline
ax6=fig.add_subplot(8,1,(7,8))
ax6.set_xlim(0.0,8.0)
#ax6.set_ylim(0.98,1.03)


#ax6.set_yticks([0.98,1.0,1.02])



ax6.plot(time/1000.0,D1_Nacc_ramp_2[:,2]/D1_Nacc_ramp_2[0,2],'-',lw=3,label='D1',color='C1',alpha=1.0)
ax6.plot(time/1000.0,D2_Nacc_ramp_2[:,2]/D2_Nacc_ramp_2[0,2],'--',lw=3,label='D2',color='C0',alpha=1.0)

ax6.plot(time/1000.0,D1_Nacc_ramp_4[:,2]/D1_Nacc_ramp_4[0,2],'-',lw=3,label='D1',color='sandybrown',alpha=1.0)
ax6.plot(time/1000.0,D2_Nacc_ramp_4[:,2]/D2_Nacc_ramp_4[0,2],'--',lw=3,label='D2',color='cornflowerblue',alpha=1.0)

ax6.plot(time/1000.0,D1_Nacc_ramp_6[:,2]/D1_Nacc_ramp_6[0,2],'-',lw=3,label='D1',color='xkcd:peach',alpha=1.0)
ax6.plot(time/1000.0,D2_Nacc_ramp_6[:,2]/D2_Nacc_ramp_6[0,2],'--',lw=3,label='D2',color='xkcd:sky blue',alpha=1.0)

ax6.text(0.17, 0.30, r'2s D1',color='C1',transform=ax6.transAxes, fontsize=18)
ax6.text(0.17, 0.2, r'2s D2',color='C0',transform=ax6.transAxes, fontsize=18)

ax6.text(0.43, 0.58, r'4s D1',color='sandybrown',transform=ax6.transAxes, fontsize=18)
ax6.text(0.43, 0.48, r'4s D2',color='cornflowerblue',transform=ax6.transAxes, fontsize=18)

ax6.text(0.67, 0.85, r'6s D1',color='xkcd:peach',transform=ax6.transAxes, fontsize=18)
ax6.text(0.67, 0.75, r'6s D2',color='xkcd:sky blue',transform=ax6.transAxes, fontsize=18)













filename="Fig_2_b_Ramps.pdf"

fig.savefig(filename,transparent = True,bbox_inches='tight')

fig.show()
fig.clf()















#Supp Fig with D1_high
plt.close('all')

#Plot receptor activation for D1  receptors for the timecourse of NAcc
fig=plt.figure(figsize=(6.2,10.76))
fig.clf()
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

#fig.suptitle('Physiological signals - Nacc')
time=np.arange(len(D1_Nacc_ramp_s2_2[:,0]))
#ax1=fig.add_subplot(312)
ax1=fig.add_subplot(6,1,(3,4))
ax2=ax1.twinx()
ax1.set_xlim(0.0,8.0)

ax2_min=50.0		#minimum of axis 2 with the K_D kinetics
ax2_max=200.0	#maximum of axis 2 with the K_D kinetics
ax1_max=95.0		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax1_min=ax1_max-((ax1_max-np.add(D1_Nacc_ramp_s2_2[0,2],D1_Nacc_ramp_s2_2[0,1]))/(ax2_max-np.add(D1_Nacc_ramp_s2_2[0,2],D1_Nacc_ramp_s2_2[0,1])))*(ax2_max-ax2_min)

ax1.set_ylim(ax1_min,ax1_max)
ax2.set_ylim(ax2_min,ax2_max)




#ax2.plot(time/1000.0,D1_Nacc_ramp_s2_6_Kd[:,2],'--',lw=3,color="#C0C0C0",label='Ramp Kd 6s')
ax2.plot(time/1000.0,np.add(D1_Nacc_ramp_s2_6_Kd[:,1],D1_Nacc_ramp_s2_6_Kd[:,2]),'--',lw=3,color="#A9A9A9",label='Ramp Kd 6s')
ax2.plot(time/1000.0,np.add(D1_Nacc_ramp_s2_4_Kd[:,1],D1_Nacc_ramp_s2_4_Kd[:,2]),'--',lw=3,color="#C0C0C0",label='Ramp Kd 4s')
ax2.plot(time/1000.0,np.add(D1_Nacc_ramp_s2_2_Kd[:,1],D1_Nacc_ramp_s2_2_Kd[:,2]),'--',lw=3,color="#CBCBCB",label='Ramp Kd 2s')
ax1.plot(time/1000.0,np.add(D1_Nacc_ramp_s2_2[:,1],D1_Nacc_ramp_s2_2[:,2]),lw=3,label='Ramp 2s',color='C2')
#ax1.plot(time/1000.0,D1_Nacc_ramp_s2_3[:,2],lw=3,label='Ramp 3s')
ax1.plot(time/1000.0,np.add(D1_Nacc_ramp_s2_4[:,1],D1_Nacc_ramp_s2_4[:,2]),lw=3,label='Ramp 4s',color='C4')
#ax1.plot(time/1000.0,D1_Nacc_ramp_s2_5[:,2],lw=3,label='Ramp 5s')
ax1.plot(time/1000.0,np.add(D1_Nacc_ramp_s2_6[:,1],D1_Nacc_ramp_s2_6[:,2]),lw=3,label='Ramp 6s',color='C6')


#ax1.set_ylabel('[D1-DA] nM',fontsize=14)
ax1.set_xticklabels([])

ax1.spines['left'].set_color('k')
ax1.yaxis.label.set_color('k')
ax1.tick_params(axis='y',colors='k')
#ax1.text(0.7, 0.8, r'D1',transform=ax1.transAxes, fontsize=15)



#ax4.set_ylabel('Instant kinetics - Concentration in nM',fontsize=15)
ax2.spines['right'].set_color('#A9A9A9')
ax2.yaxis.label.set_color('#A9A9A9')
ax2.tick_params(axis='y',colors='#A9A9A9')

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas'

ax3=fig.add_subplot(6,1,(5,6))
ax4=ax3.twinx()
ax3.set_xlim(0.0,8.0)

ax4_min=20.0		#minimum of axis 2 with the K_D kinetics
ax4_max=60.0	#maximum of axis 2 with the K_D kinetics
ax3_max=35.0		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax3_min=ax3_max-((ax3_max-np.add(D2_Nacc_ramp_s2_2[0,2],D2_Nacc_ramp_s2_2[0,1]))/(ax4_max-np.add(D2_Nacc_ramp_s2_2[0,2],D2_Nacc_ramp_s2_2[0,1])))*(ax4_max-ax4_min)

ax3.set_ylim(ax3_min,ax3_max)
ax4.set_ylim(ax4_min,ax4_max)




#ax2.plot(time/1000.0,D2_Nacc_ramp_s2_6_Kd[:,2],'--',lw=3,color="#C0C0C0",label='Ramp Kd 6s')
ax4.plot(time/1000.0,np.add(D2_Nacc_ramp_s2_6_Kd[:,1],D2_Nacc_ramp_s2_6_Kd[:,2]),'--',lw=3,color="#A9A9A9",label='Ramp Kd 6s')
ax4.plot(time/1000.0,np.add(D2_Nacc_ramp_s2_4_Kd[:,1],D2_Nacc_ramp_s2_4_Kd[:,2]),'--',lw=3,color="#C0C0C0",label='Ramp Kd 4s')
ax4.plot(time/1000.0,np.add(D2_Nacc_ramp_s2_2_Kd[:,1],D2_Nacc_ramp_s2_2_Kd[:,2]),'--',lw=3,color="#CBCBCB",label='Ramp Kd 2s')
ax3.plot(time/1000.0,np.add(D2_Nacc_ramp_s2_2[:,1],D2_Nacc_ramp_s2_2[:,2]),lw=3,label='Ramp 2s',color='C2')
#ax1.plot(time/1000.0,D2_Nacc_ramp_s2_3[:,2],lw=3,label='Ramp 3s')
ax3.plot(time/1000.0,np.add(D2_Nacc_ramp_s2_4[:,1],D2_Nacc_ramp_s2_4[:,2]),lw=3,label='Ramp 4s',color='C4')
#ax1.plot(time/1000.0,D2_Nacc_ramp_s2_5[:,2],lw=3,label='Ramp 5s')
ax3.plot(time/1000.0,np.add(D2_Nacc_ramp_s2_6[:,1],D2_Nacc_ramp_s2_6[:,2]),lw=3,label='Ramp 6s',color='C6')

#ax3.set_ylabel('[D2-DA] nM',fontsize=14)
#ax3.set_xlabel('Time in s',fontsize=15)

#ax3.get_yaxis().set_major_formatter(mtick.FormatStrFormatter('%.1f'))
#ax2.legend(loc=7)
ax3.spines['left'].set_color('k')
ax3.yaxis.label.set_color('k')
ax3.tick_params(axis='y',colors='k')
#ax3.text(0.7, 0.8, r'D2',transform=ax3.transAxes, fontsize=15)



#ax4.set_ylabel('Instant kinetics - Concentration in nM',fontsize=15)
ax4.spines['right'].set_color('#A9A9A9')
ax4.yaxis.label.set_color('#A9A9A9')
ax4.tick_params(axis='y',colors='#A9A9A9')

#ax2.set_xticklabels([])
ax3.set_zorder(ax4.get_zorder()+1) # put ax in front of ax2
ax3.patch.set_visible(False) # hide the 'canvas'



#ax5=fig.add_subplot(311)
ax5=fig.add_subplot(6,1,(1,2))
ax5.set_xlim(0.0,8.0)
ax5.set_ylim(0.0,100.0)

#ax3.set_ylim(ax3_min,ax3_max)
#ax2.set_ylim(ax2_min,ax2_max)

ax5.plot(time/1000.0,D2_Nacc_ramp_s2_2[:,0],lw=3,label='Ramp 2s',color='C2')
#ax3.plot(time/1000.0,D2_Nacc_ramp_s2_3[:,0],lw=3,label='Ramp 3s')
ax5.plot(time/1000.0,D2_Nacc_ramp_s2_4[:,0],lw=3,label='Ramp 4s', color='C4')
#ax3.plot(time/1000.0,D2_Nacc_ramp_s2_5[:,0],lw=3,label='Ramp 5s')
ax5.plot(time/1000.0,D2_Nacc_ramp_s2_6[:,0],lw=3,label='Ramp 6s',color='C6')

#ax5.set_ylabel('[DA] nM',fontsize=14)
#ax3.set_xlabel('Time in s',fontsize=15)
ax5.set_xticklabels([])

#ax3.legend()

#ax5.text(0.7, 0.8, r'DA',color='k',transform=ax5.transAxes, fontsize=15)
ax5.text(0.25, 0.6, r'2s',color='C2',transform=ax5.transAxes, fontsize=18)
ax5.text(0.45, 0.6, r'4s',color='C4',transform=ax5.transAxes, fontsize=18)
ax5.text(0.65, 0.6, r'6s',color='C6',transform=ax5.transAxes, fontsize=18)

#fig.text(0.018, 0.78, '[DA] (nM)',fontsize=18, color='k', va='center', rotation='vertical')
#fig.text(0.015, 0.22, '[D2-DA] (nM)',fontsize=18, color='k', va='center', rotation='vertical')
#fig.text(0.015, 0.5, '[D1-DA] (nM)',fontsize=18, color='k', va='center', rotation='vertical')
#fig.text(0.96, 0.5, '[D1-DA] (nM)',fontsize=18, color='#A9A9A9', va='center', rotation='vertical')
#fig.text(0.96, 0.22, '[D2-DA] (nM)',fontsize=18, color='#A9A9A9', va='center', rotation='vertical')

#fig.text(0.03, 0.5, 'Concentration in nM',fontsize=15, va='center', rotation='vertical')
filename="Fig_11_b_Ramps.pdf"

fig.savefig(filename,transparent = True,bbox_inches='tight')

fig.show()
fig.clf()
