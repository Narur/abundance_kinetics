#!/usr/bin/env python
''' This script generates Panel a and c for Figure 8 of
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_the_data_ROC script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
from collections import OrderedDict
import os
nr_samples=10

#Where am I and where are the data files ? (assuming the data files have been made with the Make_the Data script)
my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Fig_2_ROC") #make a Data folder with a subfolder for the kinetic data



p=[0.3,0.5,0.7]     #which reward probabilities should be plotted
realizations=[9]    #which realization should be plotted

D1={}
D2={}
print "DOOM"
for p1 in p:
    D1[p1]={}
    D2[p1]={}


for i in realizations:    
    for p1 in p:
        filename="free_D1_reward_states_prob_jitter_"+"{:.2f}".format(p1)+"_"+"{:.2f}".format(1.0-p1)+"_0.00_50_"+str(i)+".dat"
        D1[p1][i]=pickle.load(open(os.path.join(res_path,filename),"rb"))      
        filename="free_D2_reward_states_prob_jitter_"+"{:.2f}".format(p1)+"_"+"{:.2f}".format(1.0-p1)+"_0.00_50_"+str(i)+".dat"
        D2[p1][i]=pickle.load(open(os.path.join(res_path,filename),"rb"))  
        print i


    




plt.close('all')
for i in realizations:
#Plot a realization of 0.3 0.5 and 0.7
    #Plot receptor activation for D1  receptors for the timecourse of NAcc 
    fig=plt.figure()
    fig.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', weight='bold')
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
    matplotlib.rcParams.update({'font.size': 18})
    time=np.arange(len(D1[p[0]][i]))
    ax1=fig.add_subplot(111)



    ax1.plot(time/1000.0,D1[p[0]][i][:,2],color='C0',lw=3,label=r'\textbf{{{:>3.0f} \%}}'.format(p[0]*100.0))
    ax1.plot(time/1000.0,D1[p[1]][i][:,2],color='C1',lw=3,label=r'\textbf{{{:>3.0f} \%}}'.format(p[1]*100.0))
    ax1.plot(time/1000.0,D1[p[2]][i][:,2],color='C2',lw=3,label=r'\textbf{{{:>3.0f} \%}}'.format(p[2]*100.0))


    #ax1.set_ylabel('[D1-DA] in nM',fontsize=15)

    #ax1.set_xlabel('Time in s',fontsize=15)

    #fig.suptitle('D1 receptor activation')
    handles, labels = ax1.get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    ax1.legend(by_label.values(), by_label.keys(),fontsize=15,loc=2)

    filename="Fig_8_a_D1_"+str(i)+".pdf"

    fig.savefig(filename)

    fig.show()
    
    
    
plt.close('all')
for i in realizations:
#Plot a realization of 0.3 0.5 and 0.7
    fig=plt.figure()
    fig.clf()
    time=np.arange(len(D2[p[0]][i]))
    ax1=fig.add_subplot(111)


    ax1.plot(time/1000.0,D2[p[0]][i][:,2],color='C0',lw=3,label=r'\textbf{{{:>3.0f} \%}}'.format(p[0]*100.0))
    ax1.plot(time/1000.0,D2[p[1]][i][:,2],color='C1',lw=3,label=r'\textbf{{{:>3.0f} \%}}'.format(p[1]*100.0))
    ax1.plot(time/1000.0,D2[p[2]][i][:,2],color='C2',lw=3,label=r'\textbf{{{:>3.0f} \%}}'.format(p[2]*100.0))





    #ax1.set_ylabel('[D2-DA] in nM',fontsize=15)

    #ax1.set_xlabel('Time in s',fontsize=15)

    #fig.suptitle('D2 receptor activation')

    ax1.legend(fontsize=15,loc=2)

    filename="Fig_8_c_D2_"+str(i)+".pdf"

    fig.savefig(filename)

    fig.show()
    
    
    

    
    
    
    
    

