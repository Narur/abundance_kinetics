#!/usr/bin/env python
''' This script generates Panel a and b for Figure 1 and Figure 10 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''



import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
nr_steps=5000


plt.close('all')


D1_base_conc=1600.0
D2_base_conc=80.0
C_DA=20.0

def fractional_occupancy(K_D,C_DA,Rec_conc):
	return Rec_conc*(C_DA/(C_DA+K_D))


#Figure that shows resting state Receptor activation for different K_d but normal Nacc parameters
fig=plt.figure()
fig.clf()
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

#fig.suptitle('Receptor Activation at baseline DA concentration')

ax1=fig.add_subplot(111)
#ax1.set_ylabel('[DX-DA] in nM',fontsize=15)

x_D2 = np.arange(10.0,110.0,0.01)

D2_conc=D2_base_conc
y_D2=[fractional_occupancy(x1,C_DA,D2_conc) for x1 in x_D2]

D2_conc=0.2*D2_base_conc+D2_base_conc
y_D2_2=[fractional_occupancy(x1,C_DA,D2_conc) for x1 in x_D2]

D2_conc=-0.2*D2_base_conc+D2_base_conc
y_D2_3=[fractional_occupancy(x1,C_DA,D2_conc) for x1 in x_D2]

ax1.plot(x_D2,y_D2[:],'C0',lw=3,label='D2, [DA]=20 nM')
ax1.fill_between(x_D2, y_D2_2,y_D2_3,color='C0',alpha=.3)
ax1.plot(25.0,2.0*17.78,'xk',ms=12,mew=2) #,label='D2 this study'

ax2=ax1.twiny()
ax1.set_ylim([0,60.0])

D1_conc=D1_base_conc
x_D1 = np.arange(1000.0,3000.0,0.1)

y_D1=[fractional_occupancy(x1,C_DA,D1_conc) for x1 in x_D1]
D1_conc=0.2*D1_base_conc+D1_base_conc
y_D1_2=[fractional_occupancy(x1,C_DA,D1_conc) for x1 in x_D1]

D1_conc=-0.2*D1_base_conc+D1_base_conc
y_D1_3=[fractional_occupancy(x1,C_DA,D1_conc) for x1 in x_D1]

ax2.plot(x_D1/1000.0,y_D1[:],'C1',lw=3,label='D1, [DA]=20 nM')
ax2.fill_between(x_D1/1000.0, y_D1_2,y_D1_3,color='C1',alpha=.3)
ax2.plot(1.6,2.0*9.88,'+k',ms=12,mew=2) #,label='D1 this study')

ax1.grid(color='#D3D3D3', linestyle='--', linewidth=1)
ax1.xaxis.set_ticks(np.arange(10.0,111.0,20.0))
ax2.xaxis.set_ticks(np.arange(1.0,3.01,0.4))

ax2.set_xlabel('$K_D^{D1}$ ($\mu M$)',fontsize=18)
ax1.set_xlabel('$K_D^{D2}$ (nM)',fontsize=18)

ax2.spines['top'].set_color('C1')
ax2.xaxis.label.set_color('C1')
ax2.tick_params(axis='x',colors='C1')

ax1.spines['bottom'].set_color('C0')
ax1.xaxis.label.set_color('C0')
ax1.tick_params(axis='x',colors='C0')

h1, l1=ax1.get_legend_handles_labels()
h2, l2=ax2.get_legend_handles_labels()

#fig.show()

filename="Fig_1_a_D1_D2_baseline_occupation_Kd.pdf"
fig.savefig(filename,transparent = True,bbox_inches='tight')


#Figure that shows resting state Receptor activation for different C_DA baseline same K_D but normal Nacc parameters
fig=plt.figure()
fig.clf()
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

#fig.suptitle('Receptor Activation at baseline DA concentration')

ax1=fig.add_subplot(111)
#ax1.set_ylabel('[DX-DA] in nM',fontsize=15)

K_D2=25.0
D2_conc=D2_base_conc
x_D2 = np.arange(1.0,50.0,0.01) #baseline DA concentration

y_D2=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]

D2_conc=0.2*D2_base_conc+D2_base_conc
y_D2_2=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]

D2_conc=-0.2*D2_base_conc+D2_base_conc
y_D2_3=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]

ax1.plot(x_D2,y_D2[:],'C0',lw=3,label='D2, $K_D$=25 nM')
ax1.fill_between(x_D2, y_D2_2,y_D2_3,color='C0',alpha=.3)
ax1.plot(20.0,2.0*17.78,'xk',ms=12,mew=2) #,label='D2 this study'

ax1.set_ylim([0,60.0])
D1_conc=D1_base_conc

x_D1 = np.arange(1.0,50.0,0.01)
K_D1=1600.0
y_D1=[fractional_occupancy(K_D1,x1,D1_conc) for x1 in x_D1]

D1_conc=0.2*D1_base_conc+D1_base_conc
y_D1_2=[fractional_occupancy(K_D1,x1,D1_conc) for x1 in x_D1]

D1_conc=-0.2*D1_base_conc+D1_base_conc
y_D1_3=[fractional_occupancy(K_D1,x1,D1_conc) for x1 in x_D1]


ax1.plot(x_D1,y_D1[:],'C1',lw=3,label='D1, $K_D$=1600 nM')
#ax2.plot(x_D1/1000.0,y_D1_2[:],'--r',lw=3,label='D1 occupancy, $C_{D1}^{max}=1000.0 nM$')
ax1.fill_between(x_D1, y_D1_2,y_D1_3,color='C1',alpha=.3)
ax1.plot(20.0,2.0*9.88,'+k',ms=12,mew=2) #,label='D1 this study')


ax1.grid(color='#D3D3D3', linestyle='--', linewidth=1)
ax1.xaxis.set_ticks(np.arange(5.0,50.1,5.0))

ax1.set_ylim([0,60.0])
ax1.set_xlim([0,50.0])


ax1.spines['bottom'].set_color('k')
ax1.xaxis.label.set_color('k')
ax1.tick_params(axis='x',colors='k')

h1, l1=ax1.get_legend_handles_labels()

filename="Fig_1_b_D1_D2_baseline_occupation_DA.pdf"

fig.savefig(filename,transparent = True,bbox_inches='tight')



#Figure that shows resting state Receptor activation, for two state receptors,  for different C_DA baseline but same K_D but normal Nacc parameters
fig=plt.figure()
fig.clf()
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

#fig.suptitle('Receptor Activation at baseline DA concentration')
D1_base_conc=1600.0
D1_s2=0.1*D1_base_conc
D1_base_conc-=D1_s2

D2_base_conc=80.0
D2_s2=0.1*D2_base_conc
D2_base_conc-=D2_s2



ax1=fig.add_subplot(111)
#ax1.set_ylabel('[DX-DA] in nM',fontsize=15)

K_D2=25.0
D2_conc=D2_base_conc
D2_s2_conc=D2_s2
x_D2 = np.arange(1.0,50.0,0.01) #baseline DA concentration
y_D2=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]
y_D2_s2=[fractional_occupancy(K_D1,x1,D2_s2_conc) for x1 in x_D2]


#D2_conc=0.2*D2_base_conc+D2_base_conc
#y_D2_2=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]
#y_D2_s2_2=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]



#D2_conc=-0.2*D2_base_conc+D2_base_conc
#y_D2_3=[fractional_occupancy(K_D2,x1,D2_conc) for x1 in x_D2]

ax1.plot(x_D2,np.add(y_D2,y_D2_s2),'C0',lw=3,label='$D2^{high}+D2^{low}$')
ax1.plot(x_D1,y_D2,'C0',ls='--',lw=3,label='$D2^{high}$',alpha=.7)
ax1.plot(x_D1,y_D2_s2,'C0',ls='-.',lw=3,label='$D2^{low}$',alpha=.7)
idx=np.where(x_D1>20.00)[0][0]-1
ax1.plot(20.0,y_D2[idx]+y_D2_s2[idx],'xk',ms=12,mew=2) 

#ax1.set_ylim([0,60.0])
D1_conc=D1_base_conc

x_D1 = np.arange(1.0,50.0,0.01)
K_D1=1600.0
y_D1=[fractional_occupancy(K_D1,x1,D1_conc) for x1 in x_D1]
y_D1_s2=[fractional_occupancy(K_D2,x1,D1_s2) for x1 in x_D1]



ax1.plot(x_D1,np.add(y_D1,y_D1_s2),'C1',lw=3,label='$D1^{high}+D1^{low}$')
ax1.plot(x_D1,y_D1_s2,'C1',ls='--',lw=3,label='$D1^{high}$',alpha=.7)
ax1.plot(x_D1,y_D1,'C1',ls='-.',lw=3,label='$D1^{low}$',alpha=.7)
idx=np.where(x_D1>20.00)[0][0]-1
ax1.plot(20.0,y_D1[idx]+y_D1_s2[idx],'+k',ms=12,mew=2) #,label='D1 this study')


ax1.grid(color='#D3D3D3', linestyle='--', linewidth=1)
ax1.xaxis.set_ticks(np.arange(5.0,50.1,5.0))

ax1.set_ylim([0,200.0])
ax1.set_xlim([0.0,50.0])


ax1.spines['bottom'].set_color('k')
ax1.xaxis.label.set_color('k')
ax1.tick_params(axis='x',colors='k')

h1, l1=ax1.get_legend_handles_labels()
ax1.legend(fontsize=15)
filename="Fig_10_b_D1_D2_s2_occupation_DA.pdf"

fig.savefig(filename,transparent = True,bbox_inches='tight')






#Figure that shows resting state Receptor activation for different K_d (High and low) but normal Nacc parameters
fig=plt.figure()
fig.clf()
plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

#fig.suptitle('Receptor Activation at baseline DA concentration')

ax1=fig.add_subplot(111)
#ax1.set_ylabel('[DX-DA] in nM',fontsize=15)




D1_base_conc=1600.0
D1_s2=0.1*D1_base_conc
D1_base_conc-=D1_s2

D2_base_conc=80.0
D2_s2=0.1*D2_base_conc
D2_base_conc-=D2_s2









x_D2 = np.arange(10.0,110.0,0.005)
x_D1 = np.arange(1000.0,3000.0,0.1)

D2_conc=D2_base_conc
y_D2=[fractional_occupancy(x1,C_DA,D2_conc) for x1 in x_D2]
y_D2_s2=[fractional_occupancy(x1,C_DA,D2_s2) for x1 in x_D1]


ax2=ax1.twiny()
ax1.set_ylim([-5.0,100.0])

ax1.plot(x_D2,y_D2[:],'C5',ls='--',lw=3,label='$D2^{high}$')
ax2.plot(x_D1/1000.0,y_D2_s2[:],'C3',ls='--',lw=3,label='$D2^{low}$')





D1_conc=D1_base_conc


y_D1=[fractional_occupancy(x1,C_DA,D1_conc) for x1 in x_D1]
y_D1_s2=[fractional_occupancy(x1,C_DA,D1_s2) for x1 in x_D2]


ax2.plot(x_D1/1000.0,y_D1[:],'C3',ls='-.',lw=3,label='$D1^{low}$')
ax1.plot(x_D2,y_D1_s2[:],'C5',ls='-.',lw=3,label='$D1^{high}$')

ax1.grid(color='#D3D3D3', linestyle='--', linewidth=1)
ax1.xaxis.set_ticks(np.arange(10.0,111.0,20.0))
ax2.xaxis.set_ticks(np.arange(1.0,3.01,0.4))

ax2.set_xlabel('$K_D^{Low}$ ($\mu M$)',fontsize=18)
ax1.set_xlabel('$K_D^{High}$ (nM)',fontsize=18)

ax2.spines['top'].set_color('C5')
ax2.xaxis.label.set_color('C5')
ax2.tick_params(axis='x',colors='C5')

ax1.spines['bottom'].set_color('C3')
ax1.xaxis.label.set_color('C3')
ax1.tick_params(axis='x',colors='C3')

h1, l1=ax1.get_legend_handles_labels()
h2, l2=ax2.get_legend_handles_labels()
plt.legend(h1+h2,l1+l2,fontsize=15)
#fig.show()

filename="Fig_10_a_D1_D2_baseline_occupation_Kd.pdf"
fig.savefig(filename,transparent = True,bbox_inches='tight')




