#!/usr/bin/env python
''' This file contains the python script that uses the Generate_DA_timeline helper class to generate Dopamine timelines
    and subsequently uses Runge Kutta integration that calculates the resulting D1 and D2 receptor activations caused by these DA timecourses. 
    This file accompanies the Hunger, Kumar, Schmidt 2018 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    The function R_occup_DA is called by Make_the_data but can also be called directly from python after importing.
    
    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import numpy as np 
import pickle
from random import randint
import math
import os   #operating system
from shutil import copyfile #for copying files
from scipy import interpolate
from scipy.integrate import odeint
import sys
import Generate_DA_timeline as GTL

#function that calculates the derivative of [RL]
def deriv_RL(C_LR,t,C_DA,C_DA_times,C_R_max,k_on,k_off):
    Current_C_DA=np.interp(t, C_DA_times, C_DA)	#interpolates the dopamine concentration linearly from the supplied curve
    return k_on*Current_C_DA*C_R_max-k_on*Current_C_DA*C_LR-k_off*C_LR
        




def R_occup_DA(switch='D1', #which receptor to calculate occupation for 
                           pause_switch='burst',    #what pattern of DA timeline should be used
                           V_max=1.5,               #DA uptake V_max n micro-m/s for the DA timeline
                           rise_time=0.2,           #rise_time in s for the DA ramp/burst
                           DA_rise_conc=200.0,      #Change in DA conc from start to maximum of DA signal in nM
                           pause_length=1.0,        #length of a DA neuron firing pause, if its in the pattern
                           fast_r=1.0,              #multiplikative increase in speed of the receptors (multiplies k_on and k_off)
                           ode_mode='ode',          #integrate for realistic kinetic case ("ode"), or just use affinity to calculate equilibriumoccupation for DA conc. ("Kd") 
                           dt=1.0,                  #timestep of ODE integrates in ms
                           DA_avg_conc=20.0,        #Dopamine baseline conc. in nM
                           area_switch="Nacc",      #Nacc or Str 
                           t_repetition=15.0,       #In case of a pattern with multiple trials, what is the time interval in which the trial is repeated (in s)
                           frac_s2=0.0,             #fraction of receptors in high affinity state (for D1) or low affinity state (for D2), In case of the receptors being treated as two state receptors... as in Richfield 1989
                           BL_fraction_min_DA=0.0,  #Minimum fraction of the BL DA to which DA can drop during firing pauses, default is 0... so DA can drop to 0 nM (used for Burst_with_pause simulation with an offset)
                           **kwargs):               #optional extra parameters
    '''This function creates a timeline of receptor occupation for the receptor controlled by switch. It uses a Dopamine timeline of the type pause_switch with an uptake constant V_max
    and controls the DA timeline with the parameters rise_time, DA_rise_conc and pause_length. Values supplied in the definition are the defaults.'''

    
    #if R_base_conc in kwargs use the given value, else use the default values depending on the receptor type
    if 'R_base_conc' in kwargs:
        R_base_C_max=kwargs['R_base_conc']
    else:
        if switch=='D1':
            R_base_C_max=1600.0      #Concentration of D1 receptors in nM 
        elif switch=='D2':
            R_base_C_max=400.0       #Concentration of D2 receptors in nM, before internalization fraction

    #check if k_on is different then the default
    if 'k_on' in kwargs:
        k_on=kwargs['k_on']
    else:
        if switch=='D1':
            k_on=0.0003125/60.0 #D1 on rate in nM/second   
        elif switch=='D2':
            k_on=0.02/60.0 #D2 on rate in nM/second
            
    if 'k_on_s2' in kwargs: #s2 is the second affinity state of the given receptor, as default the receptors high/low states are modeled as the other receptor (D1_high=>D2_high), (D2_low=>D1_low) because there is not a lot of data about the kinetics
        k_on_s2=kwargs['k_on_s2']
    else:
        if switch=='D1':
            k_on_s2=0.02/60.0 #D1 on rate in nM/second   
        elif switch=='D2':
            k_on_s2=0.0003125/60.0 #D2 on rate in nM/second
            
    if 'k_off_s2' in kwargs: #s2 is the second affinity state of the given receptor, as default the receptors high/low states are modeled as the other receptor (D1_high=>D2_high), (D2_low=>D1_low) because there is not a lot of data about the kinetics
        k_off_s2=kwargs['k_off_s2']
    else:
        if switch=='D1':
            k_off_s2=0.5/60.0 #D1 on rate in nM/second   
        elif switch=='D2':
            k_off_s2=0.5/60.0 #D2 on rate in nM/second


    #check if k_off is different than the default
    if 'k_off' in kwargs:
        k_off=kwargs['k_off']
    else:
        if switch=='D1':
            k_off=0.5/60.0 #off rate in 1/second  
        elif switch=='D2':
            k_off=0.5/60.0 #off rate in 1/second   

    
    #check if R_f_ECS (fraction of receptors that protrudes into the extracellular space) is different than default
    if 'R_f_ECS' in kwargs:
        R_f_ECS=kwargs['R_f_ECS']
    else:
        if switch=='D1':
            R_f_ECS=1.0 #All the receptors are on the cell membrane and protrude into the ECS
        elif switch=='D2':
            R_f_ECS=0.2 #only 20% of the receptors protrude in the ECS, default for D2 since D2R seem to be retained in the Endoplasmatic reticulum.
            
    if ((area_switch=='Str') and (switch=='D2')):
        reg_mod=1.5 #There are more D2 receptors in the Striatum
    else:
        reg_mod=1.0 #Nacc has the default amount of receptors

    #Set general parameters
    #amount of receptors is the base value times the fraction of receptors sticking into the ECS (D1=1.0, D2=0.2) times the regional modifier for D2
    C_R_max=R_base_C_max*R_f_ECS*reg_mod   
    if (frac_s2>0.0): #in case there are two phase dopamine receptors (e.g. D1^high and D1^low) 
        C_R_max_s2=C_R_max*frac_s2  #two phases
        C_R_max-=C_R_max_s2 #two phases 
    else:
        C_R_max_s2=0.0
    
    #Set kinetic constants, if a fast_r parameters is passed, it is used here to modify the kinetics
    k_on=k_on*fast_r
    k_off=k_off*fast_r    
    k_on_s2*=fast_r
    k_off_s2*=fast_r


    K_D=k_off/k_on  #affinity 
    K_D_s2=k_off_s2/k_on_s2 #affinity of second state (if applicable)
    dt=1.0 #timestep in ms
    DA_avg_conc=20.0

    #calculate starting occupancies 
    AVG_frac_occupancy=DA_avg_conc/(DA_avg_conc+K_D)
    R=(1.0-AVG_frac_occupancy)*C_R_max
    RL=AVG_frac_occupancy*C_R_max
    
    AVG_frac_occupancy_s2=DA_avg_conc/(DA_avg_conc+K_D_s2)
    R_s2=(1.0-AVG_frac_occupancy_s2)*C_R_max_s2
    RL_s2=AVG_frac_occupancy_s2*C_R_max_s2

    #-----------------------------------------------------This part generates the DA timeline, depending on the supplied pause_switch ---------------------------------------# 
    a=GTL.DA_timelines()	#make a a DA timelines object... that will generate us our DA timeline and set the parameters for it
    a.V_max=V_max
    a.DA_avg_conc=DA_avg_conc-BL_fraction_min_DA*DA_avg_conc

    if pause_switch=="burst":	#a ramp to DA_rise_conc in rise_time beginning after 1 s of baseline, signal over after DA conc. returns to baseline
        length=10000
        
        timeline_rec_occupancy=np.zeros((int(length),3))
        pattern=[("b",1.0),("bu",rise_time,DA_rise_conc+a.DA_avg_conc)]
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        timeline_rec_occupancy[:,0]=np.add(timeline_rec_occupancy[:,0],BL_fraction_min_DA*DA_avg_conc)

    if pause_switch=="burst_w_pause":	#a ramp to DA_rise_conc in rise_time beginning after 1 s of baseline, then a firing pause of pause_length s, DA below baseline  signal over after DA conc. returns to baseline
        length=10000
        
        timeline_rec_occupancy=np.zeros((int(length),3))
        pattern=[("b",1.0),("buwp",rise_time,DA_rise_conc+a.DA_avg_conc,pause_length)]
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        timeline_rec_occupancy[:,0]=np.add(timeline_rec_occupancy[:,0],BL_fraction_min_DA*DA_avg_conc)
        
    if pause_switch=="just_pause":	#juts a pause with pause_length
        length=50000
        
        timeline_rec_occupancy=np.zeros((int(length),3))
        pattern=[("b",1.0),("pa",pause_length)]
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        timeline_rec_occupancy[:,0]=np.add(timeline_rec_occupancy[:,0],BL_fraction_min_DA*DA_avg_conc)

    if ((pause_switch=="40_10_bu_buwp") or (pause_switch=="50_00_bu_buwp") or (pause_switch=="40_00_bu_buwp") or (pause_switch=="50_bu")):	#This pattern are 50 trials, 40 with a burst, using the burst parameters supplied, and 10 burst pause , parameters used are pause_length supplied, and rise time and DA_rise_conc and rise_time are 0.5 times the burst value
        #t_repetition is set as a parameter to the function, default is 15.0s 
        
        length=1000000
        timeline_rec_occupancy=np.zeros((int(length),3))
        
        #finds the length of the burst so that burst+baseline together are t_repetition long 
        test_p=[("bu",rise_time,DA_rise_conc+a.DA_avg_conc)]
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,test_p)
        offset=timeline_rec_occupancy[:,0].argmax() #finds the time of the lowest value
        t_end_of_pulse=offset+np.argmax(timeline_rec_occupancy[offset:,0]==a.DA_avg_conc) 
        t_baseline_burst=t_repetition-(t_end_of_pulse/1000.0)	#how long does the baseline be so that the pattern is repeated every t_repetition
        
        #finds the length of the burst_pause so that burst_pause+baseline together are t_repetition long
        test_p=[("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length)]
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,test_p)
        offset=timeline_rec_occupancy[:,0].argmin() #finds the time of the lowest value
        t_end_of_pulse=offset+np.argmax(timeline_rec_occupancy[offset:,0]==a.DA_avg_conc) 
        t_baseline_wp=t_repetition-(t_end_of_pulse/1000.0)	#how long does the baseline be so that the 
        
        #make the patterns (pattern1->10 bursts, pattern 2-> 10 burst pauses)
        pattern1=[("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst),("bu",rise_time,DA_rise_conc+a.DA_avg_conc),("b",t_baseline_burst)]
        pattern2=[("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp),("buwp",0.5*rise_time,0.5*DA_rise_conc+a.DA_avg_conc,pause_length),("b",t_baseline_wp)]

        
        if pause_switch=="40_10_bu_buwp":
            pattern=[("b",10.0)]+pattern1+pattern1+pattern1+pattern1+pattern2
            t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        
        elif pause_switch=="50_00_bu_buwp":	#a ramp to 200nm in 75 ms with a  750ms firing pause of the neurons afterwards, this is for str because str has more dopaminergic terminals
            pattern=[("b",10.0)]+pattern1+pattern1+pattern1+pattern1+pattern1
            t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)	

        elif pause_switch=="40_00_bu_buwp":	#a ramp to 200nm in 75 ms with a  750ms firing pause of the neurons afterwards, this is for str because str has more dopaminergic terminals
            pattern=[("b",10.0)]+pattern1+pattern1+pattern1+pattern1
            t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)	
            
        elif pause_switch=="50_bu":
            pattern=[("b",10.0)]+pattern1+pattern1+pattern1+pattern1+pattern1
            t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        
        timeline_rec_occupancy[:,0]=np.add(timeline_rec_occupancy[:,0],BL_fraction_min_DA*DA_avg_conc)
            
    #------------------------------------------------------------------End off DA timeline generation -------------------------------------------------------#



    #run the actual ode integrator and save result in a file
    k_on=k_on/1000.0    #transform to 1/ms
    k_off=k_off/1000.0    #transform to 1/ms
    k_on_s2=k_on_s2/1000.0
    k_off_s2=k_off_s2/1000.0
    if ode_mode=='ode':
        #This is some stupid workaround ... since somehow when we run the RK integrator for the full length (300000 steps) it takes forever
        one_step_ode_int=100000
        if (length>one_step_ode_int):
                counter=0
                continue_calc=RL
                continue_calc_s2=RL_s2
                while (length>one_step_ode_int):
                    sol=odeint(deriv_RL,continue_calc,t_interp[(counter*one_step_ode_int):((counter+1)*one_step_ode_int)], args=(timeline_rec_occupancy[(counter*one_step_ode_int):((counter+1)*one_step_ode_int),0],t_interp[(counter*one_step_ode_int):((counter+1)*one_step_ode_int)],C_R_max,k_on,k_off),hmax=1.0)
                    timeline_rec_occupancy[(counter*one_step_ode_int):((counter+1)*one_step_ode_int),2]=sol.reshape(one_step_ode_int,) #sol.reshape(nr_steps,)
                    continue_calc=sol[-1,0]
                    if (frac_s2>0.0):   #if there are two states of the current receptor solve the second state too
                        sol_s2=odeint(deriv_RL,continue_calc_s2,t_interp[(counter*one_step_ode_int):((counter+1)*one_step_ode_int)], args=(timeline_rec_occupancy[(counter*one_step_ode_int):((counter+1)*one_step_ode_int),0],t_interp[(counter*one_step_ode_int):((counter+1)*one_step_ode_int)],C_R_max_s2,k_on_s2,k_off_s2),hmax=1.0)
                        timeline_rec_occupancy[(counter*one_step_ode_int):((counter+1)*one_step_ode_int),1]=sol_s2.reshape(one_step_ode_int,)
                        continue_calc_s2=sol_s2[-1,0]
                    
                    length-=one_step_ode_int
                    counter+=1
                    
                sol=odeint(deriv_RL,continue_calc,t_interp[(counter*one_step_ode_int):((counter*one_step_ode_int)+length)], args=(timeline_rec_occupancy[(counter*one_step_ode_int):((counter*one_step_ode_int)+length),0],t_interp[(counter*one_step_ode_int):((counter*one_step_ode_int)+length)],C_R_max,k_on,k_off),hmax=1.0)
                timeline_rec_occupancy[(counter*one_step_ode_int):((counter*one_step_ode_int)+length),2]=sol.reshape(length,)  
                
                if (frac_s2>0.0):   #if there are two states of the current receptor solve the second state too
                    sol_s2=odeint(deriv_RL,continue_calc_s2,t_interp[(counter*one_step_ode_int):((counter*one_step_ode_int)+length)], args=(timeline_rec_occupancy[(counter*one_step_ode_int):((counter*one_step_ode_int)+length),0],t_interp[(counter*one_step_ode_int):((counter*one_step_ode_int)+length)],C_R_max_s2,k_on_s2,k_off_s2),hmax=1.0)
                    timeline_rec_occupancy[(counter*one_step_ode_int):((counter*one_step_ode_int)+length),1]=sol_s2.reshape(length,)                        #s2_solution

        else:
                sol=odeint(deriv_RL,RL,t_interp, args=(timeline_rec_occupancy[:,0],t_interp,C_R_max,k_on,k_off),hmax=1.0)
                timeline_rec_occupancy[:,2]=sol.reshape(int(length),)
                if (frac_s2>0.0):   #if there are two states of the current receptor solve the second state too
                    sol_s2=odeint(deriv_RL,RL_s2,t_interp, args=(timeline_rec_occupancy[:,0],t_interp,C_R_max_s2,k_on_s2,k_off_s2),hmax=1.0)
                    timeline_rec_occupancy[:,1]=sol_s2.reshape(int(length),) 
    
    elif ode_mode=='Kd':
        #Fill the array with the instantaneous receptor occupation... if the system would always be in equilibrium
        for i in range(int(length)):
            L=timeline_rec_occupancy[i,0]
            timeline_rec_occupancy[i,2]=(L/(L+K_D))*C_R_max  #bound receptor default state
        
        if (frac_s2>0.0): #if there are two states of the current receptor solve the second state too
            for i in range(int(length)):
                L=timeline_rec_occupancy[i,0]
                timeline_rec_occupancy[i,1]=(L/(L+K_D_s2))*C_R_max_s2   #bound receptor s2
    
    #now we write out the files, if a filename is specified in kwargs we will use this, otherwise the file name will be build in a weird manner... for legacy reasons, so that the files can be read by the figure generating scripts 
    if 'filename' in kwargs:
        fn_D2=kwargs['filename']
    else:
        if fast_r!=1.0:  #only change the switch string if faster receptors are used
            print_switch=switch+'_fast_{:.0f}'.format(fast_r)
        else:
            print_switch=switch
        
        if pause_switch=="burst":
            fn_D2="free_"+str(print_switch)+"_"+str(pause_switch)+"_"+str(V_max)+"_"+str(rise_time)+"_"+str(DA_rise_conc)
        elif pause_switch=="burst_w_pause":
            fn_D2="free_"+str(print_switch)+"_"+str(pause_switch)+"_"+str(V_max)+"_"+str(rise_time)+"_"+str(DA_rise_conc)+"_"+str(pause_length)
        elif pause_switch=="just_pause":
            fn_D2="free_"+str(print_switch)+"_"+str(pause_switch)+"_"+str(V_max)+"_"+str(pause_length)
        elif pause_switch=="50_bu":
            fn_D2="free_"+str(print_switch)+"_"+str(pause_switch)+"_"+str(V_max)+"_"+str(rise_time)+"_"+str(DA_rise_conc)+"_"+str(t_repetition)
        else:
            fn_D2="free_"+str(print_switch)+"_"+str(pause_switch)
        
        if frac_s2>0.0:
            fn_D2+="_frac_s2_"+str(frac_s2)
        if BL_fraction_min_DA>0.0:
            fn_D2+="_BL_"+str(BL_fraction_min_DA)
        
        if ode_mode=='Kd':
            fn_D2+='_Kd.dat'
        else:
            fn_D2+='.dat'
        
    pickle.dump( timeline_rec_occupancy, open( fn_D2, "wb" ) )
