#!/usr/bin/env python
''' This script generates Panel c and d for Figure 1 and Figure 10 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''



import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, VPacker


class Receptor():
	def __init__(self, C_max,k_on,k_off,C_L,C_L_0):
		self.C_max=C_max
		self.k_on=k_on
		self.k_off=k_off
		self.K_D=k_off/k_on
		self.C_L = C_L
		self.C_L_0 = C_L_0
		self.C_RL_eq = self.C_max*(self.C_L/(self.K_D+self.C_L))
		self.C_RL_0 = self.C_max*(self.C_L_0/(self.K_D+self.C_L_0))
		


DA_high=1000.0  #DA concentration in nM after the step-up
DA_low=20.0      #DA_concentration in nM at Baseline
frac_s2=0.1

D1_forw=Receptor(1600.0, #D1 total concentration of receptors in nM
				0.0003125/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_high,     #DA_concentration before the step
				DA_low        #DA_concentration after the step
				)

D2_forw=Receptor(80.0, #D2 total concentration of receptors in nM
				0.02/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_high,     #DA_concentration before the step
				DA_low        #DA_concentration after the step
				)




D1_back=Receptor(1600.0, #437.5, #D1 Equilibrium receptor activation for C_DA=1000nm
				0.0003125/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_low,     #DA_concentration before the step
				DA_high     #DA_concentration after the step
				)

D2_back=Receptor(80.0,	#29.1, #D2 Equilibrium receptor activation for C_DA=1000nm
				0.02/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_low,     #DA_concentration before the step
				DA_high     #DA_concentration after the step
				)




def f(t,D):
	return np.heaviside(-1.0*t,1.0)*D.C_RL_0+np.heaviside(t,1.0)*((D.C_RL_0-D.C_RL_eq)*np.exp(-(D.k_on*D.C_L+D.k_off)*t)+D.C_RL_eq)




plt.rc('text', usetex=True)
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
matplotlib.rcParams.update({'font.size': 18})

# Say, "the default sans-serif font is helvetica"
#matplotlib.rcParams['font.sans-serif'] = "Helvetica"
# Then, "ALWAYS use sans-serif fonts"
#matplotlib.rcParams['font.family'] = "sans-serif"

ax = plt.subplot(111)
ax2=ax.twinx()

times=np.arange(-50.0,1000.0,0.01)
times_pos=np.arange(0.0,1000.0,0.01)
const=np.ones(times_pos.shape)
const_D1=np.ones(times_pos.shape)
const*=D2_back.C_RL_eq
const_D1*=D1_back.C_RL_eq

#plt.plot(times,f(times,D1_back),label="D1 active conc.")
ax.plot(times,f(times,D2_back),label="D2",lw=3)
ax2.plot(times,f(times,D1_back),label="D1",color='C1',lw=3)
#plt.plot(times,const,':',color='#D3D3D3',label="Equilibrium value [D2-DA]",lw=3)
ax.plot(times_pos,const,':',color='#A9A9A9',lw=3)
ax2.plot(times_pos,const_D1,':',color='#A9A9A9',lw=3)
#ax.set_xlabel("Time (s)")



# y-axis label
ybox1 = TextArea("[D1-DA]", textprops=dict(color="r", size=18,rotation='vertical',ha='left',va='bottom'))
ybox2 = TextArea(" and ", textprops=dict(color="k", size=18,rotation='vertical',ha='left',va='bottom'))
ybox3 = TextArea("[D2-DA]", textprops=dict(color="b", size=18,rotation='vertical',ha='left',va='bottom'))
ybox4 = TextArea(" in nM", textprops=dict(color="k", size=18,rotation='vertical',ha='left',va='bottom'))

ybox = VPacker(children=[ybox4, ybox3, ybox2, ybox1],
				  align="baseline",mode='fixed', pad=0, sep=0)

anchored_ybox = AnchoredOffsetbox(loc=6, child=ybox, pad=-1., frameon=False,
									  bbox_to_anchor=None,
									  bbox_transform=ax.transAxes, borderpad=-1.75)








#plt.ylabel("[D2-DA] in nM")

ax_min=0.0		#minimum of axis 2 with the K_D kinetics
ax_max=50.0	#maximum of axis 2 with the K_D kinetics
ax2_max=640.0		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax2_min=ax2_max-((ax2_max-D1_back.C_RL_0)/(ax_max-D1_back.C_RL_0))*(ax_max-ax_min)


ax.set_xlim(-50,500)
ax.set_ylim(0,100.0)
ax2.set_ylim(0,ax2_max)

ax.text(230.0, 40.0, 'EQM value [D2-DA]',fontsize=18, color='#A9A9A9', va='center', rotation='horizontal')
ax2.text(10.0, 50.0, 'EQM value [D1-DA]',fontsize=18, color='#A9A9A9', va='center', rotation='horizontal')

#ax.legend()
#ax.add_artist(anchored_ybox)
#plt.tight_layout()
#plt.gcf().subplots_adjust(bottom=0.15)
plt.savefig("Fig_1_d_Act_backward.pdf",transparent = True)

#plt.show()







plt.clf()
times=np.arange(-50.0,100.0,0.01)
const=np.ones(times_pos.shape)
const*=D2_forw.C_RL_eq

#plt.plot(times,const,':',color='#D3D3D3',label="Equilibrium value [D2-DA]",lw=3)
plt.plot(times_pos,const,':',color='#A9A9A9',lw=3)
plt.plot(times,f(times,D2_forw),label="D2",lw=3)
plt.plot(times,f(times,D1_forw),label="D1",lw=3)


#plt.xlabel("Time (s)")


plt.xlim(-2,10)
plt.ylim(0,100.0)

plt.text(0.8, 82.0, 'EQM value [D2-DA]',fontsize=18, color='#A9A9A9', va='center', rotation='horizontal')

#plt.legend(loc=4)
#plt.tight_layout()
#plt.gcf().subplots_adjust(bottom=0.15)
plt.savefig("Fig_1_c_Act_forward.pdf",transparent = True)
#plt.show()





#new figure with two state receptors
D1_forw_s2_low=Receptor(1600.0-frac_s2*1600.0, #D1 total concentration of receptors in nM
				0.0003125/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_high,     #DA_concentration before the step
				DA_low        #DA_concentration after the step
				)

D1_forw_s2_high=Receptor(frac_s2*1600.0, #D1 total concentration of receptors in nM
				0.02/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_high,     #DA_concentration before the step
				DA_low        #DA_concentration after the step
				)

D1_back_s2_low=Receptor(1600.0-frac_s2*1600.0, #437.5, #D1 Equilibrium receptor activation for C_DA=1000nm
				0.0003125/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_low,     #DA_concentration before the step
				DA_high     #DA_concentration after the step
				)

D1_back_s2_high=Receptor(frac_s2*1600.0, #437.5, #D1 Equilibrium receptor activation for C_DA=1000nm
				0.02/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_low,     #DA_concentration before the step
				DA_high     #DA_concentration after the step
				)

D2_forw_s2_high=Receptor(80.0-frac_s2*80.0, #D2 total concentration of receptors in nM
				0.02/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_high,     #DA_concentration before the step
				DA_low        #DA_concentration after the step
				)
                
D2_forw_s2_low=Receptor(frac_s2*80.0, #D2 total concentration of receptors in nM
				0.0003125/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_high,     #DA_concentration before the step
				DA_low        #DA_concentration after the step
				)

D2_back_s2_high=Receptor(80.0-frac_s2*80.0, #D2 total concentration of receptors in nM
				0.02/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_low,     #DA_concentration before the step
				DA_high      #DA_concentration after the step
				)

D2_back_s2_low=Receptor(frac_s2*80.0, #D2 total concentration of receptors in nM
				0.0003125/60.0, #k_on of D1
				0.5/60.0,	#k_off of D1
				DA_low,     #DA_concentration before the step
				DA_high        #DA_concentration after the step
				)






times=np.arange(-50.0,100.0,0.01)
const=np.ones(times_pos.shape)

const=np.ones(times_pos.shape)
const_D1=np.ones(times_pos.shape)


const*=(D2_forw_s2_high.C_RL_eq+D2_forw_s2_low.C_RL_eq)
const_D1*=(D1_forw_s2_high.C_RL_eq+D1_forw_s2_low.C_RL_eq)

plt.clf()
#plt.plot(times,const,':',color='#D3D3D3',label="Equilibrium value [D2-DA]",lw=3)
plt.plot(times_pos,const,':',color='#A9A9A9',lw=3)
plt.plot(times,np.add(f(times,D2_forw_s2_high),f(times,D2_forw_s2_low)),color='C0',label='$D2^{high}+D2^{low}$',lw=3)
plt.plot(times,np.add(f(times,D1_forw_s2_high),f(times,D1_forw_s2_low)),color='C1',label='$D1^{high}+D1^{low}$',lw=3)
plt.plot(times,f(times,D1_forw_s2_high),label='$D1^{high}$',color='C1',ls='--',lw=3,alpha=0.7)
plt.plot(times,f(times,D1_forw_s2_low),label='$D1^{low}$',color='C1',ls='-.',lw=3,alpha=0.7)



#plt.xlabel("Time (s)")


plt.xlim(-2,10)
plt.ylim(0,250.0)

plt.text(0.8, 82.0, 'EQM value [D2-DA]',fontsize=18, color='#A9A9A9', va='center', rotation='horizontal')

#plt.legend(loc=4)
#plt.tight_layout()
#plt.gcf().subplots_adjust(bottom=0.15)

plt.legend(fontsize=15)
plt.savefig("Fig_10_c_Act_forward.pdf",transparent = True)
#plt.show()



plt.clf()
ax = plt.subplot(111)
ax2=ax.twinx()

times=np.arange(-50.0,1000.0,0.01)
times_pos=np.arange(0.0,1000.0,0.01)
const=np.ones(times_pos.shape)
const_D1=np.ones(times_pos.shape)
const*=(D2_back_s2_high.C_RL_eq+D2_back_s2_low.C_RL_eq)
const_D1*=(D1_back_s2_high.C_RL_eq+D1_back_s2_low.C_RL_eq)



ax.plot(times_pos,const,':',color='#A9A9A9',lw=3)
ax2.plot(times_pos,const_D1,':',color='#A9A9A9',lw=3)


ax.plot(times,np.add(f(times,D2_back_s2_high),f(times,D2_back_s2_low)),color='C0',label='$D2^{high}+D2^{low}$',lw=3)
ax2.plot(times,np.add(f(times,D1_back_s2_high),f(times,D1_back_s2_low)),color='C1',label='$D1^{high}+D1^{low}$',lw=3)
ax2.plot(times,f(times,D1_back_s2_high),color='C1',ls='--',label='$D1^{high}$',lw=3,alpha=0.7)
ax2.plot(times,f(times,D1_back_s2_low),color='C1',ls='-.',label='$D1^{low}$',lw=3,alpha=0.7)






ax_min=0.0		#minimum of axis 2 with the K_D kinetics
ax_max=50.0	#maximum of axis 2 with the K_D kinetics
ax2_max=800.0		# maximum of axis 1 with the real kinetics... used to calculate the minimum so that the baseline aligns
ax2_min=ax2_max-((ax2_max-D1_back.C_RL_0)/(ax_max-D1_back.C_RL_0))*(ax_max-ax_min)


ax.set_xlim(-50,500)
ax.set_ylim(0,100.0)
ax2.set_ylim(0,ax2_max)


ax.text(250.0, 37.0, 'EQM value [D2-DA]',fontsize=18, color='#A9A9A9', va='center', rotation='horizontal')
ax2.text(0.8, 50.0, 'EQM value [D1-DA]',fontsize=18, color='#A9A9A9', va='center', rotation='horizontal')


h1, l1=ax.get_legend_handles_labels()
h2, l2=ax2.get_legend_handles_labels()
plt.legend(h1+h2,l1+l2,fontsize=15)


plt.savefig("Fig_10_d_Act_backward.pdf",transparent = True)

plt.show()






























