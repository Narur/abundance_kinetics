#!/usr/bin/env python    
''' This script generates Panel all panels of Figure 9 of 
    the Hunger, Kumar, Schmidt 2019 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    (after the data has been generated with the Make_data script)

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''    
    
    
    
    
    
    
    

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
import math as m
import pickle
import os
nr_steps=5000

#Where am I and where are the data files ? (assuming the data files have been made with the Make_the Data script)
my_path=os.path.dirname(os.path.realpath(__file__)) #Where does the script currently live 
res_path=os.path.join(my_path, "Data/Diff_Supp_Fig_1_kinetics") #make a Data folder with a subfolder for the kinetic data


D1=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D1_fast_2=pickle.load(open(os.path.join(res_path,"free_D1_fast_2_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D1_fast_10=pickle.load(open(os.path.join(res_path,"free_D1_fast_10_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D1_fast_50=pickle.load(open(os.path.join(res_path,"free_D1_fast_50_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D1_fast_100=pickle.load(open(os.path.join(res_path,"free_D1_fast_100_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D1_Kd=pickle.load(open(os.path.join(res_path,"free_D1_burst_w_pause_1.5_0.1_100.0_1.0_Kd.dat"),"rb"))

D2=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D2_fast_2=pickle.load(open(os.path.join(res_path,"free_D2_fast_2_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D2_fast_10=pickle.load(open(os.path.join(res_path,"free_D2_fast_10_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D2_fast_50=pickle.load(open(os.path.join(res_path,"free_D2_fast_50_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D2_fast_100=pickle.load(open(os.path.join(res_path,"free_D2_fast_100_burst_w_pause_1.5_0.1_100.0_1.0.dat"),"rb"))
D2_Kd=pickle.load(open(os.path.join(res_path,"free_D2_burst_w_pause_1.5_0.1_100.0_1.0_Kd.dat"),"rb"))

#Make The long bursts
D1_lb=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0.dat"),"rb"))
D1_fast_2_lb=pickle.load(open(os.path.join(res_path,"free_D1_fast_2_burst_1.5_0.2_200.0.dat"),"rb"))
D1_fast_10_lb=pickle.load(open(os.path.join(res_path,"free_D1_fast_10_burst_1.5_0.2_200.0.dat"),"rb"))
D1_fast_50_lb=pickle.load(open(os.path.join(res_path,"free_D1_fast_50_burst_1.5_0.2_200.0.dat"),"rb"))
D1_fast_100_lb=pickle.load(open(os.path.join(res_path,"free_D1_fast_100_burst_1.5_0.2_200.0.dat"),"rb"))

D2_lb=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0.dat"),"rb"))
D2_fast_2_lb=pickle.load(open(os.path.join(res_path,"free_D2_fast_2_burst_1.5_0.2_200.0.dat"),"rb"))
D2_fast_10_lb=pickle.load(open(os.path.join(res_path,"free_D2_fast_10_burst_1.5_0.2_200.0.dat"),"rb"))
D2_fast_50_lb=pickle.load(open(os.path.join(res_path,"free_D2_fast_50_burst_1.5_0.2_200.0.dat"),"rb"))
D2_fast_100_lb=pickle.load(open(os.path.join(res_path,"free_D2_fast_100_burst_1.5_0.2_200.0.dat"),"rb"))

D1_Kd_lb=pickle.load(open(os.path.join(res_path,"free_D1_burst_1.5_0.2_200.0_Kd.dat"),"rb"))
D2_Kd_lb=pickle.load(open(os.path.join(res_path,"free_D2_burst_1.5_0.2_200.0_Kd.dat"),"rb"))



#Also load the sequences of 50 events
D1_50_burst_trep_15=pickle.load(open(os.path.join(res_path,"free_D1_50_bu_1.5_0.2_200.0_15.0.dat"),"rb"))
D1_fast_2_50_burst_trep_15=pickle.load(open(os.path.join(res_path,"free_D1_fast_2_50_bu_1.5_0.2_200.0_15.0.dat"),"rb"))

D2_50_burst_trep_15=pickle.load(open(os.path.join(res_path,"free_D2_50_bu_1.5_0.2_200.0_15.0.dat"),"rb"))
D2_fast_2_50_burst_trep_15=pickle.load(open(os.path.join(res_path,"free_D2_fast_2_50_bu_1.5_0.2_200.0_15.0.dat"),"rb"))

D2_fast_10_50_burst_trep_15=pickle.load(open(os.path.join(res_path,"free_D2_fast_10_50_bu_1.5_0.2_200.0_15.0.dat"),"rb"))
D1_fast_10_50_burst_trep_15=pickle.load(open(os.path.join(res_path,"free_D1_fast_10_50_bu_1.5_0.2_200.0_15.0.dat"),"rb"))



plt.close('all')
#Figure with Different kinetics for the D1 receptors
plt.rc('text', usetex=True)	#use latex for all tex, that makes the font the same since some figures require us to use Latex
plt.rc('font', weight='bold')
plt.rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']

matplotlib.rcParams.update({'font.size': 18})

fig=plt.figure()
fig.clf()
time=np.arange(len(D1[:,0]))

ax1=fig.add_subplot(111)
ax1.set_xlim([0,5.0])
ax1.set_ylim([0,90.0])


#fig.suptitle('Concentration of active D1 receptor')
#ax1.set_ylabel('[D1-DA] in nM',fontsize=15)
#ax1.set_xlabel('Time (s)',fontsize=15)


ax1.plot(time/1000.0,D1_Kd[:,2],'--',color='#D3D3D3',lw=3,label=r'$q=\infty$ (instant)')
ax1.plot(time/1000.0,D1[:,2],lw=3,label='$q=1$')
ax1.plot(time/1000.0,D1_fast_2[:,2],lw=3,label='$q=2$')
ax1.plot(time/1000.0,D1_fast_10[:,2],lw=3,label='$q=10$')
ax1.plot(time/1000.0,D1_fast_50[:,2],lw=3,label='$q=50$')
ax1.plot(time/1000.0,D1_fast_100[:,2],lw=3,label='$q=100$')

ax1.legend(fontsize=15,loc=1)

filename="Fig_9_a_D1_faster_kinetics.pdf"

fig.savefig(filename)

#fig.show()
fig.clf()


#Figure with Different kinetics for the D2 receptors
fig=plt.figure()
fig.clf()




time=np.arange(len(D2[:,0]))

ax1=fig.add_subplot(111)
ax1.set_xlim([0,5.0])
ax1.set_ylim([0,90.0])


#fig.suptitle('Concentration of active D2 receptor')
#ax1.set_ylabel('[D2-DA] in nM',fontsize=15)
#ax1.set_xlabel('Time in s',fontsize=15)

ax1.plot(time/1000.0,D2_Kd[:,2],'--',color='#D3D3D3',lw=3,label=r'$q=\infty$ (instant)')
ax1.plot(time/1000.0,D2[:,2],lw=3,label='$q=1$')
ax1.plot(time/1000.0,D2_fast_2[:,2],lw=3,label='$q=2$')
ax1.plot(time/1000.0,D2_fast_10[:,2],lw=3,label='$q=10$')
ax1.plot(time/1000.0,D2_fast_50[:,2],lw=3,label='$q=50$')
ax1.plot(time/1000.0,D2_fast_100[:,2],lw=3,label='$q=100$')


ax1.legend(fontsize=15,loc=1)

filename="Fig_9_b_D2_faster_kinetics.pdf"

fig.savefig(filename)


#fig.show()
fig.clf()






#Figure with Different kinetics for the D1 receptors long burst 


fig=plt.figure()
fig.clf()
time=np.arange(len(D1[:,0]))

ax1=fig.add_subplot(111)
ax1.set_xlim([0,5.0])
ax1.set_ylim([0,90.0])


#fig.suptitle('Concentration of active D1 receptor')
#ax1.set_ylabel('[D1-DA] in nM',fontsize=15)
#ax1.set_xlabel('Time in s',fontsize=15)


ax1.plot(time/1000.0,D1_Kd_lb[:,2],'--',color='#D3D3D3',lw=3,label=r'$q=\infty$ (instant)')
ax1.plot(time/1000.0,D1_lb[:,2],lw=3,label='$q=1$')
ax1.plot(time/1000.0,D1_fast_2_lb[:,2],lw=3,label='$q=2$')
ax1.plot(time/1000.0,D1_fast_10_lb[:,2],lw=3,label='$q=10$')
ax1.plot(time/1000.0,D1_fast_50_lb[:,2],lw=3,label='$q=50$')
ax1.plot(time/1000.0,D1_fast_100_lb[:,2],lw=3,label='$q=100$')

ax1.legend(fontsize=15,loc=1)

filename="Fig_9_c_D1_faster_kinetics_longburst.pdf"

fig.savefig(filename)

#fig.show()
fig.clf()


#Figure with Different kinetics for the D2 receptors long burst
fig=plt.figure()
fig.clf()
time=np.arange(len(D2[:,0]))

ax1=fig.add_subplot(111)
ax1.set_xlim([0,5.0])
ax1.set_ylim([0,90.0])


#fig.suptitle('Concentration of active D2 receptor')
#ax1.set_ylabel('[D2-DA] in nM',fontsize=15)
#ax1.set_xlabel('Time in s',fontsize=15)

ax1.plot(time/1000.0,D2_Kd_lb[:,2],'--',color='#D3D3D3',lw=3,label=r'$q=\infty$ (instant)')
ax1.plot(time/1000.0,D2_lb[:,2],lw=3,label='$q=1$')
ax1.plot(time/1000.0,D2_fast_2_lb[:,2],lw=3,label='$q=2$')
ax1.plot(time/1000.0,D2_fast_10_lb[:,2],lw=3,label='$q=10$')
ax1.plot(time/1000.0,D2_fast_50_lb[:,2],lw=3,label='$q=50$')
ax1.plot(time/1000.0,D2_fast_100_lb[:,2],lw=3,label='$q=100$')


ax1.legend(fontsize=15,loc=1)

filename="Fig_9_d_D2_faster_kinetics_longburst.pdf"

fig.savefig(filename)




#fig.show()
fig.clf()


#Plot receptor activation for D1  receptors for the timecourse of NAcc 
fig=plt.figure()
fig.clf()
time=np.arange(len(D1_50_burst_trep_15[:,0]))
ax1=fig.add_subplot(111)


ax1.set_ylim(19.0,28.0)
#ax2.set_ylim(ax2_min,ax2_max)


ax1.plot(time/1000.0,D1_fast_10_50_burst_trep_15[:,2],lw=3,label=r'$q=10$')
ax1.plot(time/1000.0,D1_fast_2_50_burst_trep_15[:,2],lw=3,label=r'$q=2$')
ax1.plot(time/1000.0,D1_50_burst_trep_15[:,2],lw=3,label=r'$q=1$')


#ax1.set_ylabel('[D1-DA] in nM',fontsize=15)

#ax1.set_xlabel('Time in s',fontsize=15)

#fig.suptitle('D1 receptor activation, bursts - different kinetics')

ax1.legend(fontsize=15,loc=1)

filename="Fig_9_e_D1_long_diff_kinetics.pdf"

fig.savefig(filename)

#fig.show()
fig.clf()



#Plot receptor activation for D2  receptors for the timecourse of NAcc 
fig=plt.figure()
fig.clf()
time=np.arange(len(D2_50_burst_trep_15[:,0]))
ax1=fig.add_subplot(111)


ax1.set_ylim(35.0,44.0)
#ax2.set_ylim(ax2_min,ax2_max)


ax1.plot(time/1000.0,D2_fast_10_50_burst_trep_15[:,2],lw=3,label=r'$q=10$')
ax1.plot(time/1000.0,D2_fast_2_50_burst_trep_15[:,2],lw=3,label=r'$q=2$')
ax1.plot(time/1000.0,D2_50_burst_trep_15[:,2],lw=3,label=r'$q=1$')


#ax1.set_ylabel('[D2-DA] in nM',fontsize=15)

#ax1.set_xlabel('Time in s',fontsize=15)

#fig.suptitle('D2 receptor activation, bursts - different kinetics')

ax1.legend(fontsize=15,loc=1)

filename="Fig_9_f_D2_long_diff_kinetics.pdf"

fig.savefig(filename)

#fig.show()
fig.clf()








































