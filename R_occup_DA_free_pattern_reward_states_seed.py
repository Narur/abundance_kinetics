#!/usr/bin/env python
''' This file contains the python script that uses the Generate_DA_timeline helper class to generate Dopamine timelines for behavioral sequences
    and subsequently uses Runge Kutta integration to calculate the resulting D1 and D2 receptor activations caused by these DA timecourses. 
    This file accompanies the Hunger, Kumar, Schmidt 2018 Publication titled: "Abundance compensates kinetics: Similar effect of dopamine signals on D1 and D2 receptor populations".
    The function R_occup_sequences is called by Make_the_data_ROC but can also be called directly from python after importing.
    

    Copyright (C) 2018 Lars Hunger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''



import numpy as np 
import cPickle as pickle
import random as rand
import math
import os   #operating system
from shutil import copyfile #for copying files
from scipy import interpolate
from scipy.integrate import odeint
import sys
import Generate_DA_timeline as GTL

#function that calculates the derivative of [RL]
def deriv_RL(C_LR,t,C_DA,C_DA_times,C_R_max,k_on,k_off):
    Current_C_DA=np.interp(t, C_DA_times, C_DA)	#interpolates the dopamine concentration linearly from the supplied curve
    return k_on*Current_C_DA*C_R_max-k_on*Current_C_DA*C_LR-k_off*C_LR


def R_occup_sequences(switch="D1",  #which receptor to run the sequence for
                      pause_switch="reward_states_prob_jitter", #reward states should be modeled with probability (if a trial is rewarded is a random decision before each trial, trial starts are jittered )
                      p_burst=0.5,  #probability of rewarding event, burst on each trial
                      p_buwp=0.5,   #probability of burst with pause on each trial, as a non rewarding event
                      p_pause=0.0,  #probability of pause happening on each trial (not used) 
                      nr_events=50, #how many trials constitute one sequence
                      running_nr=1, #running_nr
                    D1_base_C_max=1600.0, #base concentration of D1R
                    D2_base_C_max=400.0): #base concentration of D2R

        
    area_switch="Nacc"	
        
        
    #This part is a mess, here basically the receptor paramters are set according to switch... why this is not done by directly setting them legacy reasons
    
    if switch=="D1":	#Dataset for D1 parameters, modified so that K_D is 1600nM, but k_on made faster and k_off slower ... so both are modified by a similar degree (around 20%)
        C_R_max=D1_base_C_max
        k_on=0.0003125/60.0 #on rate in nM/second
        k_off=0.5/60.0 #off rate in 1/second
    

    elif switch=="D2":	#Dataset for D2 parameters, with 90% receptors internalized
        k_on=0.02/60.0 #on rate in nM/second
        k_off=0.5/60.0 #off rate in 1/second
        C_R_max=D2_base_C_max*0.2
    

    seed_id=running_nr+int(p_burst*100000)
    rand.seed(seed_id)	#so that the seeds are the sme for D1 and D2 but different for different p_br

    if (p_burst+p_buwp+p_pause>1.0):
        print "Warning the sum of your probabilities is >1 that is undefined in this programm... whatever comes out of this is probably not what you want"




    V_max=1.5

    #general parameters
    K_D=k_off/k_on
    dt=1.0 #timestep in ms
    DA_avg_conc=20.0

    AVG_frac_occupancy=DA_avg_conc/(DA_avg_conc+K_D)
    R=(1.0-AVG_frac_occupancy)*C_R_max
    RL=AVG_frac_occupancy*C_R_max










    a=GTL.DA_timelines()	#make a a DA timelines object... that will generate us our DA timeline
    a.V_max=V_max
    a.DA_avg_conc=DA_avg_conc
    t_repetition=15.0	#ever how many s should the "experiment" be repeated



    length=1000000
    timeline_rec_occupancy=np.zeros((int(length),3))
    #here we figure out how long the baseline after each pattern has to be so that all the events start every t_repetition
    test_p=[("bu",0.2,200.0+a.DA_avg_conc)]
    t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,test_p)
    offset=timeline_rec_occupancy[:,0].argmax() #finds the time of the lowest value
    t_end_of_pulse=offset+np.argmax(timeline_rec_occupancy[offset:,0]==a.DA_avg_conc) 
    t_baseline_burst=t_repetition-(t_end_of_pulse/1000.0)	#how long does the baseline be so that the next event can start after t_repetition

    test_p=[("buwp",0.1,100.0+a.DA_avg_conc,1.3)]
    t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,test_p)
    offset=timeline_rec_occupancy[:,0].argmin() #finds the time of the lowest value
    t_end_of_pulse=offset+np.argmax(timeline_rec_occupancy[offset:,0]==a.DA_avg_conc) 
    t_baseline_buwp=t_repetition-(t_end_of_pulse/1000.0)	#how long does the baseline be so that the next event can start after t_repetition
        
    test_p=[("pa",1.5)]
    t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,test_p)
    offset=timeline_rec_occupancy[:,0].argmin() #finds the time of the lowest value
    t_end_of_pulse=offset+np.argmax(timeline_rec_occupancy[offset:,0]==a.DA_avg_conc) 
    t_baseline_pa=t_repetition-(t_end_of_pulse/1000.0)	#how long does the baseline be so that the next event can start after t_repetition





    if pause_switch=="reward_states_prob":	
        pattern=[("b",10.0)]
        length=1000000	#1000s long 
        timeline_rec_occupancy=np.zeros((int(length),3))
        nr_bursts=0
        nr_buwp=0
        nr_pause=0
        nr_base=0
        
        for i in xrange(nr_events):
            rnd=rand.random()
            if (rnd<p_burst):	#is it a burst
                pattern.append(("bu",0.2,200.0+a.DA_avg_conc))
                pattern.append(("b",t_baseline_burst))
                nr_bursts+=1
                
            elif (rnd<p_burst+p_buwp):	#is it a burst with pause?
                pattern.append(("buwp",0.1,100.0+a.DA_avg_conc,1.3))
                pattern.append(("b",t_baseline_buwp))
                nr_buwp+=1
                
            elif (rnd<p_burst+p_buwp+p_pause):	#is it just a pause?
                pattern.append(("pa",1.5))
                pattern.append(("b",t_baseline_pa))
                nr_pause+=1

            else: #if it@s none of the above it@s just going to be a baselinesection
                pattern.append(("b",t_repetition))
                nr_base=+1
        
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        print "bursts,buwp,pause,baseline:",nr_bursts ,nr_buwp, nr_pause, nr_base, seed_id


    if pause_switch=="reward_states_shuffle":	#here the probabilities are turned into expected values, and then the amount of expected value of each event is put in a list which is shuffled after
        pattern_sig=[]	#have a list with the signals and the pauses, shuffle them zipped together so and then merge them 
        pattern_pauses=[]
        length=1000000
        timeline_rec_occupancy=np.zeros((int(length),3))
        nr_bursts=int(p_burst*nr_events)
        nr_buwp=int(p_buwp*nr_events)
        nr_pause=int(p_pause*nr_events)
        nr_base=int((1.0-p_burst-p_buwp-p_pause)*nr_events)
        #print nr_base
        for i in xrange(nr_bursts):	
            pattern_sig.append(("bu",0.2,200.0+a.DA_avg_conc))
            pattern_pauses.append(("b",t_baseline_burst))

        for i in xrange(nr_buwp):	
            pattern_sig.append(("buwp",0.1,100.0+a.DA_avg_conc,1.3))
            pattern_pauses.append(("b",t_baseline_buwp))
            
        for i in xrange(nr_pause):	
            pattern_sig.append(("pa",1.5))
            pattern_pauses.append(("b",t_baseline_pa))
            
        for i in xrange(nr_base):	
            pattern_sig.append(("b",t_repetition))
            pattern_pauses.append(("b",0))	#needs to be added so that the following shuffle works
        
        #puts pause list and signal list together and shuffles them with same order 
        zipped_list=list(zip(pattern_sig,pattern_pauses))
        rand.shuffle(zipped_list)
        pattern_sig, pattern_pauses=[list(t) for t in zip(*zipped_list)]	#separates the zipped signal and pause list 
        #interleaves the signal and pause list
        pattern=pattern_sig+pattern_pauses
        pattern[::2]=pattern_sig
        pattern[1::2]=pattern_pauses
        #add a 10 second pause add beginning of pattern
        pattern=[("b",10.0)]+pattern 
        
        
        #print pattern
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)





    if pause_switch=="reward_states_prob_jitter":	#a ramp to 200nm in 75 ms with a  750ms firing pause of the neurons afterwards, this is for str because str has more dopaminergic terminals
        pattern=[("b",10.0)]
        length=1000000	#1000s long 
        timeline_rec_occupancy=np.zeros((int(length),3))
        nr_bursts=0
        nr_buwp=0
        nr_pause=0
        nr_base=0
        
        #t_repetition is assumed to be 15.0 here ... we will jitter the presentation of the stimulus uniformly in a 10 second interval, so we will add a random numbe rbetween 0 and 10 and subtract 5 (to keep the average centered at t_rep)
        
        
        for i in xrange(nr_events):
            rnd=rand.random()
            rnd_jitter=5.0-(10.0*rand.random())
            if (rnd<p_burst):	#is it a burst
                
                pattern.append(("bu",0.2,200.0+a.DA_avg_conc))
                pattern.append(("b",t_baseline_burst+rnd_jitter))
                nr_bursts+=1
            
            elif (rnd<p_burst+p_buwp):	#is it a burst with pause?
                pattern.append(("buwp",0.1,100.0+a.DA_avg_conc,1.3))
                pattern.append(("b",t_baseline_buwp+rnd_jitter))
                nr_buwp+=1
                
            elif (rnd<p_burst+p_buwp+p_pause):	#is it just a pause?
                pattern.append(("pa",1.5))
                pattern.append(("b",t_baseline_pa+rnd_jitter))
                nr_pause+=1

            else: #if it@s none of the above it@s just going to be a baselinesection
                pattern.append(("b",t_repetition+rnd_jitter))
                nr_base=+1
        
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        print "bursts,buwp,pause,baseline:",nr_bursts ,nr_buwp, nr_pause, nr_base, seed_id
        
    if pause_switch=="reward_states_prob_jitter_DA_decrease":	#This is like reward_states_prob_jitter but the size of the DA pulse decreases linearly from 200nm at p=0.1 to 75 nm at p=1.0 depending on release probability
        pattern=[("b",10.0)]
        length=1000000	#1000s long 
        timeline_rec_occupancy=np.zeros((int(length),3))
        nr_bursts=0
        nr_buwp=0
        nr_pause=0
        nr_base=0
        DA_Pulse_max=125.0/0.9 #maximum height of the pulse, chosen so that the height at p=0.1 is 200 nM
        
        #t_repetition is assumed to be 15.0 here ... we will jitter the presentation of the stimulus uniformly in a 10 second interval, so we will add a random numbe rbetween 0 and 10 and subtract 5 (to keep the average centered at t_rep)
        
        
        for i in xrange(nr_events):
            rnd=rand.random()
            rnd_jitter=5.0-(10.0*rand.random())
            if (rnd<p_burst):	#is it a burst
                DA_pulse_height=75.0+DA_Pulse_max*(1.0-p_burst)
                pattern.append(("bu",0.2,DA_pulse_height+a.DA_avg_conc))
                pattern.append(("b",t_baseline_burst+rnd_jitter))
                nr_bursts+=1
            
            elif (rnd<p_burst+p_buwp):	#is it a burst with pause?
                pattern.append(("buwp",0.1,100.0+a.DA_avg_conc,1.3))
                pattern.append(("b",t_baseline_buwp+rnd_jitter))
                nr_buwp+=1
                
            elif (rnd<p_burst+p_buwp+p_pause):	#is it just a pause?
                pattern.append(("pa",1.5))
                pattern.append(("b",t_baseline_pa+rnd_jitter))
                nr_pause+=1

            else: #if it@s none of the above it@s just going to be a baselinesection
                pattern.append(("b",t_repetition+rnd_jitter))
                nr_base=+1
        
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)
        print "bursts,buwp,pause,baseline:",nr_bursts ,nr_buwp, nr_pause, nr_base, seed_id


    if pause_switch=="reward_states_shuffle_jitter":	#here the probabilities are turned into expected values, and then the amount of expected value of each event is put in a list which is shuffled after
        pattern_sig=[]	#have a list with the signals and the pauses, shuffle them zipped together so and then merge them 
        pattern_pauses=[]
        length=1000000
        timeline_rec_occupancy=np.zeros((int(length),3))
        nr_bursts=int(p_burst*nr_events)
        nr_buwp=int(p_buwp*nr_events)
        nr_pause=int(p_pause*nr_events)
        nr_base=int((1.0-p_burst-p_buwp-p_pause)*nr_events)
        #print nr_base
        for i in xrange(nr_bursts):	
            rnd_jitter=5.0-(10.0*rand.random())
            pattern_sig.append(("bu",0.2,200.0+a.DA_avg_conc))
            pattern_pauses.append(("b",t_baseline_burst+rnd_jitter))

        for i in xrange(nr_buwp):	
            rnd_jitter=5.0-(10.0*rand.random())
            pattern_sig.append(("buwp",0.1,100.0+a.DA_avg_conc,1.3))
            pattern_pauses.append(("b",t_baseline_buwp+rnd_jitter))
            
        for i in xrange(nr_pause):	
            rnd_jitter=5.0-(10.0*rand.random())
            pattern_sig.append(("pa",1.5))
            pattern_pauses.append(("b",t_baseline_pa+rnd_jitter))
            
        for i in xrange(nr_base):	
            rnd_jitter=5.0-(10.0*rand.random())
            pattern_sig.append(("b",t_repetition+rnd_jitter))
            pattern_pauses.append(("b",0))	#needs to be added so that the following shuffle works
        
        #puts pause list and signal list together and shuffles them with same order 
        zipped_list=list(zip(pattern_sig,pattern_pauses))
        rand.shuffle(zipped_list)
        pattern_sig, pattern_pauses=[list(t) for t in zip(*zipped_list)]	#separates the zipped signal and pause list 
        #interleaves the signal and pause list
        pattern=pattern_sig+pattern_pauses
        pattern[::2]=pattern_sig
        pattern[1::2]=pattern_pauses
        #add a 10 second pause add beginning of pattern
        pattern=[("b",10.0)]+pattern 
        
        
        #print pattern
        t_interp, timeline_rec_occupancy[:,0]=a.combined_event(length,pattern)





    #run the actual ode integrator and save result in a file
    k_on=k_on/1000.0    #make into 1/ms
    k_off=k_off/1000.0    #make into 1/ms

    #This is some stupid workaround ... since somehow when we run the RK integrator for the full length (300000 steps) it takes forever
    one_step_ode_int=10000
    if (length>one_step_ode_int):
            counter=0
            continue_calc=RL
            while (length>one_step_ode_int):
                sol=odeint(deriv_RL,continue_calc,t_interp[(counter*one_step_ode_int):((counter+1)*one_step_ode_int)], args=(timeline_rec_occupancy[(counter*one_step_ode_int):((counter+1)*one_step_ode_int),0],t_interp[(counter*one_step_ode_int):((counter+1)*one_step_ode_int)],C_R_max,k_on,k_off),hmax=1.0)
                timeline_rec_occupancy[(counter*one_step_ode_int):((counter+1)*one_step_ode_int),2]=sol.reshape(one_step_ode_int,) #sol.reshape(nr_steps,)
                continue_calc=sol[-1,0]
                length-=one_step_ode_int
                counter+=1
                
            sol=odeint(deriv_RL,continue_calc,t_interp[(counter*one_step_ode_int):((counter*one_step_ode_int)+length)], args=(timeline_rec_occupancy[(counter*one_step_ode_int):((counter*one_step_ode_int)+length),0],t_interp[(counter*one_step_ode_int):((counter*one_step_ode_int)+length)],C_R_max,k_on,k_off),hmax=1.0)
            timeline_rec_occupancy[(counter*one_step_ode_int):((counter*one_step_ode_int)+length),2]=sol.reshape(length,) #sol.reshape(nr_steps,)

    else:
            sol=odeint(deriv_RL,RL,t_interp, args=(timeline_rec_occupancy[:,0],t_interp,C_R_max,k_on,k_off),hmax=1.0)
            timeline_rec_occupancy[:,2]=sol.reshape(int(length),) 



    #fn_D2="free_"+str(switch)+"_"+str(pause_switch)+"_"+str(p_burst)+"_"+str(p_buwp)+"_"+str(p_pause)+"_"+str(nr_events)+"_"+str(running_nr)+".dat"
    fn_D2="free_"+str(switch)+"_"+str(pause_switch)+"_"+"{:.2f}".format(p_burst)+"_"+"{:.2f}".format(p_buwp)+"_"+"{:.2f}".format(p_pause)+"_"+str(nr_events)+"_"+str(running_nr)+".dat"



    pickle.dump( timeline_rec_occupancy, open(fn_D2,"wb") ,protocol=-1)
